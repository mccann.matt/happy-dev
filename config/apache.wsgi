import sys
sys.path.insert(0, '/var/www/api')
sys.path.insert(0, '/var/www/external')

import os
os.environ['TARGET'] = 'production'

from happydev.api.api_root import api as application

from flask_cors import CORS

CORS(
    application,
    resources={r"/*": {"origins": ["https://www.happydev.io", "https://happydev.io"]}},
    supports_credentials=True
)
