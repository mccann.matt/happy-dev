var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-eval-source-map',

    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: [{
                    loader: 'awesome-typescript-loader',
                    options: { configFileName: helpers.root('', 'tsconfig.json') }
                } , 'angular2-template-loader', 'angular-router-loader']
            },
        ]
    },

    output: {
        path: helpers.root('dist'),
        publicPath: 'http://localhost:8080/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('[name].css'),

        new webpack.DefinePlugin({
            'process.env': {
                'API_SERVER': JSON.stringify('http://localhost:8001'),
                'MVT_ROOT_API_URL': JSON.stringify('http://localhost:8002/experiment/'),
                'BLOG_URL': JSON.stringify('http://localhost/blog')
            }
        }),

        new HtmlWebpackPlugin({
            template: 'index.ejs',
            googleAnalyticsTrackingId: 'UA-96264932-2',
        }),
    ],

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
});
