from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient.discovery import build

import os

# Authenticate and create the service for the Core Reporting API
credentials = ServiceAccountCredentials.from_json_keyfile_name(
  '.ga.json', ['https://www.googleapis.com/auth/analytics.readonly'])
http_auth = credentials.authorize(Http())
analytics = build('analytics', 'v4', http=http_auth)

# Configure the view id
view_id = '146410982'
if 'TARGET' in os.environ.keys() and os.environ['TARGET'] == 'production':
    print("Pointing at production...")
    view_id = '146416223'


def run_report(report_body):
    ''' Runs the report and results the row data. '''
    report_body['viewId'] = view_id

    report = analytics.reports().batchGet(
        body={
            'reportRequests': [report_body]
        }
    ).execute()

    return report['reports'][0]['data']['rows']
