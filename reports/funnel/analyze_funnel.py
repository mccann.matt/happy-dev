#!/usr/bin/env python3

'''
@author R. Matt McCann
@brief Prints a report of funnel statistics
@copyright &copy; 2017 R. Matt McCann
'''

import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external/mvt-microservice')

from mvt.api.event import ExperimentEvent # noqa

if len(sys.argv) != 3:
    print(
        '''
    Usage: %s [cohort_start_date] [cohort_end_date]

    cohort_start_date:
        First day in the range of activity included in this report (inclusive!)
        Example: 2017/03/03

    cohort_end_date:
        Last day in the range of activity included in this report (inclusive!)
        Example: 2017/03/05
        ''')
    sys.exit(-1)
cohort_start_date = sys.argv[1]
cohort_end_date = sys.argv[2]

exp_id = 'permanent-funnel_analysis'
cohort_filter = ('>=', cohort_start_date, '<=', cohort_end_date)

# Determine the total number of visits in the cohort
visits = len(ExperimentEvent.db_list_all(experiment_id=exp_id, action_type='visit', when_created=cohort_filter))

# Determine the audience view selection statistics
audience_view_selects = len(ExperimentEvent.db_list_all(
    experiment_id=exp_id, action_type='audience_view_select', when_created=cohort_filter))
audience_view_retain = audience_view_selects * 100 / visits if visits else 0
audience_view_total_retain = audience_view_selects * 100 / visits if visits else 0

# Determine the account registration statistics
registers = len(ExperimentEvent.db_list_all(
    experiment_id=exp_id, action_type='register_account', when_created=cohort_filter))
register_retain = registers * 100 / audience_view_selects if audience_view_selects else 0
register_total_retain = registers * 100 / visits if visits else 0

# Print out the report
report = \
    '''
    -------------------- Primary Funnel Analysis --------------------

    Cohort:    %s - %s


    -----------------------------------------------------------------
    Visits: %d (n/a | 100.0%%)
    ------------------------------------------------------
    Audience View Selects: %d (%0.2f%% | %0.2f%%)
    ------------------------------------------
    Registrations: %d (%0.2f%% | %0.2f%%)
    ----------------------------


    ''' % (
        cohort_start_date, cohort_end_date, visits, audience_view_selects,
        audience_view_retain, audience_view_total_retain, registers, register_retain,
        register_total_retain
    )
print(report)
