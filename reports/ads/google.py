'''
@author R. Matt McCann
@brief Provides helper functions for pulling Google AdWord statistics
@copyright &copy; 2017 R. Matt McCann
'''

import os

from googleads import adwords, oauth2


# Check that the configuration settings are defined in the env
for config_key in (
        'GOOG_ADS_CLIENT_ID', 'GOOG_ADS_CLIENT_SECRET', 'GOOG_ADS_API_KEY', 'GOOG_ADS_ACCESS_TOKEN',
        'GOOG_ADS_REFRESH_TOKEN'):
    if config_key not in os.environ.keys():
        raise ValueError('You must defined %s in environ!' % config_key)

client_id = os.environ['GOOG_ADS_CLIENT_ID']
client_secret = os.environ['GOOG_ADS_CLIENT_SECRET']
api_key = os.environ['GOOG_ADS_API_KEY']
access_token = 'N-DgcqrkeDqL77LhmDbH6Q'
refresh_token = os.environ['GOOG_ADS_REFRESH_TOKEN']
user_agent = 'Ads Python Client Library'
customer_id = '761-182-8601'

oauth2_client = oauth2.GoogleRefreshTokenClient(client_id, client_secret, refresh_token)
adwords_client = adwords.AdWordsClient(access_token, oauth2_client, user_agent, client_customer_id=customer_id)

campaign_service = adwords_client.GetService('CampaignService')
print(campaign_service)

selector = {
    'fields': ['Id', 'Name', 'Status'],
    'paging': {
        'startIndex': '0',
        'numberResults': '1'
    }
}

page = campaign_service.get(selector)
print(page)

for campaign in page['entries']:
    print(campaign)
