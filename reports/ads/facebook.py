'''
@author R. Matt McCann
@brief Provides functionality for fetching Facebook Ad statistics
@copyright &copy; 2017 R. Matt McCann
'''

import os

from facebookads.api import FacebookAdsApi
from facebookads import objects

# Check that the configuration settings are defined in the env
for config_key in ('FB_ADS_APP_ID', 'FB_ADS_ACCOUNT_ID', 'FB_ADS_APP_SECRET', 'FB_ADS_ACCESS_TOKEN'):
    if config_key not in os.environ.keys():
        raise ValueError('You must defined %s in environ!' % config_key)

# Automatically initialize the API when the module is imported
my_app_id = os.environ['FB_ADS_APP_ID']
my_app_secret = os.environ['FB_ADS_APP_SECRET']
my_access_token = os.environ['FB_ADS_ACCESS_TOKEN']
FacebookAdsApi.init(my_app_id, my_app_secret, my_access_token)

my_account = objects.AdAccount('act_%s' % os.environ['FB_ADS_ACCOUNT_ID'])


def get_cost_stats(ad_set_id):
    '''
    @param ad_set_id Tracking ID of the ad set
    @returns (clicks, cpc)
    '''
    insights = objects.AdSet(ad_set_id).get_insights(fields=['cpc', 'clicks'])[0]

    return (int(insights['clicks']), float(insights['cpc']))
