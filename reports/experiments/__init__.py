'''
@author R. Matt McCann
@brief Provides helper functions for producing experiment reports
@copyright &copy; 2017 R. Matt McCann
'''


def pretty_print_long_string(indent, long_string):
    max_line_len = 100 - indent

    pos = 0
    while pos < len(long_string):
        end_of_line = pos + max_line_len
        if end_of_line > len(long_string):
            end_of_line = len(long_string)

        print("%s%s" % (" " * indent, long_string[pos:end_of_line]))
        pos = end_of_line


def print_standard_experiment_description(
        id, sponser, hypotheses_under_test, testing_metrics, testing_method, performance_eval, success_critera,
        target_sample_size):
    ''' Prints out the experiment description in a standard format. '''

    print("")
    print("    -------------------- Description --------------------")
    print("")
    print("    Experiment:             %03d" % id)
    print("    Sponser:                %s" % sponser)
    print("")

    print("    Hypotheses Under Test:")
    for key, value in hypotheses_under_test.items():
        print("        %s =>" % key)
        pretty_print_long_string(12, value)
    print("")

    print("    Testing Metrics:")
    pretty_print_long_string(8, testing_metrics)
    print("")

    print("    Testing Method:")
    pretty_print_long_string(8, testing_method)
    print("")

    print("    Performance Evaluation:")
    pretty_print_long_string(8, performance_eval)
    print("")

    print("    Success Criteria:")
    pretty_print_long_string(8, success_critera)
    print("")

    print("    Target Sample Size:")
    print("        %s" % target_sample_size)
    print("")


def print_results_header():
    ''' Prints out the results header. '''
    print("    -------------------- Results --------------------")
    print("")


def print_standard_sample_details(total_sample_size, target_sample_size):
    print("    Sample:")
    print("        Total sample size:          %d" % total_sample_size)
    print("        Percent completed:          %0.0f%%" % (total_sample_size * 100 / target_sample_size))
    print("        Sample size target reached? %s" % (total_sample_size >= target_sample_size))
    print("")
