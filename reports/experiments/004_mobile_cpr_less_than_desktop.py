#!/usr/bin/env python3

'''
@author R. Matt McCann
@brief Prints experimental results for the experiment '004_mobile_cpr_less_than_desktop'
@copyright &copy; 2017 R. Matt McCann
'''

import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external/mvt-microservice')

import reports.ads.facebook as facebook # noqa
import reports.experiments as report # noqa
import reports.google_analytics as ga # noqa

''' Grab the experiment statistics. '''
target_size = 300

desktop_adset_id = '23842556811320723'
desktop_clicks, desktop_cpc = facebook.get_cost_stats(desktop_adset_id)

mobile_adset_id = '23842558542890723'
mobile_clicks, mobile_cpc = facebook.get_cost_stats(mobile_adset_id)

data = ga.run_report({
    'dateRanges': [{'startDate': '365daysAgo', 'endDate': 'today'}],
    'metrics': [{'expression': 'ga:sessions'}, {'expression': 'ga:goal1Completions'}],
    'dimensions': [{"name": "ga:campaign"}]
})
desktop_sessions = 0
desktop_registers = 0
desktop_cpr = 9999.99
mobile_sessions = 0
mobile_registers = 0
mobile_cpr = 9999.99
for row in data:
    dimensions = row['dimensions']
    metrics = row['metrics'][0]['values']

    if dimensions == ['DesktopOnly']:
        desktop_sessions, desktop_registers = int(metrics[0]), int(metrics[1])
        desktop_cpr = desktop_clicks * desktop_cpc / desktop_registers if desktop_registers else 9999.99
    if dimensions == ['MobileOnly']:
        mobile_sessions, mobile_registers = int(metrics[0]), int(metrics[1])
        mobile_cpr = mobile_clicks * mobile_cpc / mobile_registers if mobile_registers else 9999.99

''' Standard experiment details '''
id = 4
sponser = 'R. Matt McCann'
hypotheses_under_test = {
    'Hm4': 'Mobile registration costs will be less than desktop registration costs'
}
testing_metrics = 'Sessions, Registrations, CPC'
testing_method = \
    'Registrations are tracked by campaign source in GA. Two campaigns, mobile-only and desktop-only ' + \
    'run in facebook to feed traffic. Traffic cost is divided by number of registrations to calc cpr'
performance_eval = 'Difference in CPR for desktop registration vs. mobile registrations'
success_criteria = 'Mobile CPR less than of desktop CPR'
target_sample_size = '300 Desktop Sessions, 300 Mobile Registrations'

''' Print the standard experiment description '''
report.print_standard_experiment_description(
    id, sponser, hypotheses_under_test, testing_metrics, testing_method, performance_eval, success_criteria,
    target_sample_size
)

''' Print the standard results header '''
report.print_results_header()

''' Print the sample details. '''
print("    Sample:")
print("        Desktop sample size:          %d" % desktop_sessions)
print("        Desktop percent completed:    %0.0f%%" % (desktop_sessions * 100 / target_size))
print("        Mobile sample size:           %d" % mobile_sessions)
print("        Mobile percent completed:     %0.0f%%" % (mobile_sessions * 100 / target_size))
print("        Sample size target reached?   %s" % (desktop_sessions >= target_size and mobile_sessions >= target_size))
print("")

''' Print the hypothesis performance results. '''
print("    Performance:")
print("        Desktop regisrations:         %d" % desktop_registers)
print("        Desktop cpr:                 $%0.2f" % desktop_cpr)
print("        Mobile registrations:         %d" % mobile_registers)
print("        Mobile cpr:                  $%0.2f" % mobile_cpr)
print("        Mobile cpr vs desktop cpr:   $%+0.2f" % (mobile_cpr - desktop_cpr))
print("        Hypothesis success:           %s" % (mobile_cpr - desktop_cpr < 0))
print("")
