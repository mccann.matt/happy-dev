#!/usr/bin/env python3

'''
@author R. Matt McCann
@brief Prints experiment results for the experiment 'supply_side_value_hypotheses'
@copyright &copy; 2017 R. Matt McCann
'''

import operator
import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external/mvt-microservice')

import reports.experiments as report # noqa

from mvt.api.event import ExperimentEvent # noqa

''' Grab the experiment statistics '''
experiment_id = 'supply_side_value_hypotheses'
cohort = '2017_03_16+'
actions = ['register_account']

# Fetch raw event records
all_visits = ExperimentEvent.db_list_all(experiment_id=experiment_id, action_type='visit')
all_registers = ExperimentEvent.db_list_all(
    experiment_id=experiment_id, action_type='register_account')
total_sample_size = len(all_visits)

variants = [
    "Hs0_Hs1_Hs2", 'Hs0_Hs1_Hs3', 'Hs0_Hs1_Hs4', 'Hs0_Hs2_Hs3', 'Hs0_Hs2_Hs4', 'Hs0_Hs3_Hs4',
    'Hs1_Hs2_Hs3', 'Hs1_Hs2_Hs4', 'Hs1_Hs3_Hs4', 'Hs2_Hs3_Hs4']
visits = {}
registers = {}

for variant in variants:
    visits[variant] = len(ExperimentEvent.db_list_all(
        experiment_id=experiment_id, variant=variant, action_type='visit'))
    registers[variant] = len(ExperimentEvent.db_list_all(
        experiment_id=experiment_id, variant=variant, action_type='register_account'))

hypos = ['Hs0', 'Hs1', 'Hs2', 'Hs3', 'Hs4']
hypo_on_visits = {}
hypo_off_visits = {}
hypo_on_registers = {}
hypo_off_registers = {}

for hypo in hypos:
    hypo_on_visits[hypo] = 0
    hypo_off_visits[hypo] = 0
    hypo_on_registers[hypo] = 0
    hypo_off_registers[hypo] = 0

    for variant in variants:
        if hypo in variant:
            hypo_on_visits[hypo] += visits[variant]
            hypo_on_registers[hypo] += registers[variant]
        else:
            hypo_off_visits[hypo] += visits[variant]
            hypo_off_registers[hypo] += registers[variant]

hypo_on_rates = {}
hypo_off_rates = {}
hypo_performance = {}
for hypo in hypos:
    hypo_on_rates[hypo] = hypo_on_registers[hypo] * 100 / hypo_on_visits[hypo] if hypo_on_visits[hypo] else 0
    hypo_off_rates[hypo] = hypo_off_registers[hypo] * 100 / hypo_off_visits[hypo] if hypo_off_visits[hypo] else 0
    hypo_performance[hypo] = hypo_on_rates[hypo] - hypo_off_rates[hypo]

sorted_hypo_performance = sorted(hypo_performance.items(), key=operator.itemgetter(1))
sorted_hypo_performance.reverse()

''' Standard experiment details '''
id = 1
sponser = 'R. Matt McCann'
hypotheses_under_test = {
    'Hs0': 'Developers will use HappyDev because they value the anonymity benefit',
    'Hs1': 'Developers will use HappyDev because they want to be explicit about what they want',
    'Hs2': 'Developers will use HappyDev because they value having a prospective employer agree to terms up front',
    'Hs3': 'Developers will use HappyDev because it is very easy to setup',
    'Hs4': 'Developers will use HappyDev because there are no middle-men recruiters'
}
testing_metrics = 'Registration rate'
testing_method = \
    'Three feature badges are displayed for each variation. Each feature badge represents one of ' +\
    'the five value hypotheses. Registration rates are tracked for each variation and the ' +\
    'statistics are coalated for multi-variant analysis to rank relative truthiness of the ' +\
    'value hypotheses.'
performance_eval = 'Difference in registration rate for feature badge on vs. off'
success_critera = \
    'N/A - Results of experiment will be used both as baseline data and as a basis for ' +\
    'formulation of experiments intended to improve funnel performance.'
target_sample_size = 750

''' Print the standard experiment description '''
report.print_standard_experiment_description(
    id, sponser, hypotheses_under_test, testing_metrics, testing_method, performance_eval, success_critera,
    target_sample_size
)

''' Print the standard results header '''
report.print_results_header()
report.print_standard_sample_details(total_sample_size, target_sample_size)

''' Print the hypothesis performance results. '''
print("    Performance:")
for hypo, performance in sorted_hypo_performance:
    print("        %s    %+0.2f%%" % (hypo, performance))
print("")
