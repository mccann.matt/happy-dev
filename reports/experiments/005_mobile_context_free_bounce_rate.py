#!/usr/bin/env python3

'''
@author R. Matt McCann
@brief Prints experimental results for the experiment '005_mobile_context_free_bounce_rate'
@copyright &copy; 2017 R. Matt McCann
'''

import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external/mvt-microservice')

import reports.experiments as report # noqa
import reports.google_analytics as ga # noqa

''' Grab the experiment statistics. '''
target_size = 300

data = ga.run_report({
    'dateRanges': [{'startDate': '365daysAgo', 'endDate': 'today'}],
    'metrics': [{'expression': 'ga:sessions'}, {'expression': 'ga:goal2Completions'}],
    'dimensions': [{"name": "ga:deviceCategory"}]
})

desktop_sessions = 0
desktop_audience_selects = 0
mobile_sessions = 0
mobile_audience_selects = 0
desktop_bounce_rate = 0
mobile_bounce_rate = 0
for row in data:
    dimensions = row['dimensions']
    metrics = row['metrics'][0]['values']

    if dimensions == ['desktop']:
        desktop_sessions, desktop_audience_selects = int(metrics[0]), int(metrics[1])
        desktop_bounce_rate = 100 - desktop_audience_selects * 100 / desktop_sessions
    if dimensions == ['mobile'] or dimensions == ['tablet']:
        mobile_sessions += int(metrics[0])
        mobile_audience_selects += int(metrics[1])
        mobile_bounce_rate = 100 - mobile_audience_selects * 100 / mobile_sessions
bounce_rate_delta = mobile_bounce_rate - desktop_bounce_rate

id = 5
sponser = 'R. Matt McCann'
hypotheses_under_test = {
    'Hm3': 'Mobile bounce rate will be higher than desktop bounce rate'
}
testing_metrics = '# Sessions, # AudienceView Selects'
testing_method = \
    '# sessions and # audience views are pulling from Google Analytics segmented by device type'
performance_eval = 'Difference in bounce rate for desktop sessions vs. mobile sessions'
success_criteria = 'Mobile bounce rates greater than desktop rates'
target_sample_size = '300 Desktop sessions, 300 Mobile sessions'

''' Print the standard experiment description '''
report.print_standard_experiment_description(
    id, sponser, hypotheses_under_test, testing_metrics, testing_method, performance_eval, success_criteria,
    target_sample_size
)

''' Print the standard results header '''
report.print_results_header()

''' Print the sample details. '''
print("    Sample:")
print("        Desktop sample size:          %d" % desktop_sessions)
print("        Desktop percent completed:    %0.0f%%" % (desktop_sessions * 100 / target_size))
print("        Mobile sample size:           %d" % mobile_sessions)
print("        Mobile percent completed:     %0.0f%%" % (mobile_sessions * 100 / target_size))
print("        Sample size target reached?   %s" % (desktop_sessions >= target_size and mobile_sessions >= target_size))
print("")

''' Print the hypothesis performance results. '''
print("    Performance:")
print("        Desktop bounce rate:            %0.2f%%" % desktop_bounce_rate)
print("        Mobile bounce rate:             %0.2f%%" % mobile_bounce_rate)
print("        Mobile vs. desktop rate delta: %+0.2f%%" % bounce_rate_delta)
print("        Hypotheses Success:             %s" % (bounce_rate_delta > 0))
print("")
