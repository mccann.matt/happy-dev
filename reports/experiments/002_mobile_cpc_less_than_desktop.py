#!/usr/bin/env python3

'''
@author R. Matt McCann
@brief Prints experiment results for the experiment '002_mobile_cpc_less_than_desktop'
@copyright &copy; 2017 R. Matt McCann
'''

import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external/mvt-microservice')

import reports.ads.facebook as facebook # noqa
import reports.experiments as report # noqa

from mvt.api.event import ExperimentEvent # noqa

''' Grab the experiment statistics. '''

mobile_adset_id = ''
target_size = 300

desktop_adset_id = '23842556811320723'
desktop_clicks, desktop_cpc = facebook.get_cost_stats(desktop_adset_id)

mobile_adset_id = '23842558542890723'
mobile_clicks, mobile_cpc = facebook.get_cost_stats(mobile_adset_id)
cpc_delta = mobile_cpc - desktop_cpc

''' Standard experiment details '''
id = 2
sponser = 'R. Matt McCann'
hypotheses_under_test = {
    'Hm0': 'Mobile cost-per-click will be less than desktop cost-per-click'
}
testing_metrics = 'Facebook Ad(mobile-only,desktop-only) CPC & Number of Clicks'
testing_method = \
    'The same ad is configured in two ad sets, one targeting only desktop and the other targeting only mobile. ' + \
    'The cost per click data will be compared'
performance_eval = 'Difference in CPC for desktop visits vs. mobile visits'
success_criteria = 'Mobile cost-per-click less than desktop cost-per-click'
target_sample_size = '300 Desktop Clicks, 300 Mobile Clicks'

''' Print the standard experiment description '''
report.print_standard_experiment_description(
    id, sponser, hypotheses_under_test, testing_metrics, testing_method, performance_eval, success_criteria,
    target_sample_size
)

''' Print the standard results header '''
report.print_results_header()

''' Print the sample details. '''
print("    Sample:")
print("        Desktop sample size:        %d" % desktop_clicks)
print("        Desktop percent completed:  %0.0f" % (desktop_clicks * 100 / target_size))
print("        Mobile sample size:         %d" % mobile_clicks)
print("        Mobile percent completed:   %0.0f" % (mobile_clicks * 100 / target_size))
print("        Sample size target reached? %s" % (desktop_clicks >= target_size and mobile_clicks >= target_size))
print("")

''' Print the hypothesis performance results. '''
print("    Performance:")
print("        Desktop CPC:             $%0.2f" % desktop_cpc)
print("        Mobile CPC:              $%0.2f" % mobile_cpc)
print("        Mobile vs. desktop CPC:  $%+0.2f" % cpc_delta)
print("        Hypotheses Success:      %s" % (cpc_delta < 0))
print("")
