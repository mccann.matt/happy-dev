#!/usr/bin/env python3

'''
@author R. Matt McCann
@brief Prints experimental results for the experiment '003_mobile_vs_desktop_register_rates'
@copyright &copy; 2017 R. Matt McCann
'''

import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external/mvt-microservice')

import reports.experiments as report # noqa
import reports.google_analytics as ga # noqa

''' Grab the experiment statistics. '''
target_size = 300

data = ga.run_report({
    'dateRanges': [{'startDate': '365daysAgo', 'endDate': 'today'}],
    'metrics': [{'expression': 'ga:sessions'}, {'expression': 'ga:goal1Completions'}],
    'dimensions': [{"name": "ga:deviceCategory"}]
})

desktop_sessions = 0
desktop_registers = 0
mobile_sessions = 0
mobile_registers = 0
desktop_register_rate = 0
mobile_register_rate = 0
for row in data:
    dimensions = row['dimensions']
    metrics = row['metrics'][0]['values']

    if dimensions == ['desktop']:
        desktop_sessions, desktop_registers = int(metrics[0]), int(metrics[1])
        desktop_register_rate = desktop_registers * 100 / desktop_sessions
    if dimensions == ['mobile'] or dimensions == ['tablet']:
        mobile_sessions += int(metrics[0])
        mobile_registers += int(metrics[1])
        mobile_register_rate = mobile_registers * 100 / mobile_sessions
register_rate_delta = mobile_register_rate - desktop_register_rate

''' Standard experiment details '''
id = 3
sponser = 'R. Matt McCann'
hypotheses_under_test = {
    'Hm3': 'Mobile registration rates will be similar to desktop registration rates'
}
testing_metrics = 'Registration rate'
testing_method = \
    'Visit and registration event records now include device type. The records will be split by ' + \
    'device type and compared.'
performance_eval = 'Difference in registration rate for desktop visits vs. mobile visits'
success_criteria = 'Mobile registration rates within +- 1% of desktop rates'
target_sample_size = '300 Desktop Visits, 300 Mobile Visits'

''' Print the standard experiment description '''
report.print_standard_experiment_description(
    id, sponser, hypotheses_under_test, testing_metrics, testing_method, performance_eval, success_criteria,
    target_sample_size
)

''' Print the standard results header '''
report.print_results_header()

''' Print the sample details. '''
print("    Sample:")
print("        Desktop sample size:          %d" % desktop_sessions)
print("        Desktop percent completed:    %0.0f%%" % (desktop_sessions * 100 / target_size))
print("        Mobile sample size:           %d" % mobile_sessions)
print("        Mobile percent completed:     %0.0f%%" % (mobile_sessions * 100 / target_size))
print("        Sample size target reached?   %s" % (desktop_sessions >= target_size and mobile_sessions >= target_size))
print("")

''' Print the hypothesis performance results. '''
print("    Performance:")
print("        Desktop register rate:         %0.2f%%" % desktop_register_rate)
print("        Mobile register rate:          %0.2f%%" % mobile_register_rate)
print("        Mobile vs. desktop rate delta: %+0.2f%%" % register_rate_delta)
print("        Hypotheses Success:            %s" % (register_rate_delta < 1))
print("")
