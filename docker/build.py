#! /usr/bin/env python3

import os
import sys

if len(sys.argv) < 2 or (len(sys.argv) == 2 and sys.argv[1] != 'base' and sys.argv[1] != 'dev'):
    print("Usage: %s [base/dev]" % sys.argv[0])
    sys.exit(-1)

dockerfile_in = open('./docker/Dockerfile.base' if sys.argv[1] == 'base' else './docker/Dockerfile.dev')
dockerfile_out = open('./Dockerfile', 'w')

for line in dockerfile_in.readlines():
    if line.startswith('#include'):
        fragment_lines = open(line.split()[1]).readlines()
        for fragment_line in fragment_lines:
            dockerfile_out.write(fragment_line)
        dockerfile_out.write('\n')
    else:
        dockerfile_out.write(line)

dockerfile_in.close()
dockerfile_out.close()

if len(sys.argv) == 2:
    tag = 'latest' if sys.argv[1] == 'base' else 'dev'
    os.system('docker build -t registry.gitlab.com/mccann.matt/happy-dev:%s .' % tag)
