// @author R. Matt McCann
// @brief A simple mix-in that provides year-range fields and utility functions
// @copyright &copy; 2017 R. Matt McCann

const maxMonthValue = 12;
const monthAsStringLong: Array<string> = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
];

const monthAsStringShort: Array<string> = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug',
    'Sept', 'Oct', 'Nov', 'Dec'
];
const noEndMonth = 13;
const noEndYear = 9999;

export class MonthYearRange {
    private readonly _endMonth: number;
    private readonly _endYear: number;
    private readonly _startMonth: number;
    private readonly _startYear: number;

    constructor(startMonth: number, startYear: number, endMonth?: number, endYear?: number) {
        // If we have just the end month or just the end year
        if (endMonth ? !endYear : endYear) {
            throw new RangeError(
                `Must provide both endMonth(${endMonth}) and endYear(${endYear}), or neither!`);
        }

        // If the month values are out of range
        if (endMonth > maxMonthValue) {
            throw new RangeError(`endMonth too large(${endMonth})`);
        }
        if (startMonth > maxMonthValue) {
            throw new RangeError(`startMonth too large(${startMonth})`);
        }

        // If the end date was provided and it is before the start date
        if (endYear && ((startYear > endYear) ||
                (startYear === endYear && startMonth > endMonth))) {
            throw new RangeError(
                `Start date(${startMonth}/${startYear}) must be before end date(${endMonth}/${endYear})!`);
        }

        this._startMonth = startMonth;
        this._startYear = startYear;
        this._endMonth = endMonth ? endMonth : noEndMonth;
        this._endYear = endYear ? endYear : noEndYear;
    }

    get endMonth(): number {
        if (this._endMonth === noEndMonth) {
            throw new RangeError('endMonth not set, you should be checking isPresent first!');
        }

        return this._endMonth;
    }

    get endMonthAsStringLong(): string {
        return monthAsStringLong[this.endMonth];
    }

    get endMonthAsStringShort(): string {
        return monthAsStringShort[this.endMonth];
    }

    get endYear(): number {
        if (this._endYear === noEndYear) {
            throw new RangeError('endYear not set, you should be checking isPresent first!');
        }

        return this._endYear;
    }

    get isPresent(): boolean {
        let date: Date = new Date();

        return (this._endYear >= date.getFullYear()) ||
            (this._endYear === date.getFullYear() && this._endMonth > date.getMonth());
    }

    get startMonth(): number {
        return this._startMonth;
    }

    get startMonthAsStringLong(): string {
        return monthAsStringLong[this.startMonth];
    }

    get startMonthAsStringShort(): string {
        return monthAsStringShort[this.startMonth];
    }

    get startYear(): number {
        return this._startYear;
    }
}

export class YearRange {
    private _endYear: number;
    private _startYear: number;

    constructor(startYear: number, endYear?: number) {
        if (endYear && endYear < this._startYear) {
            throw new RangeError(
                `endYear(${endYear}) cannot be less than startYear(${this._startYear})!`);
        }

        this._startYear = startYear;
        this._endYear = endYear ? endYear : noEndYear;
    }

    get endYear(): number {
        if (this._endYear === noEndYear) {
            throw new RangeError(
                'endYear is not specified. You should be checking isPresent first!');
        }

        return this._endYear;
    }

    get isPresent(): boolean {
        return this._endYear > (new Date()).getFullYear();
    }

    get startYear(): number {
        return this._startYear;
    }
}
