// @author R. Matt McCann
// @brief Coverage doesn't seem to generate properly without this simple hack
// @copyright &copy; 2017 R. Matt McCann

import { TestBed } from '@angular/core/testing';

import { APP_MODULE_DEF } from './app.module';

describe('[NOTATEST] Triggering coverage...', () => {
    TestBed.configureTestingModule(APP_MODULE_DEF);
});
