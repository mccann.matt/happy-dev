// @author R. Matt McCann
// @brief Model for the experiment event
// @copyright &copy; 2017 R. Matt McCann

export interface ExperimentEvent {
    action_type: string;
    cohort: string;
    experiment_id: string;
    id: string;
    variant: string;
    visitor_id: string;
};
