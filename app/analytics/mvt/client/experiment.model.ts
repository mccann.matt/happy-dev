// @author R. Matt McCann
// @brief Model for the experiment
// @copyright &copy; 2017 R. Matt McCann

export interface Experiment {
    id: string;
    description: string;
};
