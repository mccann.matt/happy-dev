// @author R. Matt McCann
// @brief Unit tets for the MvtService
// @copyright &copy; 2017 R. Matt McCann

import { async, TestBed, inject } from '@angular/core/testing';
import { HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { MVT_STRUCTURE_DEFINITION, MVT_API_ROOT_URL, MvtService } from './mvt.service';
import {
    MvtStructureDefinition, ExperimentDefinition, VariantDefinition
} from './mvt-structure-definition.models';
import { GoogleAnalytics } from '../../google/google-analytics';
import { Experiment } from './experiment.model';


describe('MvtService', () => {
    let mvtService, mockBackend, mockGa;

    beforeEach(() => {
        mockGa = jasmine.createSpyObj('GoogleAnalytics', ['trackerCallback']);

        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                MvtService,
                {
                    provide: MVT_STRUCTURE_DEFINITION,
                    useFactory: () => {
                        let experiments = new Map<string, ExperimentDefinition>();

                        experiments['myExpId1'] = {
                            cohort: 'cohort1',
                            variants: [
                                new VariantDefinition('variant1', 0, 9999)
                            ]
                        };
                        experiments['myExpId2'] = {
                            cohort: 'cohort2',
                            variants: [
                                new VariantDefinition('variant3', 0, 9999)
                            ]
                        };

                        let structure: MvtStructureDefinition = {
                            experimentIds: ['myExpId1', 'myExpId2'],
                            experiments: experiments
                        };

                        return structure;
                    }
                },
                {
                    provide: MVT_API_ROOT_URL,
                    useValue: 'http://localhost:8002/experiment/'
                },
                { provide: XHRBackend, useClass: MockBackend },
                { provide: GoogleAnalytics, useValue: mockGa}
            ]
        });
    });

    beforeEach(inject([MvtService, XHRBackend], (_mvtService, _mockBackend) => {
        mvtService = _mvtService;
        mockBackend = _mockBackend;
    }));

    describe('createExperiment', () => {
        it('should create the experiment', async(() => {
            let experiment: Experiment = {
                id: 'myExpId1',
                description: 'a description'
            };

            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8002/experiment/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                expect(conn.request.getBody()).toEqual(JSON.stringify(experiment));

                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(experiment)})));
            });

            mvtService.createExperiment('myExpId1', 'a description').then((response) => {
                expect(response).toEqual(experiment);
            }).catch((error) => {
                fail('should not have rejected the promised Experiment');
            });
        }));

        it('should report an error message on a failed creation', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify('error!')})));
            });

            mvtService.createExperiment('myExpId1', 'a description').then((response) => {
                fail('should have rejected the promised Experiment');
            }).catch((error) => {
                expect(error).toEqual('error!');
            });
        }));
    });

    describe('createEvent', () => {
        let event;

        beforeEach(() => {
            event = {
                'action_type': 'register',
                'cohort': 'cohort1',
                'variant': 'variant1',
                'visitor_id': 'gaClientId'
            };

            mockGa.trackerCallback.and.callFake((trackerCallback) => {
                let mockTracker = jasmine.createSpyObj('tracker', ['get']);
                mockTracker.get.and.returnValue('gaClientId');

                trackerCallback(mockTracker);
            });
        });

        it('should create the event', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8002/experiment/myExpId1/event/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                if (conn.request.getBody().includes('register')) {
                    expect(conn.request.getBody()).toEqual(JSON.stringify(event));
                }

                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(event)})));
            });

            mvtService.createEvent('myExpId1', 'register').then((response) => {
                expect(response).toEqual(event);
            }).catch((error) => {
                console.error(error);
                fail('should not have rejected the promised Event');
            });
        }));

        it('should pass along optional context details', async(() => {
            let details = new Map<string, any>();
            details['something'] = 'else';

            let eventWithDetails = {
                'action_type': 'register',
                'cohort': 'cohort1',
                'variant': 'variant1',
                'visitor_id': 'gaClientId',
                'something': 'else'
            };

            mockBackend.connections.subscribe(conn => {
                if (conn.request.getBody().includes('register')) {
                    expect(conn.request.getBody()).toEqual(JSON.stringify(eventWithDetails));
                }

                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(event)})));
            });

            mvtService.createEvent('myExpId1', 'register', details).then((response) => {
                expect(response).toEqual(event);
            }).catch((error) => {
                fail('should not have rejected the promised Event');
            });
        }));

        it('should report an error message on a failed creation', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify('error!')})));
            });

            mvtService.createEvent('myExpId1', 'visit').then((response) => {
                fail('should have rejected the promised Event');
            }).catch((error) => {
                expect(error).toEqual('error!');
            });
        }));
    });

    describe('getVariant', () => {
        beforeEach(() => {
            mockGa.trackerCallback.and.callFake((trackerCallback) => {
                let mockTracker = jasmine.createSpyObj('tracker', ['get']);
                mockTracker.get.and.returnValue('gaClientId');

                trackerCallback(mockTracker);
            });
        });

        it('should report each visit only once', async(() => {
            let expVisitReported1 = false;
            let expVisitReported2 = false;

            mockBackend.connections.subscribe(conn => {
                if (conn.request.url.includes('myExpId1')) {
                    expect(expVisitReported1).toBe(false);
                    expVisitReported1 = true;
                } else if (conn.request.url.includes('myExpId2')) {
                    expect(expVisitReported2).toBe(false);
                    expVisitReported2 = true;
                }
            });

            mvtService.getVariant('myExpId1');
            mvtService.getVariant('myExpId2');
            mvtService.getVariant('myExpId1');
            mvtService.getVariant('myExpId2');


            expect(expVisitReported1).toBe(true);
            expect(expVisitReported2).toBe(true);
        }));
    });
});
