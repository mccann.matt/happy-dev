// @author R. Matt McCann
// @brief Service for interacting with the MVT microservice
// @copyright &copy; 2017 R. Matt McCann

// import 'rxjs/add/operator/toPromise';

import { Injectable, Inject, OpaqueToken } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Experiment } from './experiment.model';
import { ExperimentEvent } from './experiment-event.model';
import { MvtStructureDefinition, ExperimentDefinition } from './mvt-structure-definition.models';
import { GoogleAnalytics } from '../../google/google-analytics';

export let MVT_API_ROOT_URL = new OpaqueToken('MVT_API_ROOT_URL');
export let MVT_STRUCTURE_DEFINITION = new OpaqueToken('MVT_STRUCTURE_DEFINITION');

@Injectable()
export class MvtService {
    // Used to configure API calls with app/json content type
    private _headers: Headers = new Headers({'Content-Type': 'application/json'});

    // Whether or not this unique visit has been registered with the MVT service
    private _isVisitReported: Map<string, boolean> = new Map<string, boolean>();

    private _variants: Map<string, string> = new Map<string, string>();

    constructor(
        private _http: Http,
        @Inject(MVT_API_ROOT_URL) private _apiRootUrl: string,
        @Inject(MVT_STRUCTURE_DEFINITION) private _structure: MvtStructureDefinition,
        private _ga: GoogleAnalytics) { }

    createExperiment(experimentId: string, description: string): Promise<Experiment> {
        let myExperiment = {
            id: experimentId,
            description: description
        };

        return this._http
            .post(this._apiRootUrl, JSON.stringify(myExperiment), {headers: this._headers})
            .toPromise()
            .then(
                function(response): Experiment {
                    return response.json();
                }, function(error): any {
                    throw error.json();
                }
            );
    }

    // Create a new experiment event
    createEvent(experimentId: string, actionType: string, details?: Map<string, any>): Promise<ExperimentEvent> {
        // If the experiment visit is being manually reported, disable the autoreport!
        if (actionType === 'visit') {
            this._isVisitReported[experimentId] = true;
        }

        let me = this;
        let promise = new Promise<ExperimentEvent>((resolve, reject) => {
            this._ga.trackerCallback(function(tracker) {
                let myEvent = {
                    action_type: actionType,
                    cohort: me._structure.experiments[experimentId].cohort,
                    variant: me.getVariant(experimentId),
                    visitor_id: tracker.get('clientId')
                };

                if (details) {
                    for (let detailKey in details) {
                        if (details.hasOwnProperty(detailKey)) {
                            myEvent[detailKey] = details[detailKey];
                        }
                    }
                }

                me._http.post(
                    me._apiRootUrl + experimentId + '/event/',
                    JSON.stringify(myEvent), {headers: me._headers}
                )
                .toPromise()
                .then(response => {
                    resolve(response.json());
                })
                .catch(error => {
                    reject(error.json());
                });
            });
        });

        return promise;
    }

    // Provides the variant id for this visitor.
    //
    // @param experimentId ID of the experiment
    // @returns The variant id
    getVariant(experimentId: string): string {
        // If the variants haven't been selected yet
        if (!this._variants[experimentId]) {
            this._selectVariants();
        }
        // If this unique visit hasn't been reported yet
        if (!this._isVisitReported[experimentId]) {
            this._isVisitReported[experimentId] = true;
            this._reportVisit(experimentId);
        }
        return this._variants[experimentId];
    }

    // Reports this visit to the MVT service
    //
    // @param experimentId The experiment that was triggered in the visit
    private _reportVisit(experimentId: string ): void {
        this.createEvent(experimentId, 'visit').then(
            () => {
                console.log('Reported visit to mvt');
            },
            (error) => {
                console.warn('Failed to report visit to mvt!');
                console.warn(error);
            });
    }

    // Selects the variants for all of the experiments
    private _selectVariants(): void {
        for (let experimentId of this._structure.experimentIds) {
            let experiment: ExperimentDefinition = this._structure.experiments[experimentId];
            let rollOfDice: number = Math.floor(Math.random() * 10000);

            for (let variant of experiment.variants) {
                if (variant.isSelected(rollOfDice)) {
                    console.log(
                        'Selected variant ' + variant.name + ' for experiment ' + experimentId);
                    this._variants[experimentId] = variant.name;
                    break;
                }
            }
        }
    }
}
