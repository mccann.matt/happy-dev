// @author R. Matt McCann
// @brief Contains model definitions of the structure of the MVT experiments
//     that are available.
// @copyright &copy; 2017 R. Matt McCann

// Definition of an individual varant within a single experiment
export class VariantDefinition {
    // Name of the variant, duh!
    name: string;

    // First value in the roll-of-the-dice range - Must be between 0 and 9999
    private _startRange: number;

    // Last value (inclusive) in the roll-of-the-dice range - Must be between 0 and 9999
    // and must be greater than or eqaul to startRange
    private _endRange: number;

    constructor(name: string, startRange: number, endRange: number) {
        if (10000 <= startRange) {
            throw RangeError('startRange(' + startRange + ') must be less than 10000');
        }
        if (0 > startRange) {
            throw RangeError('startRange(' + startRange + ') must be >= 0');
        }
        if (startRange > endRange) {
            throw RangeError(
                'startRange(' + startRange + ') must be <= endRange(' + endRange + ')');
        }
        if (10000 <= endRange) {
            throw RangeError('endRange(' + endRange + ') must be less than 10000');
        }
        if (name.length >= 64) {
            throw RangeError('name must be 64 characters or less');
        }
        if (name.length === 0) {
            throw RangeError('name must not be empty');
        }

        this.name = name;
        this._startRange = startRange;
        this._endRange = endRange;
    }

    // @params The random number for this variant pick; Must be in 0-9999
    // @returns True if the rollOfDice selects this variant as active
    isSelected(rollOfDice: number): boolean {
        if (0 > rollOfDice) {
            throw RangeError('rollOfDice(' + rollOfDice + ') must be >= 0');
        }
        if (10000 <= rollOfDice) {
            throw RangeError('rollOfDice(' + rollOfDice + ') must be < 10000');
        }

        return ((this._startRange <= rollOfDice) && (rollOfDice <= this._endRange));
    }

    get startRange(): number { return this._startRange; }

    get endRange(): number { return this._endRange; }
};

// Definition of an individual experiment
export interface ExperimentDefinition {
    // Which distinct group of new users to attribute this experiment activity to
    cohort: string;

    // The variations of the experiment
    variants: Array<VariantDefinition>;
};

export interface MvtStructureDefinition {
    experimentIds: Array<string>;
    experiments: Map<string, ExperimentDefinition>;
};
