// @author R. Matt McCann
// @brief Unit tests for the MVT structure definition models
// @copyright &copy; 2017 R. Matt McCann

import { VariantDefinition } from './mvt-structure-definition.models';

describe('VariantDefinition', () => {
    describe('constructor', () => {
        it('Should reject empty name values', () => {
            try {
                new VariantDefinition('', 0, 0);
                fail('RangeError should have been thrown');
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should reject long name values', () => {
            try {
                new VariantDefinition(new Array(65).join(' '), 0, 0);
                fail('RangeError should have been thrown');
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should reject negative startRange values', () => {
            try {
                new VariantDefinition('name', -1, 0);
                fail('RangeError should have been thrown');
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should reject really large startRange values', () => {
            try {
                new VariantDefinition('name', 10000, 0);
                fail('RangeError should have been thrown');
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should reject endRange values before startRange', () => {
            try {
                new VariantDefinition('name', 100, 1);
                fail('RangeError should have been thrown');
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should reject really large endRange values', () => {
            try {
                new VariantDefinition('name', 0, 10000);
                fail('RangeError should have been thrown');
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should accept good values', () => {
            new VariantDefinition('name', 1000, 2000);
        });
    });

    describe('isSelected', () => {
        it('Should reject low roll-of-dice values', () => {
            try {
                let variant = new VariantDefinition('name', 1000, 1999);
                variant.isSelected(-1);
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should reject high roll-of-dice values', () => {
            try {
                let variant = new VariantDefinition('name', 1000, 1999);
                variant.isSelected(10000);
            } catch (ex) {
                if (!(ex instanceof RangeError)) {
                    fail('Unexpected error thrown');
                }
            }
        });
        it('Should say no for roll-of-dice values outside of the selection range', () => {
            let variant = new VariantDefinition('name', 1000, 1999);

            expect(variant.isSelected(2000)).toBe(false);
        });
        it('Should say yes for roll-of-dice values inside of the selection range', () => {
            let variant = new VariantDefinition('name', 1000, 1999);

            expect(variant.isSelected(1500)).toBe(true);
        });
    });
});
