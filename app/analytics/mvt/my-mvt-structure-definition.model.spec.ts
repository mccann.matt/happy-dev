// @author R. Matt McCann
// @brief Unit tests for the MVT structure definition for HappyDev
// @copyrgiht &copy; 2017 R. Matt McCann

import { MyMvtStructureDefinition } from './my-mvt-structure-definition.model';


describe('MyMvtStructureDefinition', () => {
    describe('constructor', () => {
        it('should defined experiment variants that sum to 100% probability', () => {
            let def = new MyMvtStructureDefinition();

            for (let experimentKey in def.experiments) {
                if (def.experiments.hasOwnProperty(experimentKey)) {
                    let experiment = def.experiments[experimentKey];
                    let probabilitySlots = new Array<boolean>(10000).fill(false);


                    // Confirm all 10000(100%) probability slots are allocated
                    for (let variantIter = 0; variantIter < experiment.variants.length; variantIter++) {
                        let variant = experiment.variants[variantIter];

                        for (let slotIter = variant.startRange; slotIter <= variant.endRange; slotIter++) {
                            probabilitySlots[slotIter] = true;
                        }
                    }

                    expect(probabilitySlots).toEqual(new Array<boolean>(10000).fill(true));
                } else {
                    fail('da fuck? for..in filter tripped?');
                }
            }
        });
    });
});
