// @author R. Matt McCann
// @brief MVT structure definition for HappyDev
// @copyright &copy; 2017 R. Matt McCann

import { Injectable } from '@angular/core';
import {
    ExperimentDefinition, VariantDefinition, MvtStructureDefinition
} from './client/mvt-structure-definition.models';
//
// MVT structure definition for HappyDev
@Injectable()
export class MyMvtStructureDefinition implements MvtStructureDefinition {
    experimentIds: Array<string>;
    experiments: Map<string, ExperimentDefinition> = new Map<string, ExperimentDefinition>();

    constructor() {
        this.experimentIds = ['supply_side_value_hypotheses'];

        this.experiments['supply_side_value_hypotheses'] = {
            cohort: '2017_03_16+',
            variants: [
                new VariantDefinition('Hs0_Hs1_Hs2', 0, 999),
                new VariantDefinition('Hs0_Hs1_Hs3', 1000, 1999),
                new VariantDefinition('Hs0_Hs1_Hs4', 2000, 2999),
                new VariantDefinition('Hs0_Hs2_Hs3', 3000, 3999),
                new VariantDefinition('Hs0_Hs2_Hs4', 4000, 4999),
                new VariantDefinition('Hs0_Hs3_Hs4', 5000, 5999),
                new VariantDefinition('Hs1_Hs2_Hs3', 6000, 6999),
                new VariantDefinition('Hs1_Hs2_Hs4', 7000, 7999),
                new VariantDefinition('Hs1_Hs3_Hs4', 8000, 8999),
                new VariantDefinition('Hs2_Hs3_Hs4', 9000, 9999)
            ]
        };
    }
}
