/**
 * @author R. Matt McCann
 * @brief Configuration/definition file for Google Analytics custom tracking parameters
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from '@angular/core';


@Injectable()
export class GoogleAnalytics {
    /** Column id for the unique per browser id. */
    browserIdDimension = 'dimension1';

    /** Column id for the unique per user id. */
    userIdDimension = 'dimension2';

    /** Column id for the timestamp of the googlge analytics event. */
    utcMillisecsDimension = 'dimension3';

    /**
     * Sends the event to Google Analytics.
     *
     * @param category e.g. 'register_account'
     * @param action e.g. 'submit'
     */
    sendEvent(category: string, action: string): void {
        if (localStorage.getItem('isStaff') !== 'true') {
            ga('send', 'event', category, action, { utmMillisecsDimension: new Date().getTime() });
        }
    }

    /**
     * Sends a pageview to Google Analytics.
     */
    sendPageview(): void {
        if (localStorage.getItem('isStaff') !== 'true') {
            ga('send', 'pageview', {utcMillisecsDimension: new Date().getTime()});
        }
    }

    /**
     * Sets a tracker property.
     *
     * @param key Property to be set
     * @param value Value to set the property to
     */
    set(key: string, value: string): void {
        if (key === 'userId') {
            throw RangeError('Use setUserId instead of set!');
        }

        if (localStorage.getItem('isStaff') !== 'true') {
            ga('set', key, value);
        }
    }

    /**
     * Sets the user ID on the tracker.
     *
     * @param userId User ID value to set
     */
    setUserId(userId: string): void {
        if (localStorage.getItem('isStaff') !== 'true') {
            ga('set', 'userId', userId);
            ga('set', this.userIdDimension, userId);
        }
    }

    /**
     * Calls the callback with the Google Analytics tracker object.
     *
     * @param callback Function to be called with signature callback(tracker) { ... }
     */
    trackerCallback(callback: any): void {
        ga(function(tracker) {
            callback(tracker);
        });
    }
}
