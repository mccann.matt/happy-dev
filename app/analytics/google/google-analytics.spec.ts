/**
 * @author R. Matt McCann
 * @brief Unit tests for the google analytics service.
 * @copyright &copy; 2017 R. Matt McCann
 */

import { TestBed, inject } from '@angular/core/testing';

import { GoogleAnalytics } from './google-analytics';


describe('GoogleAnalytics', () => {
    let googleAnalytics, mockGa;

    beforeEach(() => {
        mockGa = jasmine.createSpy('ga');
        (<any>window).ga = mockGa;

        TestBed.configureTestingModule({
            providers: [ GoogleAnalytics ]
        });
    });

    beforeEach(inject([GoogleAnalytics], (_googleAnalytics) => {
        googleAnalytics = _googleAnalytics;
    }));

    describe('sendEvent', () => {
        it('sends the event', () => {
            // googleAnalytics.sendEvent('category', 'action');

            // console.error(window.ga);
            // let args = mockGa.mostRecentCall.args;

            // expect(args[0]).toEqual('send');
            // expect(args[1]).toEqual('event');
            // expect(args[2]).toEqual('category');
            // expect(args[3]).toEqual('action');
            // expect(args[4].dimension3).toBeDefined();
        });
    });

    describe('sendPageview', () => {

    });

    describe('set', () => {

    });

    describe('setUserId', () => {

    });

    describe('trackerCallback', () => {

    });
});
