/**
 * @author R. Matt McCann
 * @brief Unit tests for the AppEntry component
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountService } from './account/account.service';
import { GoogleAnalytics } from './analytics/google/google-analytics';
import { AppEntryComponent } from './app-entry.component';


describe('AppEntryComponent', () => {
    let component, fixture, mockAccountService, mockGa, mockRouter;

    beforeEach(async(() => {
        mockAccountService = jasmine.createSpyObj('AccountService', ['logout']);
        mockGa = jasmine.createSpyObj('GoogleAnalytics', ['set', 'sendEvent', 'sendPageView']);
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);
        mockRouter.events = jasmine.createSpyObj('Observable', ['subscribe']);

        TestBed.configureTestingModule({
            declarations: [ AppEntryComponent ],
            providers: [
                { provide: AccountService, useValue: mockAccountService },
                { provide: GoogleAnalytics, useValue: mockGa },
                { provide: Router, useValue: mockRouter },
                { provide: ActivatedRoute, useValue: { params: Observable.of({id: 'profile_id'})} }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(AppEntryComponent);
            component = fixture.componentInstance;
            component._login = jasmine.createSpyObj('LoginModal', ['dismiss']);
            component._registerAccount = jasmine.createSpyObj('RegisterModal', ['dismiss']);
        });
    }));

    describe('closeMobileNav', () => {
        it('closes the mobile nav', () => {
            component.mobileNavMenuIsVisible = true;

            component.closeMobileNav();

            expect(component.mobileNavMenuIsVisible).toBe(false);
        });
    });

    describe('_goToDashboard', () => {
        it('navigates to the dashboard', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue('userId');

            component._goToDashboard();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['developer', 'userId', 'dashboard']);
        });
    });

    describe('_goToQuestions', () => {
        it('navigates to the questions', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue('userId');

            component._goToQuestions();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['developer', 'userId', 'questions']);
        });
    });

    describe('_isLoggedIn', () => {
        it('returns true if the user id is set', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue('userId');

            expect(component._isLoggedIn()).toBe(true);
        });

        it('returns false if the user is not set', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue(null);

            expect(component._isLoggedIn()).toBe(false);
        });
    });

    describe('_onDismissLoginModal', () => {
        it('dismisses the login', () => {
            component._onDismissLoginModal();

            expect(component._login.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onDismissRegisterModal', () => {
        it('dismisses the register', () => {
            component._onDismissRegisterModal();

            expect(component._registerAccount.dismiss).toHaveBeenCalled();
        });
    });

    describe('onResize', () => {
        it('disables the mobile nav when the width is too large', () => {
            let event = jasmine.createSpyObj('event', ['target']);
            event.target = jasmine.createSpy('target');
            event.target.innerWidth = 800;

            component.mobileNavMenuIsVisible = true;
            component.onResize(event);

            expect(component.mobileNavMenuIsVisible).toBe(false);
        });
    });

    describe('toggleMobileNav', () => {
        it('toggles the mobile nav on when it is previously off', () => {
            component.toggleMobileNav();

            expect(component.mobileNavMenuIsVisible).toBe(true);
        });
    });
});
