/**
 * @author R. Matt McCann
 * @brief Primary component for the app
 * @copyright &copy; 2016 R. Matt McCann
 */

import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { KSSwiperContainer } from 'angular2-swiper';

import { RegisterComponent } from '../account/register.component';
import { GoogleAnalytics } from '../analytics/google/google-analytics';


@Component({
    selector: 'app',
    styleUrls: ['./app.component.scss'],
    templateUrl: 'app.component.html'
})
export class AppComponent {
    /** Register account component */
    @ViewChild('registerAccount') _registerComp: RegisterComponent;

    /** Register account holding modal. */
    @ViewChild('registerModal') _registerModal: ModalComponent;

    @ViewChild(KSSwiperContainer) _swiperContainer: KSSwiperContainer;

    _tourPageDescription1 = [
        'Setup your profile to describe your experiences, projects, frameworks, and passions.',
        'Answer a variety of questions about compensation, benefits, work / life balance, and your interests.',
        'Companies send you interview requests with offer details that satisfy your question answers.',
        'Weigh your offers and accept the job that will make you happiest.'
    ];

    _tourPageDescription2 = [
        'Think resume, but developer specific (mostly not boring) - or you can import your profile from other' +
            ' professional sites such as LinkedIn to save time.',
        'Companies see these answers and must agree upfront before requesting an interview.',
        'You decide whether it\'s worth your time to meet with these companies - no more wasted time, no ' +
            'more aggravation.',
        '...Or, if no offers hit the mark, keep fielding offers until you find the perfect fit.'
    ];

    /** Currently displayed tour page */
    _tourPageIdx = 0;

    _tourPageTitle = [
        'Describe Yourself', 'Tell Us Your Happy', 'Interview & Offers Together', 'Become A HappyDev'
    ];

    constructor(private _ga: GoogleAnalytics) { }

    desktopNavClass(pageIdx: number): any {
        return {'btn-tour-selected': this._tourPageIdx === pageIdx};
    }

    mobileCircleClass(pageIdx: number): any {
        return {
            'fa-circle': this._swiperContainer.swiper.activeIndex === pageIdx,
            'fa-circle-thin': this._swiperContainer.swiper.activeIndex !== pageIdx
        };
    }

    _onClickGetStartedNow(): void {
        this._ga.sendEvent('app', 'register_dev_account');

        this._registerComp.isDeveloperAccount = true;
        this._registerModal.open();
    }

    _onClickRegisterDeveloper(): void {
        this._ga.sendEvent('app', 'register_dev_account');

        this._registerComp.isDeveloperAccount = true;
        this._registerModal.open();
    }

    _onClickRegisterCompany(): void {
        this._ga.sendEvent('app', 'register_company');

        this._registerComp.isDeveloperAccount = false;
        this._registerModal.open();
    }

    _onClickTour(tourPageIdx: number): void {
        this._ga.sendEvent('app', String(tourPageIdx));

        this._tourPageIdx = tourPageIdx;
        this._swiperContainer.swiper.slideTo(tourPageIdx);
    }

    _onDismissRegisterModal(): void {
        this._registerComp.dismiss();
    }

    _onGetStartedHeroBanner(): void {
        this._ga.sendEvent('app', 'register_hero_banner');

        this._registerModal.open();
    }
}
