/**
 * @author R. Matt McCann
 * @brief Frequently asked questions page
 * @copyright &copy; 2016 R. Matt McCann
 */

import { Component } from '@angular/core';


@Component({
    selector: 'faq',
    templateUrl: 'faq.component.html'
})
export class FaqComponent {

}
