// @author R. Matt McCann
// @brief Unit tests for the AppComponent
// @copyright &copy; 2017 R. Matt McCann

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AppComponent } from './app.component';
import { GoogleAnalytics } from '../analytics/google/google-analytics';


describe('AppComponent', () => {
    let component, fixture, mockGa;
    let dummyElement = document.createElement('div');

    beforeEach(async(() => {
        mockGa = jasmine.createSpyObj('GoogleAnalytics', ['set', 'sendEvent', 'sendPageview']);

        TestBed.configureTestingModule({
                declarations: [ AppComponent ],
                schemas: [ NO_ERRORS_SCHEMA ],
                providers: [
                    { provide: GoogleAnalytics, useValue: mockGa },
                    { provide: ActivatedRoute, useValue: { url: Observable.of(['']) } }
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(AppComponent);
                component = fixture.componentInstance;
                component._registerComp = jasmine.createSpyObj('RegisterComp', ['dismiss']);
                component._registerComp.isDeveloperAccount = false;
                component._registerModal = jasmine.createSpyObj('RegisterComp', ['dismiss', 'open']);
                component._swiperContainer = jasmine.createSpy('swiperContainer');
                component._swiperContainer.swiper = jasmine.createSpyObj('swiper', ['slideTo']);
                component._swiperContainer.swiper.activeIndex = 0;
            });

        spyOn(dummyElement, 'scrollIntoView');
        document.getElementById = jasmine.createSpy('getElementById').and.returnValue(dummyElement);
    }));

    it('should be a defined component', () => {
        expect(true).toBe(true);
    });

    describe('mobileCircleClass', () => {
        it('returns filled circle styling when this is the current page', () => {
            let mobileCircleClass = component.mobileCircleClass(0);

            expect(mobileCircleClass['fa-circle']).toBe(true);
            expect(mobileCircleClass['fa-circle-thin']).toBe(false);
        });

        it('returns empty circle styling when this is not the current page', () => {
            let mobileCircleClass = component.mobileCircleClass(1);

            expect(mobileCircleClass['fa-circle']).toBe(false);
            expect(mobileCircleClass['fa-circle-thin']).toBe(true);
        });
    });

    describe('_onClickGetStartedNow', () => {
        it('notifies ga and raises the modal', () => {
            component._onClickGetStartedNow();

            expect(mockGa.sendEvent).toHaveBeenCalledWith('app', 'register_dev_account');
            expect(component._registerComp.isDeveloperAccount).toBe(true);
            expect(component._registerModal.open).toHaveBeenCalled();
        });
    });

    describe('_onClickRegisterDeveloper', () => {
        it('notifies ga and raises the modal', () => {
            component._onClickRegisterDeveloper();

            expect(mockGa.sendEvent).toHaveBeenCalledWith('app', 'register_dev_account');
            expect(component._registerComp.isDeveloperAccount).toBe(true);
            expect(component._registerModal.open).toHaveBeenCalled();
        });
    });

    describe('_onClickRegisterCompany', () => {
        it('notifies ga and raises the modal', () => {
            component._registerComp.isDeveloperAccount = true;

            component._onClickRegisterCompany();

            expect(mockGa.sendEvent).toHaveBeenCalledWith('app', 'register_company');
            expect(component._registerComp.isDeveloperAccount).toBe(false);
            expect(component._registerModal.open).toHaveBeenCalled();
        });
    });

    describe('_onClickTour', () => {
        it('notifies ga and changes the tour page', () => {
            component._onClickTour(1);

            expect(mockGa.sendEvent).toHaveBeenCalledWith('app', '1');
            expect(component._tourPageIdx).toBe(1);
            expect(component._swiperContainer.swiper.slideTo).toHaveBeenCalledWith(1);
        });
    });

    describe('_onDismissRegisterModal', () => {
        it('dismisses the modal', () => {
            component._onDismissRegisterModal();

            expect(component._registerComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onGetStartedHeroBanner', () => {
        it('notifies ga and raises the modal', () => {
            component._onGetStartedHeroBanner();

            expect(mockGa.sendEvent).toHaveBeenCalledWith('app', 'register_hero_banner');
            expect(component._registerModal.open).toHaveBeenCalled();
        });
    });

    describe('ngOnInit', () => {
        xit('should report to Google Analytics', () => {
            component.ngOnInit();

            expect(mockGa.set).toHaveBeenCalledWith('page', '');
            expect(mockGa.sendPageview).toHaveBeenCalled();
        });
    });

    describe('setter: audienceView', () => {
        xit('should report an audience view selection on selection', () => {
            component.audienceView = 'developers';

            let details = new Map<string, any>();
            details['audience_view_context'] = 'developers';

            expect(mockGa.sendEvent).toHaveBeenCalledWith('audienceView', 'select');

            component.audienceView = 'hiringCompanies';

            // 1st call - visit report, 2nd call - audience_view_select only once
            expect(mockGa.sendEvent).toHaveBeenCalledWith('audienceView', 'select');
        });
    });

    describe('function: scrollToSignup', () => {
        xit('should set audienceView if not selected', fakeAsync(() => {
            component.scrollToSignup();

            // Check that the audience view state was updated
            expect(component.audienceView).toEqual('developers');

            // Check that the signup form was brought into view
            fixture.detectChanges();
            tick(300);
            expect(dummyElement.scrollIntoView).toHaveBeenCalled();
        }));

        xit('should not set audienceView if selected', fakeAsync(() => {
            component.audienceView = 'hiringCompanies';
            component.scrollToSignup();

            // Check the audience view state wasn't changed
            expect(component.audienceView).toEqual('hiringCompanies');

            // Check that the signup form was brought into view
            fixture.detectChanges();
            tick(300);
            expect(dummyElement.scrollIntoView).toHaveBeenCalled();
        }));
    });

    describe('template: menu', () => {
        xit('clicking "Developers" should change the audienceView accordingly', fakeAsync(() => {
            fixture.detectChanges();

            // Click the "Developers" link
            let developersLink = fixture.nativeElement.querySelector('nav').children[0];
            developersLink.click();
            fixture.detectChanges();

            // Check that the audience view state was updated
            expect(component.audienceView).toEqual('developers');
        }));

        xit('clicking "Hiring Companies" should change the audienceView accordingly',
            fakeAsync(() => {
                fixture.detectChanges();

                // Click the "Hiring Companies" link
                let link = fixture.nativeElement.querySelector('nav').children[1];
                link.click();
                fixture.detectChanges();

                // Check that the audience view state was updated
                expect(component.audienceView).toEqual('hiringCompanies');
            })
        );

        xit('clicking "Join The Beta" should set the audience view and scroll', fakeAsync(() => {
            // Click the "Join THe Beta" link
            let betaLink = fixture.nativeElement.querySelector('nav').children[2];
            betaLink.click();
            fixture.detectChanges();

            // Check that the audience view state was updated
            expect(component.audienceView).toEqual('developers');

            // Check that the signup form was brought into view
            fixture.detectChanges();
            tick(300);
            expect(dummyElement.scrollIntoView).toHaveBeenCalled();
        }));
    });
});
