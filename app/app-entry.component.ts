// @author R. Matt McCann
// @brief Entry point for the app, simply handles off to router-outlet
// @copyright &copy; 2016 R. Matt McCann

import {
    Component, trigger, transition, style, animate, state, HostListener, OnInit, ViewChild
} from '@angular/core';
import { Router } from '@angular/router';

import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { AccountService } from './account/account.service';
import { GoogleAnalytics } from './analytics/google/google-analytics';
import { ModalFormComponent } from './misc/modal-form.component';


@Component({
    selector: 'app-entry',
    templateUrl: 'app-entry.component.html',
    styleUrls: ['app-entry.component.scss'],
    animations: [
        trigger(
          'fadeIn', [
              state('void', style({
                  opacity: 0,
                  display: 'none'
              })),
              state('*', style({
                  opacity: 1,
                  display: 'block'
              })),
              transition('void <=> *', animate('400ms ease-in-out')),
            ]
        )
    ]
})
export class AppEntryComponent implements OnInit {
    /** Login component. */
    @ViewChild('login') _login: ModalFormComponent;

    /** Login modal container. */
    @ViewChild('loginModal') _loginModal: ModalComponent;

    /** Marked as true when the mobile menu is revealed. */
    mobileNavMenuIsVisible = false;

    /** Register account component. */
    @ViewChild('registerAccount') _registerAccount: ModalFormComponent;

    /** Register account modal container. */
    @ViewChild('registerAccountModal') _registerAccountModal: ModalComponent;

    constructor(
        private _accountService: AccountService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _ga: GoogleAnalytics) {
        _router.events.subscribe((path: any) => {
            if (path.url.indexOf('#') === -1) {
                window.scrollTo(0, 0);
            }
        });
    }

    get _blogUrl(): string {
        return process.env.BLOG_URL;
    }

    /** Closes the mobile nav menu */
    closeMobileNav(): void {
        this.mobileNavMenuIsVisible = false;
    }

    /** Navigates to the user's dashboard */
    _goToDashboard(): void {
        // Report the navigation to analytics
        this._ga.sendEvent('app_entry', 'navigate_to_dashboard');

        this._router.navigate(['developer', localStorage.getItem('userId'), 'dashboard']);
    }

    /** Navigates to the user's questions panel */
    _goToQuestions(): void {
        // Report the navigation to analytics
        this._ga.sendEvent('app_entry', 'navigate_to_questions');

        this._router.navigate(['developer', localStorage.getItem('userId'), 'questions']);
    }

    /** Returns true if the user is logged in. */
    _isLoggedIn(): boolean {
        return (localStorage.getItem('userId') !== null);
    }

    /** Initialize the component. */
    ngOnInit(): void {
        // Send the pageview events to google analytics
        this._route.url.subscribe(url => {
            this._ga.set('page', url.join('/'));
            this._ga.sendPageview();
        });
    }

    /** When the "Benefits" link is clicked */
    _onClickBenefits(): void {
        this.closeMobileNav();
    }

    /** When the "Blog" link is clicked */
    _onClickBlog(): void {
        this.closeMobileNav();
    }

    /** When the "Contact Support" link is clicked. */
    _onClickContactSupport(): void {
        (<any>document.getElementsByClassName('chatlio-title-bar')[0]).click();
    }

    /** When the "FAQ" link is clicked */
    _onClickFAQ(): void {
        this.closeMobileNav();
    }

    /** When the "login" link is clicked */
    _onClickLogin(): void {
        this.closeMobileNav();
        this._loginModal.open();
    }

    /** When the "Sign Up" link is clicked */
    _onClickSignUp(): void {
        this.closeMobileNav();
        this._registerAccountModal.open();
    }

    /** When the "Tour" link is clicked */
    _onClickTour(): void {
        this.closeMobileNav();
    }

    /** Dismisses the login modal */
    _onDismissLoginModal(): void {
        this._login.dismiss();
    }

    /** Dismisses the register modal */
    _onDismissRegisterModal(): void {
        this._registerAccount.dismiss();
    }

    /** Handles window resize events */
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        let currentWidth = event.target.innerWidth;

        // If the width is big enough, hide the mobile nav
        if (this.mobileNavMenuIsVisible && currentWidth > 750) {
            this.mobileNavMenuIsVisible = false;
        }
    }

    /** Enables the mobile nav menu */
    toggleMobileNav(): void {
        this.mobileNavMenuIsVisible = !this.mobileNavMenuIsVisible;
    }
}
