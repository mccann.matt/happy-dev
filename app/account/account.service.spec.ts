/**
 * @author R. Matt McCann
 * @brief Unit tests for the AccountService
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, TestBed, inject } from '@angular/core/testing';
import { HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { Router } from '@angular/router';
import { MockBackend } from '@angular/http/testing';

import { AccountService } from './account.service';


describe('AccountService', () => {
    let accountService, mockBackend, mockRouter;

    beforeEach(() => {
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                AccountService,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: Router, useValue: mockRouter }
            ]
        });
    });

    beforeEach(inject([AccountService, XHRBackend], (_accountService, _mockBackend) => {
        accountService = _accountService;
        mockBackend = _mockBackend;
    }));

    describe('constructor', () => {
        it('correctly initializes the url', () => {
            expect(accountService._urlBase).toEqual('http://localhost:8001/account/');
        });
    });

    describe('amIStaff', () => {
        it('returns the correct value', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue('true');

            expect(accountService.amIStaff()).toBe(true);
        });
    });

    describe('login', () => {
        it('makes the request correctly', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/account/login/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.withCredentials).toBe(true);
                expect(conn.request.getBody()).toEqual(JSON.stringify({
                    'email': 'mccann.matt@gmail.com',
                    'password': 'password',
                    'remember_me': true
                }));

                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify({
                    'status': 'success',
                    'id': 'id'
                })})));
            });

            accountService.login('mccann.matt@gmail.com', 'password', true).then((response) => {
                // pass
            }).catch((error) => {
                fail('should not have rejected the promise');
            });
        }));

        it('saves the session cookie on successful api call', async(() => {
            spyOn(window.localStorage, 'setItem');

            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify({
                    'status': 'success',
                    'id': 'id',
                    'is_staff': true
                })})));
            });

            accountService.login('mccann.matt@gmail.com', 'password', true).then((response) => {
                expect(window.localStorage.setItem).toHaveBeenCalledWith('userId', 'id');
                expect(window.localStorage.setItem).toHaveBeenCalledWith('isStaff', true);
            }).catch((error) => {
                fail('should not have rejected the promise');
            });
        }));

        it('rejects on failed api call', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify('error')})));
            });

            accountService.login('mccann.matt@gmail.com', 'password', true).then((response) => {
                fail('should have rejected the promise');
            }).catch((error) => {
                // pass
            });
        }));
    });

    describe('logout', () => {
        it('makes the request correctly', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/account/logout/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.withCredentials).toBe(true);

                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify({'status': 'success'})})));
            });

            accountService.logout();
        }));

        it('clears the session cookie on successful api call', async(() => {
            spyOn(window.localStorage, 'removeItem');
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify({'status': 'success'})})));
            });


            accountService.logout()
                .then(() => {
                    expect(window.localStorage.removeItem).toHaveBeenCalledWith('userId');
                    expect(window.localStorage.removeItem).toHaveBeenCalledWith('isStaff');
                });
        }));

        it('routes back to the landing page', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify({'status': 'success'})})));
            });

            accountService.logout()
                .then(() => {
                    expect(mockRouter.navigate).toHaveBeenCalledWith(['/']);
                });
        }));
    });
});
