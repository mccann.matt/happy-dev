/**
 * @author R. Matt McCann
 * @brief CRUD service for the account model
 * @copyright &copy; 2016 R. Matt McCann
 */

import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Router } from '@angular/router';

import { CrudService } from '../misc/crud.service';
import { Account } from './account.model';


let accountUrl = process.env.API_SERVER + '/account/';

@Injectable()
export class AccountService extends CrudService<Account> {
    _headers = new Headers({'Content-Type': 'application/json', 'Set-Cookie': ''});

    _myHttp: Http;

    constructor(http: Http, private _router: Router) {
        super(http, '/account/');

        this._myHttp = http;
    }

    /**
     * @returns True if the currently logged in user is a staff account
     */
    amIStaff(): boolean {
        return (localStorage.getItem('isStaff') === 'true');
    }

    /**
     * Logins a user into their account.
     *
     * @param email Email of the user account
     * @param password Password of the user account
     * @param remember_me True if the session cookie should be permanent
     * @returns Promise to resolve with the user profile id if logged in, rejected if login failed
     */
    login(email: string, password: string, remember_me: boolean): Promise<string> {
        let data = {
            'email': email,
            'password': password,
            'remember_me': remember_me
        };

        return this._myHttp
            .post(accountUrl + 'login/', JSON.stringify(data), {headers: this._headers, withCredentials: true})
            .toPromise()
            .then(function(response): string {
                // Stash the user id for later use
                localStorage.setItem('userId', response.json()['id']);

                // Track whether this is a staff account
                localStorage.setItem('isStaff', response.json()['is_staff']);

                return response.json()['id'];
            }, function(error): any {
                return Promise.reject(error);
            });
    }

    /** Logs out a user from their account. */
    logout(): Promise<Response> {
        let promise = this._myHttp
            .post(accountUrl + 'logout/', undefined, { withCredentials: true})
            .toPromise();

        promise.then(() => {
                // Deactivate the session locally
                localStorage.removeItem('userId');
                localStorage.removeItem('isStaff');

                // Return to the landing page
                this._router.navigate(['/']);
            });

        return promise;
    }
};
