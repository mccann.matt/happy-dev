/**
 * @author R. Matt McCann
 * @brief Model for the user accounts
 * @copyright &copy; 2017 R. Matt McCann
 */

export interface Account {
    // TEMP(mmccann) - This is temporary to capture the company name on registration before
    // the full company side of the service is built
    company_name?: string;

    email: string;

    first_name: string;

    id?: string;

    is_developer_account: boolean;

    last_name: string;

    password: string;
};
