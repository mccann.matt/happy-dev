/**
 * @author R. Matt McCann
 * @brief Unit tests for the login component
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { GoogleAnalytics } from '../analytics/google/google-analytics';
import { LoginComponent } from './login.component';
import { AccountService } from './account.service';
import { FormBuilderService } from '../misc/form-builder.service';
import { FormFieldErrorsComponent } from '../misc/form-field-errors.component';
import { checkAcceptValid, checkEmptyRequirement, itFA } from '../misc/testing/form-test';


describe('LoginComponent', () => {
    let component, fixture, mockAccountService, mockGa, mockRouter;

    beforeEach(async(() => {
        mockAccountService = jasmine.createSpyObj('AccountService', ['login']);
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent', 'setUserId']);
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [ FormFieldErrorsComponent, LoginComponent ],
                providers: [
                    { provide: AccountService, useValue: mockAccountService },
                    { provide: GoogleAnalytics, useValue: mockGa },
                    { provide: Router, useValue: mockRouter },
                    FormBuilderService
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(LoginComponent);
                component = fixture.componentInstance;
                component.onClose = jasmine.createSpyObj('CloseEmitter', ['emit']);
            });
    }));

    describe('_init', () => {
        itFA('builds the form', () => {
            fixture.detectChanges();

            expect(component._form).toBeDefined();
            expect(component._form.value.email).toBeDefined();
            expect(component._form.value.password).toBeDefined();
            expect(component._form.value.remember_me).toBeDefined();
        });

        itFA('attaches onFormChanged to form.valueChanges', () => {
            spyOn(component, '_onFormChanged');

            fixture.detectChanges();

            component._form.controls.email.markAsDirty();
            component._form.controls.email.setValue('mccann.matt@gmail.com');

            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        });
    });

    describe('_onFormChanged', () => {
        itFA('clears the login failed error message', () => {
            fixture.detectChanges();
            component._loginFailed = true;

            component._onFormChanged();

            expect(component._loginFailed).toBe(false);
        });

        itFA('invalidates empty email values', () => {
            checkEmptyRequirement(fixture, component, 'email', 'Email');
        });

        itFA('invalidates empty password values', () => {
            checkEmptyRequirement(fixture, component, 'password', 'Password');
        });

        itFA('accepts valid email values', () => {
            checkAcceptValid(fixture, component, 'email');
        });

        itFA('accepts valid password values', () => {
            checkAcceptValid(fixture, component, 'password');
        });
    });

    describe('_onSubmit', () => {
        beforeEach(fakeAsync(() => {
            component.redirectUrl = ['developer', 'PROFILE_ID'];
            mockAccountService.login.and.callFake((email, password) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb('profileId');
                    }
                };
            });

            fixture.detectChanges();
            tick(1000);
        }));

        itFA('clears the error messages on submit', () => {
            component._loginFailed = true;
            component._unexpectedError = true;

            component._onSubmit();
            tick(1000); // form.reset timer

            expect(component._loginFailed).toBe(false);
            expect(component._unexpectedError).toBe(false);
        });

        itFA('calls the api correctly', () => {
            component._form.controls.email.markAsDirty();
            component._form.controls.email.setValue('mccann.matt@gmail.com');
            component._form.controls.password.markAsDirty();
            component._form.controls.password.setValue('password');
            component._form.controls.remember_me.markAsDirty();
            component._form.controls.remember_me.setValue(true);
            tick(1000);
            fixture.detectChanges();

            component._onSubmit();
            tick(1000);

            expect(mockAccountService.login).toHaveBeenCalledWith('mccann.matt@gmail.com', 'password', true);
        });

        itFA('emits the close signal on a successful login', () => {
            component._onSubmit();
            tick(1000);

            expect(component.onClose.emit).toHaveBeenCalled();
        });

        itFA('resets the form', () => {
            spyOn(component._form, 'reset');

            component._onSubmit();
            tick(1000);

            expect(component._form.reset).toHaveBeenCalled();
        });

        itFA('navigates to the redirect page on a successful login', () => {
            component._onSubmit();
            tick(1000);

            expect(mockRouter.navigate).toHaveBeenCalledWith(['developer', 'profileId']);
        });

        itFA('reveals the login failed message on a failed login', () => {
            mockAccountService.login.and.callFake((email, password) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb({status: 403});
                    }
                };
            });

            fixture.detectChanges();
            component._onSubmit();
            tick(1000);

            expect(component._loginFailed).toBe(true);
        });

        itFA('reveals the unexpected error message on an unexpected error', () => {
            mockAccountService.login.and.callFake(() => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb({status: 500});
                    }
                };
            });

            fixture.detectChanges();
            component._onSubmit();
            tick(1000);

            expect(component._unexpectedError).toBe(true);
        });
    });

    describe('template-input: email field', () => {
        let field;
        let errors;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            field = fixture.nativeElement.querySelector('#email');
            errors = fixture.nativeElement.querySelector('#emailErrors');
        }));

        itFA('positively styles field when a valid input is provided', () => {
            component._form.controls.email.setValue('matt@matt.com');
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(true);
            expect(field.classList.contains('ng-invalid')).toBe(false);
            expect(errors.children.length).toBe(0);
        });

        itFA('negatively styles field when an invalid input is provided', () => {
            component._form.controls.email.setValue('');
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(false);
            expect(field.classList.contains('ng-invalid')).toBe(true);
        });

        itFA('reveals the error message when a bad input occurs', () => {
            component._form.controls.email.setValue('');
            component._form.controls.email.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(errors.children.length).toBe(1);
        });
    });

    describe('template-input: password field', () => {
        let field;
        let errors;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            field = fixture.nativeElement.querySelector('#password');
            errors = fixture.nativeElement.querySelector('#passwordErrors');
        }));

        itFA('is a password type field', () => {
            expect(field.type).toEqual('password');
        });

        itFA('positively styles field when a valid input is provided', () => {
            component._form.controls.password.setValue('matt@matt.com');
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(true);
            expect(field.classList.contains('ng-invalid')).toBe(false);
            expect(errors.children.length).toBe(0);
        });

        itFA('negatively styles feild when an invalid input is provided', () => {
            component._form.controls.password.setValue('');
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(false);
            expect(field.classList.contains('ng-invalid')).toBe(true);
        });

        itFA('reveals the error message when a bad input occurs', () => {
            component._form.controls.password.setValue('');
            component._form.controls.password.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(errors.children.length).toBe(1);
        });
    });

    describe('template-input: login button', () => {
        let button;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            button = fixture.nativeElement.querySelector('#loginButton');
        }));

        itFA('is enabled when the form is valid', () => {
            component._form.controls.email.setValue('matt@matt.com');
            component._form.controls.email.markAsDirty();
            component._form.controls.password.setValue('password');
            component._form.controls.password.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(false);
        });

        itFA('is disabled when the form is invalid', () => {
            expect(button.disabled).toBe(true);
        });

        itFA('fires _onSubmit() when clicked', () => {
            spyOn(component, '_onSubmit');

            component._form.controls.email.setValue('matt@matt.com');
            component._form.controls.email.markAsDirty();
            component._form.controls.password.setValue('password');
            component._form.controls.password.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            button.click();

            expect(component._onSubmit).toHaveBeenCalled();
        });
    });

    describe('template-view: login failed message', () => {
        itFA('revealed when the login failed flag is set', () => {
            component._loginFailed = true;
            fixture.detectChanges();
            tick(1000);

            expect(fixture.nativeElement.querySelector('#loginFailed')).not.toBe(null);
        });

        itFA('hidden when the login failed flag is not set', () => {
            component._loginFailed = false;
            fixture.detectChanges();
            tick(1000);

            expect(fixture.nativeElement.querySelector('#loginFailed')).toBe(null);
        });
    });

    describe('template-view: unexpected error message', () => {
        itFA('revealed when the unexpected error flag is set', () => {
            component._unexpectedError = true;
            fixture.detectChanges();
            tick(1000);

            expect(fixture.nativeElement.querySelector('#unexpectedError')).not.toBe(null);
        });

        itFA('hidden when the unexpected error flag is not set', () => {
            component._unexpectedError = false;
            fixture.detectChanges();
            tick(1000);

            expect(fixture.nativeElement.querySelector('#unexpectedError')).toBe(null);
        });
    });
});
