/**
 * @author R. Matt McCann
 * @brief Provides an interface for the user to login to their account
 * @copyright &copy; 2017 R. Matt McCann
 */

import 'rxjs/add/operator/debounceTime';

import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { GoogleAnalytics } from '../analytics/google/google-analytics';
import { FormBuilderService } from '../misc/form-builder.service';
import { ModalFormComponent } from '../misc/modal-form.component';
import { AccountService } from './account.service';


@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class LoginComponent extends ModalFormComponent {
    /** Error message revealed indicating the user's login crednetials were incorrect. */
    _loginFailed = false;

    /** Url to redirect to after a successful login. */
    @Input() public redirectUrl: Array<string>;

    constructor(
        _formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder,
        private _accountService: AccountService,
        private _router: Router,
    ) {
        super(_formBuilder);
    }

    /** Initialize the component. */
    _init(): void {
        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(['email', 'password', 'remember_me']);

        // Configure the form
        this._validationMessages['email'] =
            this._formBuilder.buildValidatorMessages('email', ['required']);
        this._validationMessages['password'] =
            this._formBuilder.buildValidatorMessages('password', ['required']);

        // Define the validation rules for the form
        let validationRules = new Map<string, Array<Validator>>();
        validationRules['email'] = [Validators.required];
        validationRules['password'] = [Validators.required];

        // Build the form
        let formGroup = {
            'email': new FormControl('', validationRules['email']),
            'password': new FormControl('', validationRules['password']),
            'remember_me': new FormControl(false, [])
        };
        this._form = this._ngFormBuilder.group(formGroup);

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Process changes in the input form. */
    _onFormChanged(): void {
        // Clear the error messages
        this._loginFailed = false;
        this._unexpectedError = false;

        // Process the form changes
        this._formBuilder.onFormChanged(this._form, this._formState, this._validationMessages);
    }

    /** Process the form submission. */
    _onSubmit(): void {
        // Clear the error messages
        this._loginFailed = false;
        this._unexpectedError = false;

        // Try to log the user in
        this._accountService.login(this._form.value.email, this._form.value.password, this._form.value.remember_me)
            .then((profileId) => {
                // Set the user id with analytics
                this._ga.setUserId(profileId);

                // Report the account login with analytics
                this._ga.sendEvent('login', 'success');

                // Insert the profile ID into the redirection url
                for (let iter = 0; iter < this.redirectUrl.length; iter++) {
                    if (this.redirectUrl[iter] === 'PROFILE_ID') {
                        this.redirectUrl[iter] = profileId;
                    }
                }

                // Put away the login modal
                this.onClose.emit();

                // Reset the form contents
                this._form.reset();

                // Redirect to the logged in page
                this._router.navigate(this.redirectUrl);
            }, (error) => {
                // If this is an authentication failure
                if (error.status === 403) {
                    // Reveal the login failed message
                    this._loginFailed = true;
                // If this is an unexpected error
                } else {
                    // Reveal the unexpected error message
                    this._unexpectedError = true;

                    // Report the unexpected error to analytics
                    this._ga.sendEvent('login', 'error');
                }
            });
    }
};
