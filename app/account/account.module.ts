/**
 * @author R. Matt McCann
 * @brief Defines the Angular module for the account functionality
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReCaptchaModule } from 'angular2-recaptcha';

import { DeveloperModule } from '../developer/developer.module';
import { SharedModule } from '../misc/shared.module';
import { AccountService } from './account.service';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';

export let MODULE_DEF = {
    imports: [
        CommonModule,
        DeveloperModule,
        FormsModule,
        ReactiveFormsModule,
        ReCaptchaModule,
        SharedModule
    ],
    exports: [ LoginComponent, RegisterComponent ],
    declarations: [ LoginComponent, RegisterComponent ],
    providers: [ AccountService ]
};

@NgModule(MODULE_DEF)
export class AccountModule { }
