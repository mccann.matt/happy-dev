/**
 * @author R. Matt McCann
 * @brief Provides a user interface for registering an account
 * @copyright &copy; 2016 R. Matt McCann
 */

import 'rxjs/add/operator/debounceTime';

import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';

import { ReCaptchaComponent } from 'angular2-recaptcha/lib/captcha.component';

import { GoogleAnalytics } from '../analytics/google/google-analytics';
import { DeveloperService } from '../developer/developer.service';
import { FormBuilderService } from '../misc/form-builder.service';
import { ModalFormComponent } from '../misc/modal-form.component';
import { AccountService } from './account.service';


@Component({
    selector: 'register-account',
    styleUrls: ['register.component.scss'],
    templateUrl: './register.component.html'
})
export class RegisterComponent extends ModalFormComponent {
    /** Marked true after the account has successfully been created. */
    _accountCreated = false;

    /** Recaptcha component used to prevent register spam. */
    @ViewChild(ReCaptchaComponent) _captcha: ReCaptchaComponent;

    /** Marked true when the Captcha challenge is completed. */
    _captchaAccepted = false;

    /** Whether or not the account register mode is for a developer or a hiring company */
    _isDeveloperAccount = true;

    set isDeveloperAccount(_isDeveloperAccount: boolean) {
        this._isDeveloperAccount = _isDeveloperAccount;

        this._init();
    }

    constructor(
        private _accountService: AccountService,
        private _developerService: DeveloperService,
        _formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder,
        private _router: Router
    ) {
        super(_formBuilder);
    }

    /** Initialize the object state */
    _init(): void {
        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(
            ['first_name', 'last_name', 'company', 'email', 'password', 'recaptcha']);

        // Configure the form
        this._validationMessages['first_name'] =
            this._formBuilder.buildValidatorMessages('first_name', ['required', 'pattern', ['maxlength', 25]]);
        this._validationMessages['last_name'] =
            this._formBuilder.buildValidatorMessages('last_name', ['required', 'pattern', ['maxlength', 25]]);
        this._validationMessages['company'] =
            this._formBuilder.buildValidatorMessages('company', ['required', ['maxlength', 25]]);
        this._validationMessages['email'] =
            this._formBuilder.buildValidatorMessages('email', ['required', 'pattern', ['maxlength', 64]]);
        this._validationMessages['password'] =
            this._formBuilder.buildValidatorMessages('password', ['required', ['minlength', 8], ['maxlength', 64]]);

        let validationRules = new Map<string, Array<Validator>>();
        validationRules['first_name'] = [
            Validators.required, Validators.maxLength(25), Validators.pattern('[-.\' a-zA-Z]+')];
        validationRules['last_name'] = [
            Validators.required, Validators.maxLength(25), Validators.pattern('[-.\' a-zA-Z]+')];
        validationRules['company'] = [Validators.required, Validators.maxLength(25)];
        validationRules['email'] = [
            Validators.required, Validators.maxLength(64),
            Validators.pattern('[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*')];
        validationRules['password'] = [Validators.required, Validators.minLength(8), Validators.maxLength(64)];

        // Build the form
        if (this._isDeveloperAccount) {
            let formGroup = {
                'first_name': [undefined, validationRules['first_name']],
                'last_name': [undefined, validationRules['last_name']],
                'email': [undefined, validationRules['email']],
                'password': [undefined, validationRules['password']]
            };

            this._form = this._ngFormBuilder.group(formGroup);
        } else {
            let formGroup = {
                'first_name': [undefined, validationRules['first_name']],
                'last_name': [undefined, validationRules['last_name']],
                'company': [undefined, validationRules['company']],
                'email': [undefined, validationRules['email']],
                'password': [undefined, validationRules['password']]
            };

            this._form = this._ngFormBuilder.group(formGroup);
        }

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Handle the reCaptcha response event */
    _handleCorrectCaptcha(event?: any) {
        this._captchaAccepted = true;
        this._formState['recaptcha'].error = '';
    }

    /** On user clicking the "Register as a developer" link */
    _onClickRegisterAsDeveloper(): void {
        this._isDeveloperAccount = true;
        this._init();
    }

    /** On user clicking the "Register as a hiring company" link */
    _onClickRegisterAsHiringCompany(): void {
        this._isDeveloperAccount = false;
        this._init();
    }

    /** When the user clicks the "Submit" button */
    _onSubmit() {
        // If we don't have a valid recaptcha token
        if (this._captcha.getResponse() === '') {
            this._formState['recaptcha'].error = 'reCaptcha must be completed to sign-up';
            this._captchaAccepted = false;

            return;
        }

        // Pack the account properties
        let accountIn = this._form.value;
            accountIn['is_developer_account'] = this._isDeveloperAccount;

        // Create the account record
        this._accountService.create(accountIn).then(
            // If the account was created successfully
            (account) => {
                // Set the user id with analytics
                this._ga.setUserId(account.id);

                // Report the account registration with analytics
                this._ga.sendEvent('register_account', 'success');

                // If this is a developer account
                if (this._isDeveloperAccount) {
                    // Create the developer profile record
                    this._developerService.create({id: account.id})
                        .then(
                            // If the account creation succeeds
                            () => {
                                let rememberMe = false;

                                // Mark in analytics that this is a developer registration
                                this._ga.sendEvent('register_account', 'developer');

                                // Log the user into their account
                                this._accountService.login(account.email, accountIn.password, rememberMe)
                                    .then(() => {
                                        // Navigate to the mostly unconfigured profile page
                                        this._router.navigate(['developer', account.id, 'dashboard']);

                                        // Close the modal
                                        this.onClose.emit();

                                        // Reset the state of the component
                                        this._init();
                                    }, (error) => {
                                        // Failed to login! Reveal the unknown error message
                                        this._unexpectedError = true;

                                        // Report the unexpected error to analytics
                                        this._ga.sendEvent('register_account', 'login_error');
                                    });
                            },
                            // If the account creation fails
                            () => {
                                // Reveal the error message
                                this._unexpectedError = true;

                                // Report the unexpected error to analytics
                                this._ga.sendEvent('register_account', 'dev_create_error');
                            }
                        );
                // If this is a hiring company account
                } else {
                    // Mark in analytics that this is a company registration
                    this._ga.sendEvent('register_account', 'company');

                    // Reveal the account created message to the company account
                    this._accountCreated = true;

                    // Reset the state of the component
                    this._init();

                    // After showing the message for a bit
                    setTimeout(() => {
                        // Close the modal
                        this.onClose.emit();

                        // Wait for the modal to close
                        setTimeout(() => {
                            // Reset the account created state
                            this._accountCreated = false;
                        }, 2000);
                    }, 5000);
                }
            },
            // If the account was not created successfully
            (error) => {
                // If the email was a duplicate
                if (error.message === 'duplicate email') {
                    // Reveal the duplicate email error
                    this._formState['email'].error = ['Email already registered!'];
                // If this is an unknown error
                } else {
                    // Reveal the unknown error message
                    this._unexpectedError = true;

                    // Report the unexpected error to analytics
                    this._ga.sendEvent('register_account', 'account_create_error');
                }
            }
        );
    }
}
