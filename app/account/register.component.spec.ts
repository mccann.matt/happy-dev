/**
 * @author R. Matt McCann
 * @brief Unit tests for the ProfileCreateComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { GoogleAnalytics } from '../analytics/google/google-analytics';
import { DeveloperService } from '../developer/developer.service';
import { FormBuilderService } from '../misc/form-builder.service';
import { FormFieldErrorsComponent } from '../misc/form-field-errors.component';
import {
    beforeEachFormUiTest, checkAcceptValid, checkEmptyRequirement, checkLongRequirement, itFA,
    checkNegativelyStyles, checkPositivelyStyles, checkMinLengthRequirement,
    checkNaturalEnglishRequirement
} from '../misc/testing/form-test';
import { AccountService } from './account.service';
import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
    let component, fixture, mockAccountService, mockCaptcha, mockDeveloperService, mockGa, mockRouter;

    beforeEach(async(() => {
        mockAccountService = jasmine.createSpyObj('AccountService', ['create', 'login']);
        mockCaptcha = jasmine.createSpyObj('Captcha', ['getResponse']);
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['create']);
        mockGa = jasmine.createSpyObj('GoogleAnalytics', ['setUserId', 'sendEvent']);
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
                imports: [ FormsModule, ReactiveFormsModule ],
                declarations: [ FormFieldErrorsComponent, RegisterComponent ],
                providers: [
                    { provide: AccountService, useValue: mockAccountService },
                    { provide: DeveloperService, useValue: mockDeveloperService },
                    { provide: GoogleAnalytics, useValue: mockGa },
                    { provide: Router, useValue: mockRouter},
                    FormBuilderService
                ],
                schemas: [ NO_ERRORS_SCHEMA ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(RegisterComponent);
                component = fixture.componentInstance;
                component._captcha = mockCaptcha;
                component.onClose = jasmine.createSpyObj('onCloseEmitter', ['emit']);
            });
    }));

    describe('_init', () => {
        itFA('builds the form when in developer mode', () => {
            component._init();

            expect(component._form).toBeDefined();
            expect(component._form.value.first_name).toBeDefined();
            expect(component._form.value.last_name).toBeDefined();
            expect(component._form.value.email).toBeDefined();
            expect(component._form.value.password).toBeDefined();
        });

        itFA('builds the form when in company mode', () => {
            component._isDeveloperAccount = false;
            component._init();

            expect(component._form).toBeDefined();
            expect(component._form.value.first_name).toBeDefined();
            expect(component._form.value.last_name).toBeDefined();
            expect(component._form.value.company).toBeDefined();
            expect(component._form.value.email).toBeDefined();
            expect(component._form.value.password).toBeDefined();
        });

        itFA('attaches onFormChanged to form.valueChanges', () => {
            spyOn(component, '_onFormChanged');

            fixture.detectChanges();

            component._form.controls.first_name.markAsDirty();
            component._form.controls.first_name.setValue('asdf');

            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        });
    });

    describe('_handleCorrectCaptcha', () => {
        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
        }));

        itFA('should mark the captcha as accepted', () => {
            component._handleCorrectCaptcha();

            expect(component._captchaAccepted).toBe(true);
        });

        itFA('should clear the captch error message', () => {
            component._handleCorrectCaptcha();

            expect(component._formState.recaptcha.error).toEqual('');
        });
    });

    describe('_onClickRegisterAsDeveloper', () => {
        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
        }));

        itFA('switches the account mode to developer', () => {
            component._isDeveloperAccount = false;

            component._onClickRegisterAsDeveloper();

            expect(component._isDeveloperAccount).toBe(true);
        });

        itFA('resets the form state', () => {
            spyOn(component, '_init');

            component._onClickRegisterAsDeveloper();

            expect(component._init).toHaveBeenCalled();
        });
    });

    describe('_onClickRegisterAsHiringCompany', () => {
        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
        }));

        itFA('switches the account mode to company', () => {
            component._isDeveloperAccount = true;

            component._onClickRegisterAsHiringCompany();

            expect(component._isDeveloperAccount).toBe(false);
        });

        itFA('resets the form state', () => {
            spyOn(component, '_init');

            component._onClickRegisterAsHiringCompany();

            expect(component._init).toHaveBeenCalled();
        });
    });

    describe('_onFormChanged', () => {
        itFA('accepts valid first name values', () => {
            checkAcceptValid(fixture, component, 'first_name');
        });

        itFA('accepts valid last name values', () => {
            checkAcceptValid(fixture, component, 'last_name');
        });

        itFA('accepts valid company values', () => {
            component._isDeveloperAccount = false;

            checkAcceptValid(fixture, component, 'company');
        });

        itFA('accepts valid email values', () => {
            checkAcceptValid(fixture, component, 'email', 'mccann.matt@gmail.com');
        });

        itFA('accepts valid password values', () => {
            checkAcceptValid(fixture, component, 'password', 'Password123');
        });

        itFA('rejects missing first name values', () => {
            checkEmptyRequirement(fixture, component, 'first_name', 'First name');
        });

        itFA('rejects missing last name values', () => {
            checkEmptyRequirement(fixture, component, 'last_name', 'Last name');
        });

        itFA('rejects missing company values', () => {
            component._isDeveloperAccount = false;

            checkEmptyRequirement(fixture, component, 'company', 'Company');
        });

        itFA('rejects missing email values', () => {
            checkEmptyRequirement(fixture, component, 'email', 'Email');
        });

        itFA('rejects missing password values', () => {
            checkEmptyRequirement(fixture, component, 'password', 'Password');
        });

        itFA('rejects too long first name values', () => {
            checkLongRequirement(fixture, component, 'first_name', 25, 'First name');
        });

        itFA('rejects too long last name values', () => {
            checkLongRequirement(fixture, component, 'last_name', 25, 'Last name');
        });

        itFA('rejects too long company values', () => {
            component._isDeveloperAccount = false;

            checkLongRequirement(fixture, component, 'company', 25, 'Company');
        });

        itFA('rejects too long email values', () => {
            fixture.detectChanges();

            component._form.controls['email'].markAsDirty();
            component._form.controls['email'].setValue(new Array(66).join('A') + '@gmail.com');

            tick(1000);

            expect(component._form.controls['email'].valid).toBe(false);
            expect(component._formState['email'].accepted).toBe(false);
            expect(component._formState['email'].error).toEqual(
                ['Email cannot be more than 64 chacters long']);
        });

        itFA('rejects too long password values', () => {
            checkLongRequirement(fixture, component, 'password', 64, 'Password');
        });

        itFA('rejects mispatterned first name values', () => {
            checkNaturalEnglishRequirement(fixture, component, 'first_name', 'First name');
        });

        itFA('rejects mispatterned last name values', () => {
            checkNaturalEnglishRequirement(fixture, component, 'last_name', 'Last name');
        });

        itFA('rejects mispatterned email values', () => {
            fixture.detectChanges();

            component._form.controls['email'].markAsDirty();
            component._form.controls['email'].setValue('!!!!!!!@@!@');

            tick(1000);

            expect(component._form.controls['email'].valid).toBe(false);
            expect(component._formState['email'].accepted).toBe(false);
            expect(component._formState['email'].error).toEqual(
                ['Email does not appear to be valid']);
        });

        itFA('rejects too short password values', () => {
            checkMinLengthRequirement(fixture, component, 'password', 8, 'Password');
        });
    });

    describe('_onSubmit', () => {
        let submitValue;

        beforeEach(fakeAsync(() => {
            submitValue = {
                first_name: 'first_name',
                last_name: 'last_name',
                email: 'matt@matt.com',
                password: 'password'
            };

            mockAccountService.create.and.returnValue({
                    then: (then_cb, catch_cb) => {
                        then_cb({
                            id: 'woof',
                            email: 'matt@matt.com',
                            password: 'password'
                        });
                    }
                });
            mockDeveloperService.create.and.returnValue({
                then: (then_cb, catch_cb) => { }
            });

            component._captcha.getResponse.and.returnValue('someValue');

            fixture.detectChanges();
            component._form.setValue(submitValue);
            tick(1000);
        }));

        itFA('reject submit when captcha not set', () => {
            component._captcha.getResponse.and.returnValue('');

            component._captchaAccepted = true;
            component._onSubmit();

            expect(component._captchaAccepted).toBe(false);
            expect(component._formState.recaptcha.error).toEqual('reCaptcha must be completed to sign-up');
            expect(mockAccountService.create).not.toHaveBeenCalled();
        });

        it('tries to create the account record', () => {
            component._onSubmit();

            submitValue['is_developer_account'] = true;
            expect(mockAccountService.create).toHaveBeenCalledWith(submitValue);
        });

        describe('after successful account create', () => {
            itFA('reports to google analytics', () => {
                component._onSubmit();

                submitValue['is_developer_account'] = true;
                expect(mockAccountService.create).toHaveBeenCalledWith(submitValue);
                expect(mockGa.setUserId).toHaveBeenCalledWith('woof');
                expect(mockGa.sendEvent).toHaveBeenCalledWith('register_account', 'success');
            });

            describe('developer account mode', () => {
                beforeEach(() => {
                    mockAccountService.login.and.returnValue({
                            then: (then_cb, catch_cb) => { }
                        });

                    component._isDeveloperAccount = true;
                });

                it('creates the developer account record', () => {
                    component._onSubmit();

                    expect(mockDeveloperService.create).toHaveBeenCalledWith({'id': 'woof'});
                });

                describe('after successful create', () => {
                    beforeEach(() => {
                        mockDeveloperService.create.and.returnValue({
                            then: (then_cb, catch_cb) => {
                                then_cb();
                            }
                        });
                    });

                    it('logs the user into the account', () => {
                        let rememberMe = false;

                        component._onSubmit();

                        expect(mockAccountService.login).toHaveBeenCalledWith('matt@matt.com', 'password', rememberMe);
                    });

                    describe('after successful login', () => {
                        beforeEach(() => {
                            mockAccountService.login.and.returnValue({
                                    then: (then_cb, catch_cb) => { then_cb(); }
                                });
                        });

                        it('navigates to the dashboard page', () => {
                            component._onSubmit();

                            expect(mockRouter.navigate).toHaveBeenCalledWith(['developer', 'woof', 'dashboard']);
                        });

                        it('fires the close modal event', () => {
                            component._onSubmit();

                            expect(component.onClose.emit).toHaveBeenCalled();
                        });

                        it('resets the component state', () => {
                            spyOn(component, '_init');

                            component._onSubmit();

                            expect(component._init).toHaveBeenCalled();
                        });
                    });

                    describe('after failed login', () => {
                        beforeEach(() => {
                            mockAccountService.login.and.returnValue({
                                then: (then_cb, catch_cb) => { catch_cb(); }
                            });
                        });

                        it('reveals the unexpected error message', () => {
                            component._onSubmit();

                            expect(component._unexpectedError).toBe(true);
                        });
                    });
                });

                describe('after failed create', () => {
                    beforeEach(() => {
                        mockDeveloperService.create.and.returnValue({
                            then: (then_cb, catch_cb) => { catch_cb(); }
                        });
                    });

                    it('reveals the unknown error message', () => {
                        component._onSubmit();

                        expect(component._unexpectedError).toBe(true);
                    });
                });
            });

            describe('hiring company account mode', () => {
                beforeEach(() => {
                    component._isDeveloperAccount = false;
                });

                itFA('marks the account as created', () => {
                    component._onSubmit();

                    expect(component._accountCreated).toBe(true);
                    tick(7000);
                });

                itFA('resets the form state', () => {
                    spyOn(component, '_init');

                    component._onSubmit();

                    expect(component._init).toHaveBeenCalled();
                    tick(7000);
                });

                itFA('closes the modal after a while', () => {
                    component._onSubmit();
                    tick(5000);

                    expect(component.onClose.emit).toHaveBeenCalled();
                    tick(2000);
                });

                itFA('clears the account created state after a while longer', () => {
                    component._onSubmit();
                    tick(7000);

                    expect(component._accountCreated).toBe(false);
                });
            });
        });

        describe('after failed account create', () => {
            it('reveals the duplicate email error message when a duplicate email is sent', () => {
                mockAccountService.create.and.returnValue({
                    then: (then_cb, catch_cb) => {
                        catch_cb({
                            'message': 'duplicate email'
                        });
                    }
                });

                component._onSubmit();

                expect(component._formState['email'].error).toEqual(['Email already registered!']);
            });

            it('reveals the unexpected error when anything other than an duplicate email error is returned', () => {
                mockAccountService.create.and.returnValue({
                    then: (then_cb, catch_cb) => {
                        catch_cb({
                            'message': 'error'
                        });
                    }
                });

                component._onSubmit();

                expect(component._unexpectedError).toBe(true);
            });
        });
    });

    describe('template: Register as a hiring company', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            elem = fixture.nativeElement.querySelector('#registerAsHiringCompany');
        }));

        it('revealed when isDeveloperAccount', () => {
            expect(elem).not.toBe(null);
        });

        itFA('hidden when not isDeveloperAccount', () => {
            component._isDeveloperAccount = false;
            component._init();
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#registerAsHiringCompany')).toBe(null);
        });

        it('changes isDeveloperAccount to false and rebuilds form when clicked', () => {
            spyOn(component, '_init');

            elem.click();

            expect(component._isDeveloperAccount).toBe(false);
            expect(component._init).toHaveBeenCalled();
        });
    });

    describe('template: Register as a developer', () => {
        itFA('revealed when not isDeveloperAccount and account not created', () => {
            component._isDeveloperAccount = false;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#registerAsDeveloper')).not.toBe(null);
        });

        itFA('hidden when not isDeveloperAccount and account is created', () => {
            component._isDeveloperAccount = false;
            component._accountCreated = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#registerAsDeveloper')).toBe(null);
        });

        itFA('hidden when isDeveloperAccount', () => {
            component._isDeveloperAccount = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#registerAsDeveloper')).toBe(null);
        });

        itFA('changes isDeveloperAccount to true and rebuilds form when clicked', () => {
            component._isDeveloperAccount = false;
            fixture.detectChanges();

            spyOn(component, '_init');

            fixture.nativeElement.querySelector('#registerAsDeveloper').click();

            expect(component._isDeveloperAccount).toBe(true);
            expect(component._init).toHaveBeenCalled();
        });
    });

    describe('template: Account created message', () => {
        itFA('revealed when account created', () => {
            component._accountCreated = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#accountCreated')).not.toBe(null);
        });

        itFA('hidden when account not yet created', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#accountCreated')).toBe(null);
        });
    });

    describe('template: first name field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'first_name');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.first_name.setValue('matt');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.first_name.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: last name field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'last_name');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.last_name.setValue('mccann');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.last_name.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: company field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            component._isDeveloperAccount = false;

            elem = beforeEachFormUiTest(fixture, 'company');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.company.setValue('happydev');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.company.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: email field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'email');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.email.setValue('matt@happydev.io');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.email.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: password field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'password');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.password.setValue('password');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.password.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: recaptcha field', () => {
        xit('fires _handleCorrectCaptcha when captcha is solved', () => {

        });

        itFA('reveals error message when bad input is provided', () => {
            fixture.detectChanges();
            component._formState.recaptcha.error = 'woof';
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#recaptchaError')).not.toBe(null);
        });
    });

    describe('template: unexpected error message', () => {
        itFA('revealed when unexpected error set', () => {
            component._unexpectedError = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).not.toBe(null);
        });

        itFA('hidden when unexpected error is not set', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).toBe(null);
        });
    });

    describe('template: get started button', () => {
        let button;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            component._form.controls.first_name.setValue('matt');
            component._form.controls.last_name.setValue('mccann');
            component._form.controls.email.setValue('matt@happydev.io');
            component._form.controls.password.setValue('password');
            component._captchAccepted = true;
            tick(1000);
            fixture.detectChanges();

            button = fixture.nativeElement.querySelector('#getStartedButton');
        }));

        itFA('disabled when form is invalid', () => {
            component._form.controls.first_name.setValue('');
            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(true);
        });

        itFA('enabled when form is valid', () => {
            expect(button.disabled).toBe(false);
        });

        itFA('fires _onSubmit when clicked', () => {
            spyOn(component, '_onSubmit');

            button.click();

            expect(component._onSubmit).toHaveBeenCalled();
        });
    });
});
