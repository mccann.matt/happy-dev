// Angular
import '@angular/animations';
import '@angular/core';
import '@angular/forms';
import '@angular/http';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/common';
import '@angular/router';

// RxJs
import 'rxjs';

// Import ng2-bs3-modal and its deps
import 'jquery';
import 'bootstrap';
import 'ng2-bs3-modal/ng2-bs3-modal';

async function loadFont() {
    let webfont: any = require('webfontloader');
    let config: any = {
      typekit: { id: 'ovm4olu'}
    };
    webfont.load(config);
}
loadFont();
