/**
 * @author R. Matt McCann
 * @brief CRUD service for the DeveloperProfile model
 * @copyright &copy; 2016 R. Matt McCann
 */

import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { buildGetSuggestions } from '../misc/suggestion.service';
import { Company } from './company.model';

@Injectable()
export class CompanyService {
    private _companyUrl = process.env.API_SERVER + '/company/';
    private _headers = new Headers({'Content-Type': 'application/json'});

    getSuggestions: (fragment: any) => Observable<Company[]>;

    constructor(private http: Http) {
        this.getSuggestions = buildGetSuggestions<Company>(this._companyUrl + 'suggest/', http);
    }

    /**
     * Saves a new Company instance to the database.
     *
     * @param company Company to be created
     * @returns Promise to return the created Company, or reject with an error message
     */
    create(company: Company): Promise<Company> {
        return this.http
            .post(this._companyUrl, JSON.stringify(company), {headers: this._headers, withCredentials: true})
            .toPromise()
            .then(function(response): Company {
                return response.json();
            }).catch(function(response): any {
                return Promise.reject(response.json());
            });
    }

    /**
     * Retrieve a Company instance.
     *
     * @param id Tracking ID of the company
     * @returns Promise to return the requested company, or reject with an error message
     */
    get(id: string): Promise<Company> {
        const url = `${this._companyUrl}${id}/`;

        return this.http
            .get(url)
            .toPromise()
            .then(response => response.json())
            .catch(error => Promise.reject(error.json()));
    }

    /**
     * Update the company instance changes to the database.
     *
     * @param company The company to be saved
     * @return Promise to return the saved company, or reject with an error message
     */
    update(company: Company): Promise<Company> {
        const url = `${this._companyUrl}${company.id}/`;

        return this.http
            .put(url, JSON.stringify(company), {headers: this._headers, withCredentials: true})
            .toPromise()
            .then(response => response.json())
            .catch(error => Promise.reject(error.json()));
    }
};
