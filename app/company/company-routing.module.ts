/**
 * @author R. Matt McCann
 * @brief Routing module for the company module
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompanyComponent } from './company.component';

const routes: Routes = [
    { path: '', component: CompanyComponent },
    { path: ':id/edit', component: CompanyComponent },
    { path: ':id', component: CompanyComponent }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class CompanyRoutingModule { }
