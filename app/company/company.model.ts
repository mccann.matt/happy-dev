/**
 * @author R. Matt McCann
 * @brief Model for hiring companies
 * @copyright &copy; 2017 R. Matt McCann
 */

/** Basic information about hiring companies. */
export interface Company {
    /** Description about the company. */
    about: string;

    /** City in which the company is hiring. */
    city: string;

    /** Tracking id of the company. */
    id?: string;

    /** Url of the company URL. */
    logo_url?: string;

    /** Name of the company. */
    name: string;

    /** State in which the company is hiring. */
    state: string;

    /** Number of employees at the company. */
    number_of_employees: number;

    /** Year the company was founded. */
    year_founded: number;

    /** Url of the company's website. */
    website_url: string;
};
