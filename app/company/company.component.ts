/**
 * @author R. Matt McCann
 * @brief Interface for creating company entries
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/never';
import 'rxjs/add/operator/debounceTime';

import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validator, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params, UrlSegment } from '@angular/router';

import { AccountService } from '../account/account.service';
import { Company } from './company.model';
import { CompanyService} from './company.service';
import { FormBuilderService, FieldState } from '../misc/form-builder.service';

@Component({
    selector: 'company',
    templateUrl: 'company.component.html'
})
export class CompanyComponent implements OnInit {
    @Input() company: Company;

    _form: FormGroup;

    _formGroup: {[key: string]: any};

    _formState: Map<string, FieldState>;

    @Input() isEmbedded: boolean;

    _isEditMode = false;

    _unknownError = false;

    _validationMessages = new Map<string, Map<string, string>>();

    constructor(
        private _accountService: AccountService,
        private _companyService: CompanyService,
        private _formBuilder: FormBuilderService,
        private _ngFormBuilder: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        // @Inject(WindowToken) private _window: Window
    ) { }

    _buildForm(): void {
        let company = this.company;
        if (company === undefined) {
            company = {
                'about': undefined,
                'website_url': undefined,
                'city': undefined,
                'logo_url': undefined,
                'name': undefined,
                'state': undefined,
                'number_of_employees': undefined,
                'year_founded': undefined
            };
        }

        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState([
            'about', 'website_url', 'city', 'name', 'state', 'number_of_employees', 'year_founded']);

        // Configure the form
        this._validationMessages['about'] =
            this._formBuilder.buildValidatorMessages('about', ['required', ['maxlength', 4096]]);
        this._validationMessages['website_url'] =
            this._formBuilder.buildValidatorMessages('website_url', ['required', ['maxlength', 64]]);
        this._validationMessages['city'] =
            this._formBuilder.buildValidatorMessages('city', ['required', ['maxlength', 64]]);
        this._validationMessages['name'] =
            this._formBuilder.buildValidatorMessages('name', ['required', ['maxlength', 64]]);
        this._validationMessages['state'] =
            this._formBuilder.buildValidatorMessages('state', ['required']);
        this._validationMessages['number_of_employees'] =
            this._formBuilder.buildValidatorMessages('number_of_employees', ['required', 'pattern']);
        this._validationMessages['year_founded'] =
            this._formBuilder.buildValidatorMessages('year_founded', ['required', 'pattern']);

        let validationRules = new Map<string, Array<Validator>>();
        validationRules['about'] = [Validators.required, Validators.maxLength(4096)];
        validationRules['website_url'] = [Validators.required, Validators.maxLength(64)];
        validationRules['city'] = [Validators.required, Validators.maxLength(64)];
        validationRules['name'] = [Validators.required, Validators.maxLength(64)];
        validationRules['state'] = [Validators.required];
        validationRules['number_of_employees'] = [Validators.required, Validators.pattern('[0-9]*')];
        validationRules['year_founded'] = [Validators.required, Validators.pattern('[0-9]*')];

        this._formGroup = {
            'about': new FormControl(company.about, validationRules['about']),
            'website_url': new FormControl(company.website_url, validationRules['website_url']),
            'city': new FormControl(company.city, validationRules['city']),
            'name': new FormControl(company.name, validationRules['name']),
            'state': new FormControl(company.state, validationRules['state']),
            'number_of_employees': new FormControl(company.number_of_employees, validationRules['number_of_employees']),
            'year_founded': new FormControl(company.year_founded, validationRules['year_founded'])
        };

        // Build the form
        this._form = this._ngFormBuilder.group(this._formGroup);

        // Subscribe to the form value changes
        this._form.valueChanges
            .debounceTime(1000)
            .subscribe(data => this._onFormChanged());
    }

    _cancel(): void {
        // Reset the form state
        this._form.controls['about'].setValue(this.company.about);
        this._form.controls['website_url'].setValue(this.company.website_url);
        this._form.controls['city'].setValue(this.company.city);
        this._form.controls['name'].setValue(this.company.name);
        this._form.controls['state'].setValue(this.company.state);
        this._form.controls['number_of_employees'].setValue(this.company.number_of_employees);
        this._form.controls['year_founded'].setValue(this.company.year_founded);

        // Return to the view state
        this._isEditMode = false;
    }

    _isCreateMode(): boolean {
        return !this.company;
    }

    _isEdittable(): boolean {
        return this._accountService.amIStaff();
    }

    _isViewMode(): boolean {
        return this.company && !this._isEditMode;
    }

    _goToCompanyWebsite(): void {
        // this._window.location.href = this.company.website_url;
    }

    ngOnInit(): void {
        this._route.url
            .subscribe((urlFragments: UrlSegment[]) => {
                // If the url hit ends in 'edit'
                if (urlFragments.length && (urlFragments[urlFragments.length - 1].path === 'edit')) {
                    // Put the component in edit mode
                    this._isEditMode = true;
                }
            });

        this._route.params
            .switchMap((params: Params) => {
                // If the company id is in the params
                if (params['id']) {
                    let promise = this._companyService.get(params['id']);

                    return Observable.fromPromise(promise);
                } else {
                    // Build the form to create a new record
                    this._buildForm();

                    return Observable.never();
                }
            })
            .subscribe(
                company => {
                    // Let's work on the fetched company record
                    this.company = company;

                    // Build the form for editing the record
                    this._buildForm();
                },
                error => {
                    // The requested company does not exist
                    this._router.navigate(['/404'], {skipLocationChange: true});
                }
            );
    }

    /** Process changes in the input form. */
    _onFormChanged(): void {
        this._unknownError = false;
        this._formBuilder.onFormChanged(this._form, this._formState, this._validationMessages);
    }

    /** Process the form submission. */
    _onSubmit(): void {
        // If this is create mode
        if (this._isCreateMode()) {
            this._companyService.create(this._form.value)
                .then((company) => {
                    this._router.navigate(['company', company.id]);
                }, (error) => {
                    // Reveal the unknown error message
                    this._unknownError = true;
                });
        // If this is edit mode
        } else {
            // Pack the form input into the entry object
            for (let field in this._form.value) {
                if (this._form.value.hasOwnProperty(field) && this._form.value[field]) {
                    this.company[field] = this._form.value[field];
                }
            }
            this._companyService.update(this.company)
                .then((company) => {
                    this._isEditMode = false;
                    this._router.navigate(['company', this.company.id]);
                }, (error) => {
                    // Reveal the unknown error message
                    this._unknownError = true;
                });
        }
    }
};
