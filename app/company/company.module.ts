/**
 * @author R. Matt McCann
 * @brief Defines the Angualr2 module for the company views
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CompanyComponent } from './company.component';
import { CompanyRoutingModule } from './company-routing.module';
import { CompanyService } from './company.service';
import { SharedModule } from '../misc/shared.module';

export let MODULE_DEF = {
    imports: [
        CommonModule,
        CompanyRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    exports: [ CompanyComponent ],
    declarations: [
        CompanyComponent
    ],
    providers: [
        CompanyService
    ]
};

@NgModule(MODULE_DEF)
export class CompanyModule { }
