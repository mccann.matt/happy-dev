/**
 * @author R. Matt McCann
 * @brief Unit tests for the CompanyComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountService } from '../account/account.service';
import { CompanyComponent } from './company.component';
import { CompanyService } from './company.service';
import { FormBuilderService } from '../misc/form-builder.service';
import { FormFieldErrorsComponent } from '../misc/form-field-errors.component';
import {
 beforeEachFormUiTest, checkAcceptValid, checkEmptyRequirement, checkLongRequirement, itFA,
 checkNegativelyStyles, checkPositivelyStyles, checkRevealsErrorMessage, checkNumericRequirement
} from '../misc/testing/form-test';
import { NewLinesToBrPipe } from '../misc/new-lines-to-br.pipe';
import { WindowToken } from '../misc/window';


describe('CompanyComponent', () => {
    let company, component, fixture, mockAccountService, mockCompanyService, mockRoute, mockRouter, mockWindow;

    beforeEach(async(() => {
        company = {
            'about': 'about',
            'website_url': 'bob.com/website_url',
            'city': 'city',
            'logo_url': 'logo_url',
            'name': 'name',
            'state': 'state',
            'number_of_employees': 2,
            'year_founded': 2017,
            'id': 'id'
        };
        mockAccountService = jasmine.createSpyObj('AccountService', ['amIStaff']);
        mockCompanyService = jasmine.createSpyObj('CompanyService', ['create', 'get', 'update']);
        mockRoute = jasmine.createSpy('ActivatedRoute');
        mockRoute.url = jasmine.createSpyObj('url', ['subscribe']);
        mockRoute.params = jasmine.createSpyObj('params', ['switchMap']);
        mockRoute.params.switchMap.and.returnValue({
            subscribe: (then_cb, catch_cb) => {}
        });
        mockRouter = jasmine.createSpyObj('router', ['navigate']);
        mockWindow = jasmine.createSpy('window');
        mockWindow.location = jasmine.createSpy('location');
        mockWindow.location.href = 'woof.html';

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [ CompanyComponent, FormFieldErrorsComponent, NewLinesToBrPipe ],
                providers: [
                    { provide: AccountService, useValue: mockAccountService },
                    { provide: CompanyService, useValue: mockCompanyService },
                    { provide: ActivatedRoute, useValue: mockRoute },
                    { provide: Router, useValue: mockRouter },
                    { provide: WindowToken, useValue: mockWindow },
                    FormBuilderService
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(CompanyComponent);
                component = fixture.componentInstance;
                component.closeEmitter = jasmine.createSpyObj('CloseEmitter', ['emit']);
            });
    }));

    describe('_buildForm', () => {
        it('builds the form', () => {
            component._buildForm();

            expect(component._form).toBeDefined();
            expect(component._form.controls.about).toBeDefined();
            expect(component._form.controls.website_url).toBeDefined();
            expect(component._form.controls.city).toBeDefined();
            expect(component._form.controls.name).toBeDefined();
            expect(component._form.controls.state).toBeDefined();
            expect(component._form.controls.number_of_employees).toBeDefined();
            expect(component._form.controls.year_founded).toBeDefined();
        });

        itFA('attaches _onFormChanged to form.valueChanges', () => {
            spyOn(component, '_onFormChanged');

            component._buildForm();
            component._form.controls.name.markAsDirty();
            component._form.controls.name.setValue('aasdf');

            fixture.detectChanges();
            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        });

        it('sets the form values to the existing company properties', () => {
            component.company = company;
            component._buildForm();

            expect(component._form.value.about).toEqual('about');
            expect(component._form.value.website_url).toEqual('bob.com/website_url');
            expect(component._form.value.city).toEqual('city');
            expect(component._form.value.name).toEqual('name');
            expect(component._form.value.state).toEqual('state');
            expect(component._form.value.number_of_employees).toEqual(2);
            expect(component._form.value.year_founded).toEqual(2017);
        });
    });

    describe('_cancel', () => {
        beforeEach(fakeAsync(() => {
            component._buildForm();
            component.company = company;
            fixture.detectChanges();
        }));

        itFA('rebuilds the form', () => {
            component._form.controls.name.setValue('woof');
            fixture.detectChanges();
            tick(1000);

            component._cancel();
            fixture.detectChanges();
            tick(1000);

            expect(component._form.value.name).toEqual('name');
        });

        it('returns to view state', () => {
            component._isEditMode = true;

            component._cancel();

            expect(component._isEditMode).toBe(false);
        });
    });

    describe('_goToCompanyWesite', () => {
        it('points the browser to the company website', () => {
            component.company = company;
            component._goToCompanyWebsite();

            // expect(mockWindow.location.href).toEqual(company.website_url);
        });
    });

    describe('_isCreateMode', () => {
        it('returns true when company is not defined', () => {
            expect(component._isCreateMode()).toEqual(true);
        });
    });

    describe('_isEdittable', () => {
        it('returns true when the user is staff', () => {
            mockAccountService.amIStaff.and.returnValue(true);

            expect(component._isEdittable()).toBe(true);
        });
    });

    describe('_isViewMode', () => {
        it('returns true when the company exists but its not in edit mode', () => {
            component.company = company;

            expect(component._isViewMode()).toBe(true);
        });
    });

    describe('ngOnInit', () => {
        beforeEach(() => {
            let urlFragments = [
                    jasmine.createSpyObj('fragment', ['path']),
                    jasmine.createSpyObj('fragment', ['path'])
                ];
            urlFragments[0].path = 'company';
            urlFragments[1].path = 'id';
            mockRoute.url.subscribe.and.callFake((func) => {
                func(urlFragments);
            });

            mockRoute.params.switchMap.and.callFake((func) => {
                let params = {'id': 'id'};

                return func(params);
            });

            mockCompanyService.get.and.returnValue(
                new Promise((resolve, reject) => {
                    resolve(company);
                })
            );
        });

        itFA('puts the component into edit mode with an edit url', () => {
            let urlFragments = [
                    jasmine.createSpyObj('fragment', ['path']),
                    jasmine.createSpyObj('fragment', ['path']),
                    jasmine.createSpyObj('fragment', ['path'])
                ];
            urlFragments[0].path = 'company';
            urlFragments[1].path = 'id';
            urlFragments[2].path = 'edit';
            mockRoute.url.subscribe.and.callFake((func) => {
                func(urlFragments);
            });

            component.ngOnInit();
            tick();

            expect(component._isEditMode).toBe(true);
        });

        itFA('does not put the component into edit mode without an edit url', () => {
            component.ngOnInit();
            tick();

            expect(component._isEditMode).toBe(false);
        });

        itFA('fetches the company record when id in the url', () => {
            spyOn(component, '_buildForm');

            component.ngOnInit();
            tick();

            expect(component.company).toBe(company);
            expect(component._buildForm).toHaveBeenCalled();
        });

        itFA('reveals a 404 error when the company record does not exist', () => {
            mockRoute.params.switchMap.and.callFake((func) => {
                let params = {'id': 'id'};

                return func(params);
            });

            mockCompanyService.get.and.returnValue(
                Promise.reject('error')
            );

            component.ngOnInit();
            tick();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['/404'], {skipLocationChange: true});
        });

        itFA('builds the form when this is a new company record', () => {
            mockRoute.params.switchMap.and.callFake((func) => {
                let params = {};

                return func(params);
            });

            spyOn(component, '_buildForm');

            component.ngOnInit();
            tick();

            expect(component.company).not.toBeDefined();
            expect(component._buildForm).toHaveBeenCalled();
        });
    });

    describe('_onFormChanged', () => {
        beforeEach(() => {
            component._buildForm();
        });

        it('clears the unknown error message', () => {
            component._unknownError = true;

            component._onFormChanged();

            expect(component._unknownError).toBe(false);
        });

        itFA('accepts valid name values', () => {
            checkAcceptValid(fixture, component, 'name');
        });

        itFA('accepts valid website url values', () => {
            checkAcceptValid(fixture, component, 'website_url');
        });

        itFA('accepts valid city values', () => {
            checkAcceptValid(fixture, component, 'city');
        });

        itFA('accepts valid state values', () => {
            checkAcceptValid(fixture, component, 'state');
        });

        itFA('accepts valid number of employee values', () => {
            checkAcceptValid(fixture, component, 'number_of_employees', 2);
        });

        itFA('accepts valid year founded values', () => {
            checkAcceptValid(fixture, component, 'year_founded', 2017);
        });

        itFA('rejects missing name values', () => {
            checkEmptyRequirement(fixture, component, 'name', 'Name');
        });

        itFA('rejects missing website url values', () => {
            checkEmptyRequirement(fixture, component, 'website_url', 'Website url');
        });

        itFA('rejects missing city values', () => {
            checkEmptyRequirement(fixture, component, 'city', 'City');
        });

        itFA('rejects missing state values', () => {
            checkEmptyRequirement(fixture, component, 'state', 'State');
        });

        itFA('rejects missing number of employee values', () => {
            checkEmptyRequirement(fixture, component, 'number_of_employees', 'Number of employees');
        });

        itFA('rejects missing year founded values', () => {
            checkEmptyRequirement(fixture, component, 'year_founded', 'Year founded');
        });

        itFA('rejects too long about values', () => {
            checkLongRequirement(fixture, component, 'about', 4096, 'About');
        });

        itFA('rejects too long website url values', () => {
            checkLongRequirement(fixture, component, 'website_url', 64, 'Website url');
        });

        itFA('rejects too long city values', () => {
            checkLongRequirement(fixture, component, 'city', 64, 'City');
        });

        itFA('rejects too long name values', () => {
            checkLongRequirement(fixture, component, 'name', 64, 'Name');
        });

        itFA('rejects non-numeric number of employee values', () => {
            checkNumericRequirement(fixture, component, 'number_of_employees', 'Number of employees');
        });

        itFA('rejects non-numeric year found values', () => {
            checkNumericRequirement(fixture, component, 'year_founded', 'Year founded');
        });
    });

    describe('_onSubmit', () => {
        beforeEach(() => {
            component._buildForm();

            mockCompanyService.create.and.callFake((companyIn) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(company);
                    }
                };
            });
        });

        it('updates the company record if it previously existed', () => {
            mockCompanyService.update.and.callFake((companyIn) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(company);
                    }
                };
            });

            component.company = company;
            component._buildForm();
            component._onSubmit();

            expect(mockCompanyService.update).toHaveBeenCalled();
        });

        it('creates a new company record if it did not previously exist', () => {
            component._onSubmit();

            expect(mockCompanyService.create).toHaveBeenCalled();
        });

        it('routes to a view of the company on success', () => {
            component._onSubmit();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['company', company.id]);
        });

        it('reveals the unknown error message on create failure', () => {
            mockCompanyService.create.and.callFake((returnedCompany) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb('error');
                    }
                };
            });

            component._onSubmit();

            expect(component._unknownError).toBe(true);
        });

        it('reveals the unknown error message on update failure', () => {
            mockCompanyService.update.and.callFake((returnedCompany) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb('error');
                    }
                };
            });

            component.company = company;
            component._buildForm();
            component._onSubmit();

            expect(component._unknownError).toBe(true);
        });

    });

    describe('template: create mode', () => {
        beforeEach(fakeAsync(() => {
            component._buildForm();
            fixture.detectChanges();
        }));

        describe('template-input: create button', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = fixture.nativeElement.querySelector('#createButton');

                component._form.controls.about.setValue('about');
                component._form.controls.website_url.setValue('website_url');
                component._form.controls.city.setValue('city');
                component._form.controls.name.setValue('name');
                component._form.controls.state.setValue('state');
                component._form.controls.number_of_employees.setValue(2);
                component._form.controls.year_founded.setValue(2017);
                fixture.detectChanges();
                tick(1000);
            }));

            itFA('reveals when in create mode', () => {
                expect(elem).not.toBe(null);
            });

            itFA('disabled when the form is not valid', () => {
                component._form.controls.city.setValue('');
                fixture.detectChanges();
                tick(1000);

                expect(elem.disabled).toBe(true);
            });

            itFA('enabled when the form is valid', () => {
                expect(elem.disabled).toBe(false);
            });

            itFA('fires _onSubmit when clicked', () => {
                spyOn(component, '_onSubmit');

                elem.click();

                expect(component._onSubmit).toHaveBeenCalled();
            });
        });

        describe('template-input: save button', () => {
            itFA('hidden when not in edit mode', () => {
                expect(fixture.nativeElement.querySelector('#saveButton')).toBe(null);
            });
        });

        describe('template-input: cancel button', () => {
            itFA('hidden when not in edit mode', () => {
                expect(fixture.nativeElement.querySelector('#cancelButton')).toBe(null);
            });
        });
    });

    describe('template: edit mode', () => {
        beforeEach(fakeAsync(() => {
            component.company = company;
            component._isEditMode = true;
            component._buildForm();
            fixture.detectChanges();
        }));

        describe('template-input: save button', () => {
            let elem;

            beforeEach(() => {
                elem = fixture.nativeElement.querySelector('#saveButton');
            });

            itFA('reveals when in edit mode', () => {
                expect(elem).not.toBe(null);
            });

            itFA('disabled when the form is not valid', () => {
                component._form.controls.city.setValue('');
                fixture.detectChanges();
                tick(1000);

                expect(elem.disabled).toBe(true);
            });

            itFA('enabled when the form is valid', () => {
                expect(elem.disabled).toBe(false);
            });

            itFA('fires _onSubmit when clicked', () => {
                spyOn(component, '_onSubmit');

                elem.click();

                expect(component._onSubmit).toHaveBeenCalled();
            });
        });

        describe('template-input: cancel button', () => {
            let elem;

            beforeEach(() => {
                elem = fixture.nativeElement.querySelector('#cancelButton');
            });

            itFA('reveals when in view mode', () => {
                expect(elem).not.toBe(null);
            });

            itFA('fires _cancel when clicked', () => {
                spyOn(component, '_cancel');

                elem.click();

                expect(component._cancel).toHaveBeenCalled();
            });
        });
    });

    describe('template: create/edit mode', () => {
        beforeEach(fakeAsync(() => {
            component.company = company;
            component._isEditMode = true;
            component._buildForm();
            fixture.detectChanges();
        }));

        itFA('reveals create/edit form when in edit mode', () => {
            expect(fixture.nativeElement.querySelector('#createOrEdit')).not.toBe(null);
        });

        describe('template-input: create button', () => {
            itFA('hidden when not in create mode', () => {
                expect(fixture.nativeElement.querySelector('#createButton')).toBe(null);
            });
        });

        describe('template-input: name field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'name');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('name');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.name.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.name.setValue('');
                component._form.controls.name.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-input: website url field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'websiteUrl');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('bob.com/website_url');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.website_url.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.website_url.setValue('');
                component._form.controls.website_url.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-input: city field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'city');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('city');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.city.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.city.setValue('');
                component._form.controls.city.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-input: state field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'state');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('state');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.state.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.state.setValue('');
                component._form.controls.state.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-input: year founded field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'yearFounded');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('2017');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.year_founded.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.year_founded.setValue('');
                component._form.controls.year_founded.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-input: number of employees field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'numberOfEmployees');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('2');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.number_of_employees.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.number_of_employees.setValue('');
                component._form.controls.number_of_employees.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-input: about field', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                elem = beforeEachFormUiTest(fixture, 'about');
            }));

            itFA('contents set to existing value', () => {
                expect(elem.field.value).toEqual('about');
            });

            itFA('positively styles field when good input is provided', () => {
                checkPositivelyStyles(fixture, elem);
            });

            itFA('negatively styles field when bad input is provided', () => {
                component._form.controls.about.setValue('');
                fixture.detectChanges();
                tick(1000);

                checkNegativelyStyles(fixture, elem);
            });

            itFA('reveals the error message when a bad input is provided', () => {
                component._form.controls.about.setValue('');
                component._form.controls.about.markAsDirty();
                fixture.detectChanges();
                tick(1000);

                checkRevealsErrorMessage(fixture, elem);
            });
        });

        describe('template-view: unknown error message', () => {
            itFA('revealed when there is an error', () => {
                component._unknownError = true;
                fixture.detectChanges();

                expect(fixture.nativeElement.querySelector('#unknownError')).not.toBe(null);
            });

            itFA('hidden when there is no error', () => {
                fixture.detectChanges();

                expect(fixture.nativeElement.querySelector('#unknownError')).toBe(null);
            });
        });
    });

    describe('view mode', () => {
        beforeEach(fakeAsync(() => {
            component.company = company;
        }));

        itFA('hides create/edit form when in view mode', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#createOrEdit')).toEqual(null);
        });

        itFA('reveals view mode when in view mode', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#view')).not.toEqual(null);
        });

        describe('template-view: company name', () => {
            itFA('set to the company value', () => {
                fixture.detectChanges();

                expect(fixture.nativeElement.querySelector('#name').innerHTML.trim()).toEqual('name');
            });
        });

        describe('template-input: edit button', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                spyOn(component, '_isEdittable').and.returnValue(true);
                fixture.detectChanges();

                elem = fixture.nativeElement.querySelector('#edit');
            }));

            itFA('is revealed when edittable', () => {
                expect(elem).toBeDefined();
            });

            itFA('is hidden when not edittable', () => {
                component._isEdittable.and.returnValue(false);
                fixture.detectChanges();

                expect(fixture.nativeElement.querySelector('#edit')).toBe(null);
            });

            itFA('puts the component into edit mode when clicked', () => {
                elem.click();

                expect(component._isEditMode).toBe(true);
            });
        });

        describe('template-view: website url', () => {
            let elem;

            beforeEach(fakeAsync(() => {
                fixture.detectChanges();

                elem = fixture.nativeElement.querySelector('#websiteUrl');
            }));

            itFA('set to the website url value', () => {
                expect(elem.innerHTML.trim()).toEqual('bob.com/website_url');
            });

            itFA('points at the company url', () => {
                spyOn(component, '_goToCompanyWebsite');

                elem.click();

                expect(component._goToCompanyWebsite).toHaveBeenCalled();
            });
        });

        describe('template-view: location', () => {
            itFA('set to the location value', () => {
                fixture.detectChanges();

                let elem = fixture.nativeElement.querySelector('#location');

                expect(elem.innerHTML.trim()).toEqual('city, state');
            });
        });

        describe('template-view: about', () => {
            itFA('maintains paragraph structure in the description', () => {
                component.company.about = 'P1\nP2\nP3';
                fixture.detectChanges();

                let children = fixture.nativeElement.querySelector('#about').children;

                expect(children.length).toEqual(4);
                expect(children[1].innerHTML.trim()).toEqual('P1');
                expect(children[2].innerHTML.trim()).toEqual('P2');
                expect(children[3].innerHTML.trim()).toEqual('P3');
            });
        });
    });
});
