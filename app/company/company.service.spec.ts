// @author R. Matt McCann
// @brief Unit tests for the ProfileService
// @copyright &copy; 2017 R. Matt McCann

import { TestBed, async, inject } from '@angular/core/testing';
import { HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { Company } from './company.model';
import { CompanyService } from './company.service';


describe('CompanyService', () => {
    let companyService, mockBackend;

    let company: Company = {
        about: 'About',
        city: 'City',
        id: 'profile_id',
        logo_url: 'Url',
        name: 'name',
        state: 'state',
        number_of_employees: 2,
        year_founded: 2010,
        website_url: 'https://www.woofoo.com'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                CompanyService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });
    });

    beforeEach(inject([CompanyService, XHRBackend], (_companyService, _mockBackend) => {
        companyService = _companyService;
        mockBackend = _mockBackend;
    }));

    describe('create', () => {
        it('should create the company', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/company/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                expect(conn.request.getBody()).toEqual(JSON.stringify(company));

                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(company)})));
            });

            companyService.create(company).then((response) => {
                expect(response).toEqual(company);
            }).catch((error) => {
                fail('should not have rejected the promised Company');
            });
        }));

        it('should reject errors and return the response body', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/company/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                expect(conn.request.getBody()).toEqual(JSON.stringify(company));

                conn.mockError(new Response(new ResponseOptions({
                        body: JSON.stringify('errorMessage')
                    })));
            });

            companyService.create(company).then((response) => {
                fail('should have rejected the promised Company');
            }).catch((error) => {
                expect(error).toEqual('errorMessage');
            });
        }));
    });

    describe('get', () => {
        it('should call the API endpoint correctly', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/company/profile_id/');
                expect(conn.request.method).toEqual(RequestMethod.Get);
            });

            companyService.get('profile_id');
        }));

        it('should return the company when the API call is successful', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(company)})));
            });

            companyService.get('profile_id').then((response) => {
                expect(response).toEqual(company);
            }).catch(error => {
                fail('should not have rejected the promised company');
            });
        }));

        it('should reject with an error when the API call fails', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify('error message')})));
            });

            companyService.get('profile_id').then(response => {
                fail('should not have resolved the promised company');
            }, error => {
                expect(error).toEqual('error message');
            });
        }));
    });

    describe('update', () => {
        it('should call the API endpoint correctly', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/company/profile_id/');
                expect(conn.request.method).toEqual(RequestMethod.Put);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                expect(conn.request.getBody()).toEqual(JSON.stringify(company));
            });

            companyService.update(company);
        }));

        it('should return the profile when the API call is successful', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(company)})));
            });

            companyService.update(company).then((response) => {
                expect(response).toEqual(company);
            }).catch((error) => {
                fail('should not have rejected the promised company');
            });
        }));

        it('should reject with an error when the API call fails', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify('error message')})));
            });

            companyService.update(company).then((response) => {
                fail('should not have accepted the error response');
            }).catch((error) => {
                expect(error).toEqual('error message');
            });
        }));
    });
});
