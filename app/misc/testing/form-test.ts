/**
 * @author R. Matt McCann
 * @brief Helper functions for unit testing forms
 * @copyright &copy; 2017 R. Matt McCann
 */

import { fakeAsync, tick } from '@angular/core/testing';

export let beforeEachFormUiTest = function(fixture, fieldId) {
    fixture.detectChanges();
    tick(1000);

    let field = fixture.nativeElement.querySelector('#'+fieldId);
    let errors = fixture.nativeElement.querySelector('#'+fieldId+'Errors');

    return {field: field, errors: errors};
};

export let checkAcceptValid = function(fixture, component, key, validValue?) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    if (validValue) {
        component._form.controls[key].setValue(validValue);
    } else {
        component._form.controls[key].setValue('Valid');
    }

    tick(1000);

    expect(component._form.controls[key].valid).toBe(true);
    expect(component._formState[key].accepted).toBe(true);
    expect(component._formState[key].error).toEqual([]);
};

export let checkAcceptMissing = function(fixture, component, key) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('');

    tick(1000);

    expect(component._form.controls[key].valid).toBe(true);
    expect(component._formState[key].accepted).toBe(true);
    expect(component._formState[key].error).toEqual([]);
};

export let checkEmptyRequirement = function(fixture, component, key, pretty_key) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('');

    tick(1000);

    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual([pretty_key + ' is required']);
};

export let checkLongRequirement = function(fixture, component, key, length, pretty_key) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue(new Array(length+2).join('A'));

    tick(1000);

    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual(
        [pretty_key + ' cannot be more than ' + length + ' chacters long']);
};

export let checkMinLengthRequirement = function(fixture, component, key, length, pretty_key) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('A');

    tick(1000);

    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual(
        [pretty_key + ' must be at least ' + length + ' characters long']);
}

export let checkNaturalEnglishRequirement = function(fixture, component, key, pretty_key) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('!!!!!!!@@!@');

    tick(1000);

    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual(
        [pretty_key + ' does not appear to be valid']);
};

export let checkNumericRequirement = function(fixture, component, key, pretty_key) {
    fixture.detectChanges();

    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('asdf');

    tick(1000);

    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual(
        [pretty_key + ' does not appear to be valid']);
}

export let checkNegativelyStyles = function(fixture, elem) {
    fixture.detectChanges();
    tick(1000);

    expect(elem.field.classList.contains('ng-valid')).toBe(false);
    expect(elem.field.classList.contains('ng-invalid')).toBe(true);
}

export let checkPositivelyStyles = function(fixture, elem) {
    fixture.detectChanges();
    tick(1000);

    expect(elem.field.classList.contains('ng-valid')).toBe(true);
    expect(elem.field.classList.contains('ng-invalid')).toBe(false);
    expect(elem.errors.children.length).toBe(0);
};

export let checkRevealsErrorMessage = function(fixture, elem) {
    fixture.detectChanges();
    tick(1000);
    fixture.detectChanges();

    expect(elem.errors.children.length).toBe(1);
};

export let itFA = function(description, testFunc) {
    it(description, fakeAsync(testFunc));
};