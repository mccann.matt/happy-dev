"use strict";
var testing_1 = require("@angular/core/testing");
exports.beforeEachFormUiTest = function (fixture, fieldId) {
    fixture.detectChanges();
    testing_1.tick(1000);
    var field = fixture.nativeElement.querySelector('#' + fieldId);
    var errors = fixture.nativeElement.querySelector('#' + fieldId + 'Errors');
    return { field: field, errors: errors };
};
exports.checkAcceptValid = function (fixture, component, key, validValue) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    if (validValue) {
        component._form.controls[key].setValue(validValue);
    }
    else {
        component._form.controls[key].setValue('Valid');
    }
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(true);
    expect(component._formState[key].accepted).toBe(true);
    expect(component._formState[key].error).toEqual([]);
};
exports.checkAcceptMissing = function (fixture, component, key) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('');
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(true);
    expect(component._formState[key].accepted).toBe(true);
    expect(component._formState[key].error).toEqual([]);
};
exports.checkEmptyRequirement = function (fixture, component, key, pretty_key) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('');
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual([pretty_key + ' is required']);
};
exports.checkLongRequirement = function (fixture, component, key, length, pretty_key) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue(new Array(length + 2).join('A'));
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual([pretty_key + ' cannot be more than ' + length + ' chacters long']);
};
exports.checkMinLengthRequirement = function (fixture, component, key, length, pretty_key) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('A');
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual([pretty_key + ' must be at least ' + length + ' characters long']);
};
exports.checkNaturalEnglishRequirement = function (fixture, component, key, pretty_key) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('!!!!!!!@@!@');
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual([pretty_key + ' does not appear to be valid']);
};
exports.checkNumericRequirement = function (fixture, component, key, pretty_key) {
    fixture.detectChanges();
    component._form.controls[key].markAsDirty();
    component._form.controls[key].setValue('asdf');
    testing_1.tick(1000);
    expect(component._form.controls[key].valid).toBe(false);
    expect(component._formState[key].accepted).toBe(false);
    expect(component._formState[key].error).toEqual([pretty_key + ' does not appear to be valid']);
};
exports.checkNegativelyStyles = function (fixture, elem) {
    fixture.detectChanges();
    testing_1.tick(1000);
    expect(elem.field.classList.contains('ng-valid')).toBe(false);
    expect(elem.field.classList.contains('ng-invalid')).toBe(true);
};
exports.checkPositivelyStyles = function (fixture, elem) {
    fixture.detectChanges();
    testing_1.tick(1000);
    expect(elem.field.classList.contains('ng-valid')).toBe(true);
    expect(elem.field.classList.contains('ng-invalid')).toBe(false);
    expect(elem.errors.children.length).toBe(0);
};
exports.checkRevealsErrorMessage = function (fixture, elem) {
    fixture.detectChanges();
    testing_1.tick(1000);
    fixture.detectChanges();
    expect(elem.errors.children.length).toBe(1);
};
exports.itFA = function (description, testFunc) {
    it(description, testing_1.fakeAsync(testFunc));
};
//# sourceMappingURL=form-test.js.map