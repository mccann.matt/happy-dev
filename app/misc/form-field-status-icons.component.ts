/**
 * @author R. Matt McCann
 * @brief A small green check / red x input feedback
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, Input } from '@angular/core';

import { FieldState } from './form-builder.service';


@Component({
    selector: 'form-field-status-icons',
    template: `
        <div class="status-icons">
            <span *ngIf="field.accepted" class="valid fa fa-check"></span>
            <span *ngIf="field.error.length" class="invalid fa fa-times"></span>
        </div>
    `,
    styleUrls: ['form-field-status-icons.component.scss']
})
export class FormFieldStatusIconsComponent {
    @Input() field: FieldState;
}
