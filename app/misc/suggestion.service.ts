/**
 * @author R. Matt McCann
 * @brief Generic service fragment that fetches suggestions
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import { Headers, Http } from '@angular/http';


export function buildGetSuggestions<T>(suggestionsUrl: string, http: Http): (fragment: any) => Observable<T[]> {
    let headers = new Headers({'Content-Type': 'application/json'});

    return (fragment): Observable<Array<T>> => {
        return http
            .post(suggestionsUrl, JSON.stringify(fragment.toLowerCase()), {headers: headers, withCredentials: true})
            .map((response) => response.json())
            .catch(error => Observable.throw(error.json()));
    };
}
