/**
 * @author R. Matt McCann
 * @brief Unit tests for the ModalFormComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { ModalFormComponent } from './modal-form.component';


class ModalFormComponentImpl extends ModalFormComponent {
    constructor(formBuilder) {
        super(formBuilder);
    }

    _init(): void { }
}

describe('ModalFormComponent', () => {
    let component, mockFormBuilder;

    beforeEach(() => {
        mockFormBuilder = jasmine.createSpyObj('FormBuilder', ['onFormChanged']);
        component = new ModalFormComponentImpl(mockFormBuilder);
        component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
    });

    describe('dismiss', () => {
        it('resets the state of the component', () => {
            spyOn(component, '_init');

            component.dismiss();

            expect(component._init).toHaveBeenCalled();
        });
    });

    describe('ngOnInit', () => {
        it('initializes the component', () => {
            spyOn(component, '_init');

            component.ngOnInit();

            expect(component._init).toHaveBeenCalled();
        });
    });

    describe('_onCancel', () => {
        it('resets and fires close event', () => {
            spyOn(component, '_init');

            component._onCancel();

            expect(component._init).toHaveBeenCalled();
            expect(component.onClose.emit).toHaveBeenCalledWith();
        });
    });

    describe('_onFormChanged', () => {
        it('clears the unexpected error message and calls standard form processing', () => {
            component._unexpectedError = true;

            component._onFormChanged();

            expect(component._unexpectedError).toBe(false);
            expect(mockFormBuilder.onFormChanged).toHaveBeenCalled();
        });
    });
});
