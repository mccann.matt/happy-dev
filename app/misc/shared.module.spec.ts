/**
 * @author R. Matt McCann
 * @brief Not an actual unit test suite, this describe block ensures that all of the module objects are hit
 *        by test coverage, even if they are not tested.
 * @copyright &copy; 2017 R. Matt McCann
 */

import { TestBed } from '@angular/core/testing';
import { MODULE_DEF } from './shared.module';

describe('[NOTATEST] Triggering coverage...', () => {
    TestBed.configureTestingModule(MODULE_DEF);
});
