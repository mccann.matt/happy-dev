/**
 * @author R. Matt McCann
 * @brief Unit tests for the SuggestionField class
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component } from '@angular/core';
import { async, TestBed, tick } from '@angular/core/testing';

import { itFA } from './testing/form-test';
import { SuggestionField } from './suggestion.field';

interface Suggestion {
    name: string;
    name_suggest: string;
};

@Component({
    selector: 'suggestion-field-impl',
    template: `
        Woof!
    `
})
class SuggestionFieldImpl extends SuggestionField<Suggestion> {
    constructor() {
        super('field', 'name');
    }
};


describe('SuggestionField', () => {
    let component, fixture;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ SuggestionFieldImpl ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(SuggestionFieldImpl);
            component = fixture.componentInstance;
        });
    }));

    describe('acceptSuggestion', () => {
        beforeEach(() => {
            component._control = jasmine.createSpyObj('control', ['markAsDirty', 'setValue']);
        });

        it('updates the control state', () => {
            component.acceptSuggestion({name_suggest: 'woof'});

            expect(component._control.markAsDirty).toHaveBeenCalled();
            expect(component._suggestionInProgress).toBe(false);
        });

        it('clears the suggestion list', () => {
            component.acceptSuggestion({name: 'woof', name_suggest: 'woof'});

            expect(component.suggestions).toEqual([]);
        });

        it('unsubscribes from the current ongoing suggesstion request', () => {
            component._suggestionsRequest = jasmine.createSpyObj('Request', ['unsubscribe']);

            component.acceptSuggestion({name: 'woof', name_suggest: 'woof'});

            expect(component._suggestionsRequest.unsubscribe).toHaveBeenCalled();
        });

        describe('when a valid suggestion is accepted', () => {
            it('accepts the suggestion', () => {
                component.acceptSuggestion({name: 'woof', name_suggest: 'woof2'});

                expect(component._control.setValue).toHaveBeenCalledWith('woof');
                expect(component._suggestionAccepted).toBe(true);
            });

            it('fires the accept callback', () => {
                component._acceptCallback = jasmine.createSpy('Callback');

                let suggestion = {name: 'woof', name_suggest: 'woof2'};
                component.acceptSuggestion(suggestion);

                expect(component._acceptCallback).toHaveBeenCalledWith(suggestion);
            });
        });

        describe('when an invalid suggestion is accepted', () => {
            it('clears the suggestion state', () => {
                component._field = jasmine.createSpy('field');
                component._field.nativeElement = jasmine.createSpy('nativeElement');
                component._suggestionAccepted = true;

                component.acceptSuggestion({});

                expect(component._suggestionAccepted).toBe(false);
                expect(component._field.nativeElement.value).toBe('');
                expect(component._control.setValue).toHaveBeenCalledWith('');
            });
        });
    });

    describe('markAsAccepted', () => {
        it('marks the suggestion as accepted', () => {
            component.markAsAccepted();

            expect(component._suggestionAccepted).toBe(true);
            expect(component._suggestionInProgress).toBe(false);
        });
    });

    describe('onFieldChanged', () => {
        beforeEach(() => {
            component._control = jasmine.createSpyObj('control', ['markAsDirty', 'setValue', 'updateValueAndValidity']);
            component._field = jasmine.createSpy('field');
            component._field.nativeElement = jasmine.createSpy('nativeElement');
            component._field.nativeElement.value = 'woof';
            component._getSuggestions = jasmine.createSpy('getSuggestions');
            component._getSuggestions.and.returnValue({
                subscribe: (then_cb, catch_cb) => { }
            });
        });

        it('marks the field as dirty', () => {
            component._suggestionAccepted = true;

            component._onFieldChanged();

            expect(component._suggestionAccepted).toBe(false);
            expect(component._control.updateValueAndValidity).toHaveBeenCalled();
            expect(component._control.markAsDirty).toHaveBeenCalled();
        });

        it('cancels the last suggestion request', () => {
            let request = jasmine.createSpyObj('Request', ['unsubscribe']);
            component._suggestionsRequest = request;

            component._onFieldChanged();

            expect(request.unsubscribe).toHaveBeenCalled();
        });

        it('does not send empty requests', () => {
            component._field.nativeElement.value = '';
            component._suggestionInProgress = false;

            component._onFieldChanged();

            expect(component._suggestionInProgress).toBe(false);
        });

        it('correctly calls getSuggestions', () => {
            component._field.nativeElement.value = 'woof';

            component._onFieldChanged();

            expect(component._getSuggestions).toHaveBeenCalledWith('woof');
        });

        describe('after successful suggestion fetch', () => {
            beforeEach(() => {
                component._getSuggestions = jasmine.createSpy('getSuggestions');
            });

            itFA('sets the suggestions when suggestions are available', () => {
                component._getSuggestions.and.returnValue({
                    subscribe: (then_cb, catch_cb) => { then_cb(['suggestions']); }
                });

                component._onFieldChanged();
                tick();

                expect(component.suggestions).toEqual(['suggestions']);
            });

            itFA('sets a no-results message if no suggestions are available', () => {
                component._getSuggestions.and.returnValue({
                    subscribe: (then_cb, catch_cb) => { then_cb([]); }
                });

                component._onFieldChanged();
                tick();

                expect(component.suggestions).toEqual([{name: 'Not recognized!', name_suggest: undefined}]);
            });
        });

        describe('after failed suggestion fetch', () => {
            beforeEach(() => {
                component._getSuggestions = jasmine.createSpy('getSuggestions');
            });

            itFA('sets an error message', () => {
                component._getSuggestions.and.returnValue({
                    subscribe: (then_cb, catch_cb) => { catch_cb('error'); }
                });

                component._onFieldChanged();
                tick();

                expect(component.suggestions).toEqual([{name: 'Unexpected error occurred!', name_suggest: undefined}]);
            });
        });
    });
});
