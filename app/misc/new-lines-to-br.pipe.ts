/**
 * @author R. Matt McCann
 * @brief Transforms new lines into <br /> for maintaining paragraph structure
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Pipe, PipeTransform } from '@angular/core';


@Pipe({name: 'newLinesToBr'})
export class NewLinesToBrPipe implements PipeTransform {
    transform(value: string): Array<string> {
        let splitArray = value.split('\n');

        return splitArray;
    }
}
