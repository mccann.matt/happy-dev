/**
 * @author R. Matt McCann
 * @brief General data manipulation helper functions
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from '@angular/core';


/** Provides data manipulation helper functions. */
@Injectable()
export class DataManipService {
    /**
     * Converts the provided data URI into a Blob.
     *
     * @param dataURI Data URI encoded data to be converted to Blob
     * @returns Blob encoded data
     */
    dataUriToBlob(dataURI: string): Blob {
        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            byteString = atob(dataURI.split(',')[1]);
        } else {
            byteString = decodeURI(dataURI.split(',')[1]);
        }

        // separate out the mime component
        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    };
}
