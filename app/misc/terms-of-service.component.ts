/**
 * @author R. Matt McCann
 * @brief Terms of service for HappyDev.io
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component } from '@angular/core';

@Component({
    selector: 'terms-of-service',
    templateUrl: 'terms-of-service.component.html'
})
export class TermsOfServiceComponent { }
