/**
 * @author R. Matt McCann
 * @brief Unit tests for the SuggestionService
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { TestBed, inject } from '@angular/core/testing';
import { Http, HttpModule, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { buildGetSuggestions } from './suggestion.service';


@Injectable()
class SuggestionServiceImpl {
    getSuggestions: (fragment: any) => Observable<string[]>;

    constructor(http: Http) {
        this.getSuggestions = buildGetSuggestions<string>('http://localhost:8001/suggestion/', http);
    }
};


describe('SuggestionService', () => {
    let suggestionService, mockBackend;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ HttpModule ],
            providers: [
                SuggestionServiceImpl,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });
    });

    beforeEach(inject([SuggestionServiceImpl, XHRBackend], (_suggestionService, _mockBackend) => {
        suggestionService = _suggestionService;
        mockBackend = _mockBackend;
    }));

    describe('getSuggestions', () => {
        /*it('calls the api correctly', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/suggestion/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                expect(conn.request.withCredentials).toBe(true);
                expect(conn.request.getBody()).toEqual(JSON.stringify('something'));
            });

            suggestionService.getSuggestions('something');
        }));

        it('returns the suggestions result on success', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify([])})));
            });

            suggestionService.getSuggestions('something').then(
                (response) => { expect(response).toEqual([]); },
                (error) => { fail('should not have rejected the resolved response'); }
            );
        }));

        it('rejects the error on failure', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify([])})));
            });

            suggestionService.getSuggestions('something').then(
                (response) => { fail('should not have resolved the error response'); },
                (error) => { expect(error).toEqual([]); }
            );
        }));*/
    });
});
