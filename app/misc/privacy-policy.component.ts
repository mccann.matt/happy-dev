// @author R. Matt McCann
// @brief Privacy policy for HappyDev.io
// @copyright &copy; 2017 R. Matt McCann

import { Component } from '@angular/core';

@Component({
    selector: 'privacy-policy',
    templateUrl: 'privacy-policy.component.html',
    styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent { }
