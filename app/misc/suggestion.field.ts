/**
 * @author R. Matt McCann
 * @brief Helper class for fetching and interacting with auto-complete suggestions
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ElementRef } from '@angular/core';

import { AbstractControl, ValidatorFn } from '@angular/forms';


export class SuggestionField<T> {
    /** Callback fired when a suggestion is accepted. */
    _acceptCallback: (T) => void;

    /** Control object for whom suggestions are made. */
    _control: AbstractControl;

    /** Field object for whom suggestions are made. */
    _field: ElementRef;

    /** Name of the field for whom suggestions are made. */
    _fieldName: string;

    /** API service fragment that fetches the suggestions list. */
    _getSuggestions: (fragment: any) => Observable<T[]>;

    /** Marked true when a suggestion is accepted. */
    _suggestionAccepted = false;

    /** Marked true while suggestions are being made. */
    _suggestionInProgress = true;

    /** Property of the suggestable object type which is being searched for matches. */
    _suggestionPropertyName: string;

    /** List of suggested values. */
    suggestions: Array<T|any>;

    /** The current api request for suggestions. */
    _suggestionsRequest: Subscription;

    constructor(fieldName: string, suggestionPropertyName: string) {
        this._fieldName = fieldName;
        this._suggestionPropertyName = suggestionPropertyName;
    }

    /** Triggered when the user accepts a suggestion. */
    acceptSuggestion(suggestion: T): void {
        // Update the control state
        this._control.markAsDirty();
        this._suggestionInProgress = false;

        // If this is a valid suggestion/
        if (suggestion[this._suggestionPropertyName + '_suggest']) {
            // Accept the suggestion
            this._suggestionAccepted = true;
            this._control.setValue(suggestion[this._suggestionPropertyName]);

            if (this._acceptCallback) {
                this._acceptCallback(suggestion);
            }
        } else {
            // Clear the suggestion
            this._suggestionAccepted = false;
            this._field.nativeElement.value = '';
            this._control.setValue('');
        }

        // Clear the list of suggestions
        this.suggestions = [];

        // If a suggestion is underway, clear the request
        if (this._suggestionsRequest) {
            this._suggestionsRequest.unsubscribe();
        }
    }

    /** Marks the suggestion as accepted. */
    markAsAccepted(): void {
        this._suggestionAccepted = true;
        this._suggestionInProgress = false;
    }

    /** After the component view has initialized. */
    ngAfterViewInit(field: ElementRef): void {
        this._field = field;

        // Listen for input events on the suggestable field
        Observable.fromEvent(this._field.nativeElement, 'input')
            .debounceTime(100)
            .subscribe(data => this._onFieldChanged());
    }

    /** After the component has been initialized. */
    ngOnInit(
            control: AbstractControl, getSuggestions: (fragment: any) => Observable<T[]>,
            acceptCallback?: (T) => void): void {
        // Set the control input objects and api fragment
        this._acceptCallback = acceptCallback;
        this._control = control;
        this._getSuggestions = getSuggestions;
        this.suggestions = [];
    }

    /** Triggered when there is a user input event on the suggestable field. */
    _onFieldChanged(): void {
        // Mark the field as dirty
        this._suggestionAccepted = false;
        this._control.updateValueAndValidity();
        this._control.markAsDirty();

        // Cancel the last suggestion request if it hasn't returned yet
        if (this._suggestionsRequest) {
            this._suggestionsRequest.unsubscribe();
        }

        // Do not send empty suggestion requests
        if (!this._field.nativeElement.value) {
            this.suggestions = [];
            return;
        }

        // Mark the suggestion as in progress
        this._suggestionInProgress = true;

        // Fetch the next batch of suggestions
        this._suggestionsRequest = this._getSuggestions(this._field.nativeElement.value)
            .subscribe(
                // If the suggestions are successfully fetched
                (suggestions) => {
                    // If there are suggestions available
                    if (suggestions.length > 0) {
                        // Reveal the list of suggetions
                        this.suggestions = suggestions;
                    // If there are no suggestions
                    } else {
                        // Put in a not recognized message
                        let emptySuggestion = {};
                            emptySuggestion[this._suggestionPropertyName] = 'Not recognized!';
                            emptySuggestion[this._suggestionPropertyName + '_suggest'] = undefined;

                        this.suggestions = [emptySuggestion];
                    }
                },
                // If the suggestions are not fetched successfully
                (error) => {
                    // Put in an error message
                    let errorSuggestion = {};
                        errorSuggestion[this._suggestionPropertyName] = 'Unexpected error occurred!';
                        errorSuggestion[this._suggestionPropertyName + '_suggest'] = undefined;
                    this.suggestions = [errorSuggestion];
                }
            );
    }

    /** Builds a validator function that returns true if a suggestion has been accepted. */
    validateAccepted(): ValidatorFn {
        let me = this;

        return (control: AbstractControl): {[key: string]: any} => {
            // If the suggestions are in progress
            if (me._suggestionInProgress) {
                // Return an unstyle validator error to prevent form acceptance without displaying an error message
                return {suggestionInProgress: true};
            }

            // If the suggestion is not accepted, return the required validation error
            return me._suggestionAccepted ? null : {required: true};
        };
    }
}
