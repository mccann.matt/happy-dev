/**
 * @author R. Matt McCann
 * @brief Injection token for injecting the window
 * @copyright &copy; 2017 R. Matt McCann
 */

import { OpaqueToken } from '@angular/core';

export const WindowToken = new OpaqueToken('Window');
