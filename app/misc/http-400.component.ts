// @author R. Matt McCann
// @brief Handles HTTP 400 missing page requests
// @copyright &copy; 2016 R. Matt McCann

import { Component } from '@angular/core';

@Component({
    selector: 'http-400',
    templateUrl: 'http-400.component.html'
})
export class Http400Component { }
