/**
 * @author R. Matt McCann
 * @brief Configures shared dependency
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBuilderService } from './form-builder.service';
import { FormFieldErrorsComponent } from './form-field-errors.component';
import { FormFieldStatusIconsComponent } from './form-field-status-icons.component';
import { DataManipService } from './data-manip.service';
import { NewLinesToBrPipe } from './new-lines-to-br.pipe';
import { WindowToken } from '../misc/window';


export let MODULE_DEF = {
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        FormFieldErrorsComponent,
        FormFieldStatusIconsComponent,
        NewLinesToBrPipe
    ],
    exports: [
        FormFieldErrorsComponent,
        FormFieldStatusIconsComponent,
        NewLinesToBrPipe
    ],
    providers: [
        DataManipService,
        FormBuilderService,
        { provide: WindowToken, useValue: window }
    ]
};

@NgModule(MODULE_DEF)
export class SharedModule { }
