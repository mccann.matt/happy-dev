/**
 * @author R. Matt McCann
 * @brief Defines to root Angular module that controls the application
 * @copyright &copy; 2016 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { KSSwiperModule } from 'angular2-swiper';

import { AppEntryComponent } from './app-entry.component';
import { AppRoutingModule } from './app-routing.module';
import { AccountModule } from './account/account.module';
import { MvtService, MVT_API_ROOT_URL, MVT_STRUCTURE_DEFINITION } from './analytics/mvt/client/mvt.service';
import { AppComponent } from './landing-page/app.component';
import { FaqComponent } from './landing-page/faq.component';
import { Http400Component } from './misc/http-400.component';
import { PrivacyPolicyComponent } from './misc/privacy-policy.component';
import { SharedModule } from './misc/shared.module';
import { TermsOfServiceComponent } from './misc/terms-of-service.component';
import { MyMvtStructureDefinition } from './analytics/mvt/my-mvt-structure-definition.model';
import { GoogleAnalytics } from './analytics/google/google-analytics';

// NOTE(mmccann) - Wtf? Lazy module loading is no longer working in Angular2 4.0???
import { DeveloperModule } from './developer/developer.module';

let providers = [
    {
        provide: MVT_STRUCTURE_DEFINITION,
        useClass: MyMvtStructureDefinition
    },
    {
        provide: MVT_API_ROOT_URL,
        useValue: process.env.MVT_ROOT_API_URL
    },
    GoogleAnalytics,
    MvtService
];

export let APP_MODULE_DEF = {
    imports: [
        AccountModule,
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        KSSwiperModule,
        Ng2Bs3ModalModule,
        Ng2PageScrollModule.forRoot(),
        ReactiveFormsModule,
        ReCaptchaModule,
        SharedModule,

        DeveloperModule
    ],
    declarations: [
        AppComponent,
        AppEntryComponent,
        FaqComponent,
        Http400Component,
        PrivacyPolicyComponent,
        TermsOfServiceComponent
    ],
    providers: providers,
    bootstrap: [ AppEntryComponent ]
};

@NgModule(APP_MODULE_DEF)
export class AppModule { }
