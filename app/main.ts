// @author R. Matt McCann
// @brief Bootstraps the application to run in the browser
// @copyright &copy; 2016 R. Matt McCann

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

import { AppModule } from './app.module';


if (process.env.ENV === 'production') {
    enableProdMode();
}

document.addEventListener('DOMContentLoaded', function() {
    platformBrowserDynamic()
        .bootstrapModule(AppModule);
});
