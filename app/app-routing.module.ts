// @author R. Matt McCann
// @brief Router logic for the App component
// @copyright &copy; 2016 R. Matt McCann

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './landing-page/app.component';
import { FaqComponent } from './landing-page/faq.component';
import { Http400Component } from './misc/http-400.component';
import { PrivacyPolicyComponent } from './misc/privacy-policy.component';
import { TermsOfServiceComponent } from './misc/terms-of-service.component';

import { DeveloperDashboardComponent } from './developer/dashboard/developer-dashboard.component';
import { QuestionDetailsComponent } from './developer/question/question-details/question-details.component';

const routes: Routes = [
    { path: '', component: AppComponent },
    { path: 'company', loadChildren: './company/company.module#CompanyModule', pathMatch: 'prefix'},
    { path: 'developer/:id/dashboard', component: DeveloperDashboardComponent },
    { path: 'developer/:id/questions', component: QuestionDetailsComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: 'terms-and-conditions', component: TermsOfServiceComponent },
    { path: '**', component: Http400Component }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
