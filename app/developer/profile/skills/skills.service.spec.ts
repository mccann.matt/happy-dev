/**
 * @author R. Matt McCann
 * @brief Unit tests for the skill service
 * @copyright &copy; 2017 R. Matt McCann
 */

import { TestBed, async, inject } from '@angular/core/testing';
import { HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { SkillsService } from './skills.service';


describe('SkillsService', () => {
    let skillsService, mockBackend;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                SkillsService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });
    });

    beforeEach(inject([SkillsService, XHRBackend], (_skillsService, _mockBackend) => {
        skillsService = _skillsService;
        mockBackend = _mockBackend;
    }));

    describe('constructor', () => {
        it('should populate getSuggestions', () => {
            expect(skillsService.getSuggestions).toBeDefined();
        });
    });

    /*describe('getSuggestions', () => {
        it('should call the API endpoint correctly', async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual('http://localhost:8001/skills/');
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get('Content-Type')).toEqual('application/json');
                expect(conn.request.getBody()).toEqual(JSON.stringify('typ'));
            });

            skillsService.getSuggestions('Typ');
        }));

        it('should return a list of suggestions when the api call succeeds', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(['Typescript', 'Typing'])})));
            });

            skillsService.getSuggestions('Typ').subscribe(response => {
                expect(response).toEqual(['Typescript', 'Typing']);
            }, error => {
                fail('should not have thrown error');
            });
        }));

        it('should propogate the error when the api call fails', async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify('oh no!')})));
            });

            skillsService.getSuggestions('Typ').subscribe(response => {
                fail('should have thrown error!');
            }, error => {
                expect(error).toEqual('oh no!');
            });
        }));
    });*/
});