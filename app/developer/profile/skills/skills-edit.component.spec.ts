/**
 * @author R. Matt McCann
 * @brief Unit tests for the SkillsEditComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { itFA } from '../../../misc/testing/form-test';
import { DeveloperService } from '../../developer.service';
import { SkillsEditComponent } from './skills-edit.component';
import { SkillsService } from './skills.service';


describe('SkillsEditComponent', () => {
    let component, fixture, mockDeveloperService, mockGa, mockSkillsService, mockSkills, mockDeveloper;

    beforeEach(async(() => {
        mockSkillsService = jasmine.createSpyObj('SkillsService', ['getSuggestions']);
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockDeveloper = jasmine.createSpy('Profile');
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);
        mockSkills = ['Typescript', 'Angular2'];

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [ SkillsEditComponent ],
                schemas: [ NO_ERRORS_SCHEMA ],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService },
                    { provide: GoogleAnalytics, useValue: mockGa },
                    { provide: SkillsService, useValue: mockSkillsService }
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(SkillsEditComponent);
                component = fixture.componentInstance;
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.skills = mockSkills;
                component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('_buildAcceptSkillSuggestion', () => {
        let acceptSkillSuggestion;

        beforeEach(fakeAsync(() => {
            acceptSkillSuggestion = component._buildAcceptSkillSuggestion();
            fixture.detectChanges();
        }));

        it('should add the accepted skill to the list', () => {
            acceptSkillSuggestion({name: 'skill'});

            expect(component._skills.length).toBe(3);
            console.error(component._skills);
            expect(component._skills[2]).toEqual('skill');
        });

        it('should clear the skill field', () => {
            component._skillField.nativeElement.value = 'skill';
            acceptSkillSuggestion('skill');

            expect(component._skillField.nativeElement.value).toBe('');
        });
    })

    describe('_deleteSkill', () => {
        it('removes the skill from the skills list', fakeAsync(() => {
            fixture.detectChanges();

            component._deleteSkill('Angular2');

            expect(component._skills.length).toEqual(1);
            expect(component._skills[0]).toEqual('Typescript');
        }));
    });

    describe('_init', () => {
        it('makes a local copy of the profiles skills list', () => {
            component.ngOnInit();

            expect(component._skills.length).toEqual(2);
            expect(component._skills[0]).toEqual('Typescript');
            expect(component._skills[1]).toEqual('Angular2');
        });

        it('builds the form group', () => {
            component.ngOnInit();

            expect(component._form).toBeDefined();
            expect(component._form.controls['skill']).toBeDefined();
        });

        it('initializes the skill suggestion field', () => {
            spyOn(component._skillSuggestion, 'ngOnInit');

            component.ngOnInit();

            expect(component._skillSuggestion.ngOnInit).toHaveBeenCalled();
        });
    });

    describe('ngAfterViewInit', () => {
        it('post-view initializes the skill suggestion field', () => {
            spyOn(component._skillSuggestion, 'ngAfterViewInit');

            component.ngOnInit();
            component.ngAfterViewInit();

            expect(component._skillSuggestion.ngAfterViewInit).toHaveBeenCalled();
        });
    });

    describe('_save', () => {
        beforeEach(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: (then_cb, catch_cb) => {
                    }
                };
            });
        });

        it('sets the profile skills list to the newly edited list value', () => {
            component._save();

            expect(component.developer.skills).toEqual(component._skills);
        });

        it('calls the update api endpoint with the profile', () => {
            component._save();

            expect(mockDeveloperService.update).toHaveBeenCalledWith(component.developer.id, component.developer);
        });

        itFA('resets the form and fires the onClose event on successful update', () => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: (then_cb, catch_cb) => {
                        then_cb(developer);
                    }
                };
            });

            fixture.detectChanges();
            spyOn(component, '_init');

            component._save();

            expect(component._init).toHaveBeenCalled();
            expect(component.onClose.emit).toHaveBeenCalledWith();
        });

        it('reveals the unknown error message if the update api call fails', () => {
            mockDeveloperService.update.and.callFake(profile => {
                return {
                    then: (then_cb, catch_cb) => {
                        catch_cb('error!');
                    }
                };
            });

            component._save();

            expect(component._unexpectedError).toBe(true);
        });
    });

    describe('template-input: accept suggestion click', () => {
        xit('test me!', () => {

        });
    });

    describe('template-input: save click', () => {
        xit('test me!', () => {

        });
    });

    describe('template-input: cancel click', () => {
        xit('test me!', () => {

        });
    });
});
