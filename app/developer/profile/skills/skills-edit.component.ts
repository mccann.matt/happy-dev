/**
 * @author R. Matt McCann
 * @brief Provides the developer with an interface for editting their skill tags
 * @copyright &copy; 2017 R. Matt McCann
 */

import 'rxjs/add/operator/debounceTime';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AfterViewInit, Component, ElementRef, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { SuggestionField } from '../../../misc/suggestion.field';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';
import { Skill } from './skill.model';
import { SkillsService } from './skills.service';


/** Provides the developer with an interface for editting their skill tags. */
@Component({
    selector: 'skills-edit',
    templateUrl: 'skills-edit.component.html',
    styleUrls: ['skills-edit.component.scss']

})
export class SkillsEditComponent extends ModalFormComponent implements AfterViewInit {
    /** Developer object being editted. */
    @Input() developer: Developer;

    /** Raw suggestable skill field. Handed off to the suggestable field object. */
    @ViewChild("newSkill") private _skillField: ElementRef;

    /** Suggestable skill field. Fetches suggestions from the backend. */
    _skillSuggestion = new SuggestionField<Skill>('skill', 'name');

    /** List of skills being editted. */
    _skills: Array<string>;

    constructor(
        private _developerService: DeveloperService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder,
        private _skillService: SkillsService
    ) {
        super();
    }

    /** Builds the acceptSkillSuggestion callback that saves the selected skill and clears the field. */
    _buildAcceptSkillSuggestion() {
        let me = this;

        return (skill) => {
            // Save the selected skill suggestion
            me._skills.push(skill.name);

            // Clear the skill field contents
            me._skillField.nativeElement.value = '';
        };
    }

    /** Fired when the user clicks the delete skill button. */
    _deleteSkill(skill: string): void {
        this._skills.splice(this._skills.indexOf(skill), 1);
    }

    /** Initializes the component state. */
    _init(): void {
        // Extract the skills list so we can edit it without editting the owning developer object
        this._skills = [];
        for (let skill of this.developer.skills) {
            this._skills.push(skill);
        }

        // We are only building a form to comply with the requirements of the SuggestionField
        this._form = this._ngFormBuilder.group({'skill': new FormControl(undefined, [])});

        // Initialize the suggestable field wrapper
        this._skillSuggestion.ngOnInit(
            this._form.controls['skill'], this._skillService.getSuggestions, this._buildAcceptSkillSuggestion());

        // Clear the text field contents if its been built
        if (this._skillField) {
            this._skillField.nativeElement.value = '';
        }
    }
    /** After the view has been initialized, does work on the view objects. */

    /** After the component view has been initialized. */
    ngAfterViewInit(): void {
        // Pass the field reference to the suggestion field wrapper now that it is available
        this._skillSuggestion.ngAfterViewInit(this._skillField);
    }

    /** Fired when the user clicks the save button. */
    _save(): void {
        // Update the developer object with the editted skills
        this.developer.skills = this._skills;

        // Update the developer record
        this._developerService.update(this.developer.id, this.developer)
            .then(
                // If the update succeeds
                (profile) => {
                    // Report the edit to analytics
                    this._ga.sendEvent('skills_edit', 'success');

                    // Fire the close modal event
                    this.onClose.emit();

                    // Reset the state of the component
                    this._init();
                },
                // If the update fails
                (error) => {
                    // Report the error to analytics
                    this._ga.sendEvent('skills_edit', 'error');

                    // Reveal the unexpected error message
                    this._unexpectedError = true;
                }
            );
    }
}
