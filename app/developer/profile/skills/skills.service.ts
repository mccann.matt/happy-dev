/**
 * @author R. Matt McCann
 * @brief Http service for hitting the Skills api
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { buildGetSuggestions } from '../../../misc/suggestion.service';
import { Skill } from './skill.model';

@Injectable()
export class SkillsService {
    _skillsUrl = process.env.API_SERVER + '/skills/';

    getSuggestions: (fragment: any) => Observable<Skill[]>;

    constructor(private _http: Http) {
        this.getSuggestions = buildGetSuggestions<Skill>(this._skillsUrl + 'suggest/', _http);
    }
}