// @author R. Matt McCann
// @brief A simple way to showoff a featured skill
// @copyright &copy; 2016 R. Matt McCann

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'skill-tag',
    templateUrl: 'skill-tag.component.html',
    styleUrls: ['skill-tag.component.scss']
        

})
export class SkillTagComponent {
    @Input() isDeletable: boolean;

    @Output() onDelete = new EventEmitter<boolean>();

    @Input() skill: string;
}
