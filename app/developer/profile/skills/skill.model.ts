/**
 * @author R. Matt McCann
 * @brief Model for the skill tags
 * @copyright &copy; 2017 R. Matt McCann
 */

export interface Skill {
    name: string;
    name_suggest: string;
};
