// @author R. Matt McCann
// @brief Summary details for the developer's work experience
// @copyright &copy; 2017 R. Matt McCann;

export interface WorkHistoryDetails {
    company: string;
    start_date: number;
    start_month?: any;
    start_year?: number;
    end_date: number;
    end_month?: any;
    end_year? : number;
    description?: string;
    currently_works_here: boolean;
    title: string;
};
