/**
 * @author R. Matt McCann
 * @brief Summary details for the developer's work experience
 * @copyright &copy; 2016 R. Matt McCann
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';

import { DeveloperService} from '../../developer.service';
import { ListDetailsComponent } from '../misc/list-details.component';
import { WorkHistoryDetails } from './work-history-details.model';


/** View of the details of a developer's work experience */
@Component({
    selector: 'work-history-details',
    templateUrl: 'work-history-details.component.html',
    styleUrls: ['work-history-details.component.scss']
})
export class WorkHistoryDetailsComponent extends ListDetailsComponent<WorkHistoryDetails> {
    /** Provides a textual display of the work months. */
    _monthNames = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];

    constructor(developerService: DeveloperService) {
        super(developerService, 'work_history');
    }
}
