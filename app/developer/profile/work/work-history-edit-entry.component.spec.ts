/**
 * @author R. Matt McCann
 * @brief Unit tests for the WorkHistoryEditEntryComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import { FormFieldErrorsComponent } from '../../../misc/form-field-errors.component';
import {
    checkAcceptValid, checkEmptyRequirement, checkLongRequirement, checkAcceptMissing
} from '../../../misc/testing/form-test';
import { DeveloperService} from '../../developer.service';
import { WorkHistoryEditEntryComponent } from './work-history-edit-entry.component';


describe('WorkHistoryEditEntryComponent', () => {
    let component, fixture, mockDeveloperService, mockGa, mockWorkHistory, mockDeveloper;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);
        mockWorkHistory = [
            {
                company: 'ScriptFlipers, Inc',
                start_date: 2012.03,
                start_month: 3,
                start_year: 2012,
                end_date: undefined,
                end_month: undefined,
                end_year: undefined,
                description: 'Flippin the script',
                currently_works_here: true,
                title: 'Chief Somethin'
            },
            {
                company: 'NuttFudder, Co',
                start_date: 2012.03,
                start_month: 3,
                start_year: 2012,
                end_date: 2014.09,
                end_month: 9,
                end_year: 2014,
                description: 'Made pb & j',
                currently_works_here: false,
                title: 'Chief Somebody'
            },
        ];

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [ FormFieldErrorsComponent, WorkHistoryEditEntryComponent ],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService },
                    { provide: GoogleAnalytics, useValue: mockGa },
                    FormBuilderService
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(WorkHistoryEditEntryComponent);
                component = fixture.componentInstance;
                component._entryUnderEditIdx = 0;
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.work_history = mockWorkHistory;
                component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('constructor', () => {
        it('builds a seventy year optionrange', () => {
            const thisYear = new Date().getFullYear();

            for (let iter = 0; iter < 70; iter++) {
                expect(component._yearOptions[iter]).toEqual(thisYear - iter);
            }
        });
    });

    describe('_isChronoOrderViolated', () => {
        it('does not freak out when called before form construction is complete', () => {
            expect(component._isChronoOrderViolated()).toBe(false);
        });

        it('is false when currently works here is enabled', fakeAsync(() => {
            fixture.detectChanges();
            component._form.controls.currently_works_here.setValue(true);
            tick(1000);

            expect(component._isChronoOrderViolated()).toBe(false);
        }));

        it('is false when not all of the dates have been filled in', fakeAsync(() => {
            fixture.detectChanges();
            component._form.value.currently_works_here = false;

            expect(component._isChronoOrderViolated()).toBe(false);
        }));

        it('is false when the start date is before the end date', fakeAsync(() => {
            fixture.detectChanges();
            component._form.value.currently_works_here = false;
            component._form.value.start_month = 1;
            component._form.value.start_year = 1999;
            component._form.value.end_month = 10;
            component._form.value.end_year = 2009;
            tick(1000);

            expect(component._isChronoOrderViolated()).toBe(false);
        }));

        it('is true when the start date is after the end date', fakeAsync(() => {
            fixture.detectChanges();
            component._form.value.currently_works_here = false;
            component._form.value.start_month = 1;
            component._form.value.start_year = 1999;
            component._form.value.end_month = 10;
            component._form.value.end_year = 1998;
            tick(1000);

            expect(component._isChronoOrderViolated()).toBe(true);
        }));
    });

    describe('ngOnInit', () => {
        it('initializes an empty object when this is a new entry edit', fakeAsync(() => {
            component.entryUnderEditIdx = undefined;

            fixture.detectChanges();

            expect(component._entry).toBeDefined();
        }));

        it('sets the entry to be worked when this is an existing entry edit', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._entry).toBe(mockWorkHistory[0]);
        }));

        it('builds the form', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form).toBeDefined();
            expect(component._form.value.company).toBeDefined();
            expect(component._form.value.title).toBeDefined();
            expect(component._form.value.start_month).toBeDefined();
            expect(component._form.value.start_year).toBeDefined();
            expect(component._form.value.end_month).toBeDefined();
            expect(component._form.value.end_year).toBeDefined();
            expect(component._form.value.description).toBeDefined();
            expect(component._form.value.currently_works_here).toBeDefined();
        }));

        it('attaches onFormChanged to form.valueChanges', fakeAsync(() => {
            spyOn(component, '_onFormChanged');

            fixture.detectChanges();

            component._form.controls.company.markAsDirty();
            component._form.controls.company.setValue('asdfasfd');

            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        }));

        it('sets the form fields to the current state of the entry', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form.value.company).toEqual('ScriptFlipers, Inc');
            expect(component._form.value.title).toEqual('Chief Somethin');
            expect(component._form.value.start_month).toEqual(3);
            expect(component._form.value.start_year).toEqual(2012);
            expect(component._form.value.description).toEqual('Flippin the script');
            expect(component._form.value.currently_works_here).toEqual(true);
        }));
    });

    describe('onFormChanged', () => {
        it('clears the unknown error message', fakeAsync(() => {
            fixture.detectChanges();
            component._unexpectedError = true;

            component._onFormChanged();

            expect(component._unexpectedError).toBe(false);
        }));

        it('dirties end date fields when currently works here is disabled', fakeAsync(() => {
            fixture.detectChanges();
            spyOn(component._formBuilder, 'onFormChanged');
            component._form.controls['currently_works_here'].markAsDirty();

            component._onFormChanged();

            expect(component._form.controls['end_month'].dirty).toBe(true);
            expect(component._form.controls['end_year'].dirty).toBe(true);

            // TODO(mmccann) - Why the double-timer?
            tick(2000);
        }));

        it('dirties all date fields when all are filled and at least one is dirty', fakeAsync(() => {
            fixture.detectChanges();
            spyOn(component._formBuilder, 'onFormChanged');
            component._form.value.start_month = 1;
            component._form.value.start_year = 1999;
            component._form.value.end_month = 1;
            component._form.value.end_year = 1998;
            component._form.controls['end_year'].markAsDirty();

            component._onFormChanged();

            expect(component._form.controls['start_month'].dirty).toBe(true);
            expect(component._form.controls['start_year'].dirty).toBe(true);
            expect(component._form.controls['end_month'].dirty).toBe(true);
            expect(component._form.controls['end_year'].dirty).toBe(true);

            tick(1000);
        }));

        it('accepts valid company values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'company');
        }));

        it('accepts valid title values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'title');
        }));

        it('accepts valid start month values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'start_month', {name: 'April', value: 4});
        }));

        it('accepts valid start year values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'start_year', 2015);
        }));

        it('accepts valid end month values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'end_month', 5);
        }));

        it('accepts valid end year values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'end_year', 2016);
        }));

        it('accepts valid description values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'description');
        }));

        it('rejects missing company values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'company', 'Company');
        }));

        it('rejects missing title values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'title', 'Title');
        }));

        it('rejects missing start month values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'start_month', 'Start month');
        }));

        it('rejects missing start year values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'start_year', 'Start year');
        }));

        it('rejects missing end month values when not currently working there', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.currently_works_here.setValue(false);
            component._form.controls.currently_works_here.markAsDirty();

            // TODO(mmccann) - Get rid of the double delay???
            tick(1000);
            tick(1000);

            expect(component._form.controls['end_month'].valid).toBe(false);
            expect(component._formState['end_month'].accepted).toBe(false);
            expect(component._formState['end_month'].error).toEqual(['End month is required']);
        }));

        it('rejects missing end year values when not currently working there', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.currently_works_here.setValue(false);
            component._form.controls.currently_works_here.markAsDirty();

            // TODO(mmccann) - Get rid of the double delay???
            tick(1000);
            tick(1000);

            expect(component._form.controls['end_year'].valid).toBe(false);
            expect(component._formState['end_year'].accepted).toBe(false);
            expect(component._formState['end_year'].error).toEqual(['End year is required']);
        }));

        it('accepts empty end month values when currently working there', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.currently_works_here.setValue(true);

            checkAcceptValid(fixture, component, 'end_month', undefined);
        }));

        it('accepts missing end year values when currently working there', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.currently_works_here.setValue(true);

            checkAcceptValid(fixture, component, 'end_year', undefined);
        }));

        it('rejects long company values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'company', 64, 'Company');
        }));

        it('rejects long title values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'title', 64, 'Title');
        }));

        it('rejects long description values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'description', 4096, 'Description');
        }));
    });

    describe('_onSubmit', () => {
        let setInput = function() {
            component._form.controls.company.setValue('Companizzle');
            component._form.controls.title.setValue('Boss');
            component._form.controls.start_month.setValue(5);
            component._form.controls.start_year.setValue(2005);
            component._form.controls.end_month.setValue(11);
            component._form.controls.end_year.setValue(2014);
            component._form.controls.description.setValue('Getting paid!');
            component._form.controls.currently_works_here.setValue(false);
            tick(1000);
        };

        beforeEach(fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(developer);
                    }
                };
            });
        }));

        it('updates the entry fields to the form state', fakeAsync(() => {
            fixture.detectChanges();

            setInput();

            component._onSubmit();
            tick(1000); // form.reset timer

            expect(component._entry.company).toEqual('Companizzle');
            expect(component._entry.title).toEqual('Boss');
            expect(component._entry.start_year).toEqual(2005);
            expect(component._entry.start_month).toEqual(5);
            expect(component._entry.start_date).toEqual(2005.05);
            expect(component._entry.currently_works_here).toEqual(false);
            expect(component._entry.end_date).toEqual(2014.11);
            expect(component._entry.end_year).toEqual(2014);
            expect(component._entry.end_month).toEqual(11);
            expect(component._entry.description).toEqual('Getting paid!');
        }));

        it('slices in existing entries', fakeAsync(() => {
            fixture.detectChanges();

            setInput();

            component._onSubmit();
            tick(1000); // form.reset timer

            expect(component.developer.work_history.length).toEqual(2);
            expect(component.developer.work_history[0].end_date).toEqual(2014.11);
            expect(component.developer.work_history[1].end_date).toEqual(2014.09);
        }));

        it('splices in new entries while maintaining chronological order re: end date', fakeAsync(() => {
            component._entryUnderEditIdx = undefined;
            fixture.detectChanges();

            setInput();

            component._onSubmit();
            tick(1000); // form.reset timer

            expect(component.developer.work_history.length).toEqual(3);
            expect(component.developer.work_history[0].end_date).toEqual(undefined);
            expect(component.developer.work_history[1].end_date).toEqual(2014.11);
            expect(component.developer.work_history[2].end_date).toEqual(2014.09);
        }));

        it('splices in a new entry currently worked while maintaining order re: end date', fakeAsync(() => {
            component._entryUnderEditIdx = undefined;
            fixture.detectChanges();

            setInput();

            component._form.controls.currently_works_here.setValue(true);
            tick(1000);

            component._onSubmit();
            tick(1000); // form.reset timer

            expect(component.developer.work_history.length).toEqual(3);
            expect(component.developer.work_history[0].end_date).toEqual(undefined);
            expect(component.developer.work_history[1].end_date).toEqual(2005.05);
            expect(component.developer.work_history[2].end_date).toEqual(2014.09);
        }));

        it('appends new entries that are the chronologically oldest re: end date', fakeAsync(() => {
            component._entryUnderEditIdx = undefined;
            fixture.detectChanges();

            setInput();
            component._form.controls.start_year.setValue(1990);
            component._form.controls.end_year.setValue(1995);
            tick(1000);

            component._onSubmit();
            tick(1000); // form.reset timer

            expect(component.developer.work_history.length).toEqual(3);
            expect(component.developer.work_history[0].end_date).toEqual(undefined);
            expect(component.developer.work_history[1].end_date).toEqual(2014.09);
            expect(component.developer.work_history[2].end_date).toEqual(1995.11);
        }));

        it('resets the form and fires onClose event on successful api call', fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(developer);
                    }
                };
            });

            fixture.detectChanges();
            spyOn(component, '_init');

            component._onSubmit();

            expect(component._init).toHaveBeenCalled();
            expect(component.onClose.emit).toHaveBeenCalled();
        }));

        it('reveals the unknown error message on a failed api call', fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb('error message');
                    }
                };
            });

            fixture.detectChanges();
            component._onSubmit();

            expect(mockDeveloperService.update).toHaveBeenCalled();
            expect(component._unexpectedError).toBe(true);
        }));
    });

    describe('template-input: company field', () => {
        let field;
        let errors;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            field = fixture.nativeElement.querySelector('#company');
            errors = fixture.nativeElement.querySelector('#companyErrors');
        }));

        it('contents set to existing value', fakeAsync(() => {
            expect(field.value).toEqual('ScriptFlipers, Inc');
        }));

        it('positively styles field when a valid input is provided', fakeAsync(() => {
            expect(field.classList.contains('ng-valid')).toBe(true);
            expect(field.classList.contains('ng-invalid')).toBe(false);
            expect(errors.children.length).toBe(0);
        }));

        it('negatively styles field when a bad input occurs', fakeAsync(() => {
            component._form.controls.company.setValue('');
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(false);
            expect(field.classList.contains('ng-invalid')).toBe(true);
        }));

        it('reveals the error message when a bad input occurs', fakeAsync(() => {
            component._form.controls.company.setValue('');
            component._form.controls.company.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(errors.children.length).toBe(1);
        }));
    });

    describe('template-input: title field', () => {
        let field;
        let errors;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            field = fixture.nativeElement.querySelector('#title');
            errors = fixture.nativeElement.querySelector('#titleErrors');
        }));

        it('contents set to existing value', fakeAsync(() => {
            expect(field.value).toEqual('Chief Somethin');
        }));

        it('positively styles field when a valid input is provided', fakeAsync(() => {
            expect(field.classList.contains('ng-valid')).toBe(true);
            expect(field.classList.contains('ng-invalid')).toBe(false);
            expect(errors.children.length).toBe(0);
        }));

        it('negatively styles field when an invalid input is provided', fakeAsync(() => {
            component._form.controls.title.setValue('');
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(false);
            expect(field.classList.contains('ng-invalid')).toBe(true);
        }));

        it('reveals the error message when a bad input is provided', fakeAsync(() => {
            component._form.controls.title.setValue('');
            component._form.controls.title.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(errors.children.length).toBe(1);
        }));
    });

    describe('template-input: start month field', () => {
        it('empty value field is disabled', fakeAsync(() => {
            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#start_month').children[0];
            expect(option.disabled).toBe(true);
            expect(option.innerHTML.trim()).toEqual('month');
        }));

        it('selects the placeholder option when no start month is provided', fakeAsync(() => {
            component.developer.work_history[0].start_month = undefined;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#start_month').children[0];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual('month');
        }));

        it('selects the correct option to match the existing start month', fakeAsync(() => {
            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#start_month').children[3];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual('March');
        }));
    });

    describe('template-input: start year field', () => {
        it('empty value field is disabled', fakeAsync(() => {
            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#start_year').children[0];
            expect(option.disabled).toBe(true);
            expect(option.innerHTML.trim()).toEqual('year');
        }));

        it('selects the placeholder option when no start year is provided', fakeAsync(() => {
            component.developer.work_history[0].start_year = undefined;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#start_year').children[0];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual('year');
        }));

        it('selects the correct option to match the existing start year', fakeAsync(() => {
            component.developer.work_history[0].start_year = new Date().getFullYear() - 1;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#start_year').children[2];
            expect(option.selected).toBe(true)
            expect(option.innerHTML.trim()).toEqual(''+component.developer.work_history[0].start_year);
        }));
    });

    describe('template-input: end month field', () => {
        it('empty value field is disabled', fakeAsync(() => {
            component.developer.work_history[0].currently_works_here = false;
            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#end_month').children[0];
            expect(option.disabled).toBe(true);
            expect(option.innerHTML.trim()).toEqual('month');
        }));

        it('selects the placeholder option when no end month is provided', fakeAsync(() => {
            component.developer.work_history[0].currently_works_here = false;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#end_month').children[0];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual('month');
        }));

        it('selects the correct option to match the existing end month', fakeAsync(() => {
            component.developer.work_history[0].currently_works_here = false;
            component.developer.work_history[0].end_month = 5;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#end_month').children[5];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual('May');
        }));
    });

    describe('template-input: end year field', () => {
        it('empty value field is disabled', fakeAsync(() => {
            component.developer.work_history[0].currently_works_here = false;
            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#end_year').children[0];
            expect(option.disabled).toBe(true);
            expect(option.innerHTML.trim()).toEqual('year');
        }));

        it('selects the placeholder option when no end year is provided', fakeAsync(() => {
            component.developer.work_history[0].currently_works_here = false;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#end_year').children[0];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual('year');
        }));

        it('selects the correct option to match the existing end year', fakeAsync(() => {
            component.developer.work_history[0].currently_works_here = false;
            component.developer.work_history[0].end_year = new Date().getFullYear() - 1;

            fixture.detectChanges();

            let option = fixture.nativeElement.querySelector('#end_year').children[2];
            expect(option.selected).toBe(true);
            expect(option.innerHTML.trim()).toEqual(''+component.developer.work_history[0].end_year);
        }));
    });

    describe('template-input: currently working here field', () => {
        let field;
        let presentLabel;
        let endDateSection;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            field = fixture.nativeElement.querySelector('#currently_works_here');
            presentLabel = fixture.nativeElement.querySelector('#presentLabel');
            endDateSection = fixture.nativeElement.querySelector('#endDate');
        }));

        it('value is set to the current value', fakeAsync(() => {
            expect(field.value).toEqual('on');
        }));

        it('hides end month and end year when selected', fakeAsync(() => {
            expect(endDateSection).toBe(null);
        }));

        it('reveals "Present" when selected', fakeAsync(() => {
            expect(presentLabel.innerHTML.trim()).toEqual('Present');
        }));

        it('reveals end month and end year when deselected', fakeAsync(() => {
            component._form.controls.currently_works_here.setValue(false);
            fixture.detectChanges();
            tick(1000);

            endDateSection = fixture.nativeElement.querySelector('#endDate');

            expect(endDateSection).not.toBe(null);
        }));
    });

    describe('template-input: description field', () => {
        let field;
        let errors;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            field = fixture.nativeElement.querySelector('#description');
            errors = fixture.nativeElement.querySelector('#descriptionErrors');
        }));

        it('contents set to existing value', fakeAsync(() => {
            expect(field.value).toEqual('Flippin the script');
        }));

        it('positively styles field when a valid input is provided', fakeAsync(() => {
            expect(field.classList.contains('ng-valid')).toBe(true);
            expect(field.classList.contains('ng-invalid')).toBe(false);
            expect(errors.children.length).toBe(0);
        }));

        it('negatively styles field when an invalid input is provided', fakeAsync(() => {
            component._form.controls.description.setValue(new Array(4098).join('A'));
            fixture.detectChanges();

            tick(1000);

            expect(field.classList.contains('ng-valid')).toBe(false);
            expect(field.classList.contains('ng-invalid')).toBe(true);
        }));

        it('reveals the error message when a bad input is provided', fakeAsync(() => {
            component._form.controls.description.setValue(new Array(4098).join('A'));
            component._form.controls.description.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(errors.children.length).toBe(1);
        }));
    });

    describe('template-input: submit button', () => {
        let button;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            button = fixture.nativeElement.querySelector('#saveButton');
        }));

        it('is enabled when the form is valid', fakeAsync(() => {
            expect(button.disabled).toBe(false);
        }));

        it('is disabled when the form is invalid', fakeAsync(() => {
            component._form.controls.company.setValue('');
            component._form.controls.company.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(true);
        }));

        xit('is styled to reflects its disabled state when the form is invalid', fakeAsync(() => {
            component._form.controls.company.setValue('');
            component._form.controls.company.markAsDirty();
            fixture.detectChanges();

            tick(1000);
            fixture.detectChanges();

            // TODO: Check the styling is applied
        }));

        it('fires __onSubmit() when clicked', fakeAsync(() => {
            spyOn(component, '_onSubmit');

            button.click();

            expect(component._onSubmit).toHaveBeenCalled();
        }));
    });

    describe('template-input: cancel button', () => {
        it('fires the onClose with an empty value when clicked', fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            fixture.nativeElement.querySelector('#cancelButton').click();

            expect(component.onClose.emit).toHaveBeenCalled();
        }));
    });

    describe('template-view: chrono order violated label', () => {
        it('hidden when order is not violated', fakeAsync(() => {
            spyOn(component, '_isChronoOrderViolated').and.returnValue(false);

            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#chronoOrderViolatedLabel')).toBe(null);
        }));

        it('revealed when order is violated', fakeAsync(() => {
            spyOn(component, '_isChronoOrderViolated').and.returnValue(true);

            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#chronoOrderViolatedLabel').children.length).toBe(1);
        }))
    });
});
