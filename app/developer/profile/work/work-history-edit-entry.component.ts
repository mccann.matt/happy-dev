/**
 * @author R. Matt McCann
 * @brief Provides the user with an interface to create and edit work history entries
 * @copyright &copy; 2017 R. Matt McCann
 */

import 'rxjs/add/operator/debounceTime';

import { Component, Input, EventEmitter, Output } from '@angular/core';
import {
    FormGroup, FormBuilder, FormControl, Validator, Validators, ValidatorFn, AbstractControl
} from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService, FieldState } from '../../../misc/form-builder.service';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';
import { WorkHistoryDetails } from './work-history-details.model';

/** Pairs the integer and text representations of months. */
interface Month {
    name: string;
    value: number;
}

/** Provides an interface for developers to edit their work history entries. */
@Component({
    selector: 'work-history-edit-entry',
    styles: [`
        select.form-control {padding-right:12px}
    `],
    templateUrl: 'work-history-edit-entry.component.html'
})
export class WorkHistoryEditEntryComponent extends ModalFormComponent {
    /** Error message displayed when the dates worked are invalid. */
    _chronoOrderViolatedMessage = {error: ['Start date must not come after end date']};

    /** Developer object being editted. */
    @Input() developer: Developer;

    /** Work history entry being editted. */
    _entry: WorkHistoryDetails;

    /** Index of the entry being editted. */
    _entryUnderEditIdx: number;

    /** List of months of the year used by the form. */
    _monthOptions = new Array<Month>();

    /** List of year options used by the form. */
    _yearOptions = new Array<number>();

    constructor(
        private _developerService: DeveloperService,
        formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder
    ) {
        super(formBuilder);

        let thisYear = new Date().getFullYear();

        // Build the list of year options
        for (let iter = thisYear; iter > thisYear - 70; iter--) {
            this._yearOptions.push(iter);
        }

        // Build the list of month options
        this._monthOptions = [
            {name: 'January', value: 1},
            {name: 'February', value: 2},
            {name: 'March', value: 3},
            {name: 'April', value: 4},
            {name: 'May', value: 5},
            {name: 'June', value: 6},
            {name: 'July', value: 7},
            {name: 'August', value: 8},
            {name: 'September', value: 9},
            {name: 'October', value: 10},
            {name: 'November', value: 11},
            {name: 'December', value: 12}
        ];
    }

    @Input()
    set entryUnderEditIdx(entryUnderEditIdx: number) {
        this._entryUnderEditIdx = entryUnderEditIdx;

        // Update the state of the component
        this._init();
    }

    /** Evaluates whether or not the chronological order of the dates are violated. */
    _isChronoOrderViolated(): boolean {
        // Gracefully handle this being called before the form is built
        if (!this._form) {
            return false;
        }

        // Don't display the error if currently works here
        if (this._form.value.currently_works_here) {
            return false;
        }

        // Don't display the error if not all dates have been filled in
        if (!this._form.value.start_month || !this._form.value.start_year || !this._form.value.end_month ||
                !this._form.value.end_year) {
            return false;
        }

        // Calculate the start and end date
        let startDate = this._form.value.start_year + this._form.value.start_month / 100;
        let endDate = this._form.value.end_year + this._form.value.end_month / 100;

        // Return invalid if the start date is after the end date
        return startDate > endDate;
    }

    /** Initialize the component state. */
    _init(): void {
        // If this is a new work history entry
        if (this._entryUnderEditIdx === undefined) {
            // Initialize an empty work history object as the entry
            this._entry = {
                company: undefined,
                start_date: undefined,
                title: undefined,
                currently_works_here: false,
                end_date: undefined,
                description: undefined
            };
        }
        // If this an entry under edit
        else {
            // Extract the entry from the developer profile so we can edit it
            this._entry = this.developer.work_history[this._entryUnderEditIdx];
        }

        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState([
            'company', 'title', 'start_month', 'start_year', 'end_month', 'end_year',
            'currently_works_here', 'description'
        ]);

        // Configure the form
        this._validationMessages['company'] =
            this._formBuilder.buildValidatorMessages('company', ['required', ['maxlength', 64]]);
        this._validationMessages['title'] =
            this._formBuilder.buildValidatorMessages('title', ['required', ['maxlength', 64]]);
        this._validationMessages['start_month'] =
            this._formBuilder.buildValidatorMessages('start_month', ['required']);
        this._validationMessages['start_year'] =
            this._formBuilder.buildValidatorMessages('start_year', ['required']);
        this._validationMessages['end_month'] =
            this._formBuilder.buildValidatorMessages('end_month', ['required']);
        this._validationMessages['end_year'] =
            this._formBuilder.buildValidatorMessages('end_year', ['required']);
        this._validationMessages['description'] =
            this._formBuilder.buildValidatorMessages('description', [['maxlength', 4096]]);

        let validationRules = new Map<string, Array<Validator>>();
        validationRules['company'] = [Validators.required, Validators.maxLength(64)];
        validationRules['title'] = [Validators.required, Validators.maxLength(64)];
        validationRules['start_month'] = [Validators.required, this._validateChronologicOrder()];
        validationRules['start_year'] = [Validators.required, this._validateChronologicOrder()];
        validationRules['end_month'] = [this._validateRequiredIfNotStillWorksThere(), this._validateChronologicOrder()];
        validationRules['end_year'] = [this._validateRequiredIfNotStillWorksThere(), this._validateChronologicOrder()];
        validationRules['description'] = [Validators.maxLength(4096)];

        // Build the form
        let formGroup = {
            'company': new FormControl(this._entry.company, validationRules['company']),
            'title': new FormControl(this._entry.title, validationRules['title']),
            'start_month': new FormControl(this._entry.start_month, validationRules['start_month']),
            'start_year': new FormControl(this._entry.start_year, validationRules['start_year']),
            'end_month': new FormControl(this._entry.end_month, validationRules['end_month']),
            'end_year': new FormControl(this._entry.end_year, validationRules['end_year']),
            'description': new FormControl(this._entry.description, validationRules['description']),
            'currently_works_here': new FormControl(this._entry.currently_works_here, [])
        };
        this._form = this._ngFormBuilder.group(formGroup);

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Builds a validator function to validate the chronological order of the start date. */
    _validateChronologicOrder(): ValidatorFn {
        let me = this;

        return (control: AbstractControl): {[key: string]: any} => {
            return me._isChronoOrderViolated() ? {chronoOrder: true} : null;
        };
    }

    /** Builds a validator function that requires the end date set when not marked as currently working there. */
    _validateRequiredIfNotStillWorksThere(): ValidatorFn {
        let me = this;

        return (control: AbstractControl): {[key: string]: any} => {
            // Gracefully handle calls before the form is initialized
            if (me._form) {
                // If the form is not currently marked as working there
                if (!me._form.value.currently_works_here) {
                    // Require the end date to be set
                    return control.value !== null ? null : {required: true};
                }
            } else {
                return null;
            }
        };
    }

    /** Process changes in the input form. */
    _onFormChanged(): void {
        // If 'currently works here' is changed, re-evaluate end month and end year
        if (this._form.controls['currently_works_here'].dirty) {
            this._form.controls['end_month'].updateValueAndValidity();
            this._form.controls['end_month'].markAsDirty();
            this._form.controls['end_year'].updateValueAndValidity();
            this._form.controls['end_year'].markAsDirty();
        }

        // If all of the date fields are filled, and one of them changes, reevaluate them all
        if (this._form.value.start_month && this._form.value.start_year &&
                this._form.value.end_month && this._form.value.end_year) {
            if (this._form.controls['start_month'].dirty || this._form.controls['start_year'].dirty ||
                    this._form.controls['end_month'].dirty || this._form.controls['end_year'].dirty) {
                this._form.controls['start_month'].updateValueAndValidity();
                this._form.controls['start_month'].markAsDirty();
                this._form.controls['start_year'].updateValueAndValidity();
                this._form.controls['start_year'].markAsDirty();
                this._form.controls['end_month'].updateValueAndValidity();
                this._form.controls['end_month'].markAsDirty();
                this._form.controls['end_year'].updateValueAndValidity();
                this._form.controls['end_year'].markAsDirty();
            }
        }

        super._onFormChanged();
    }

    /** Process the form submission. */
    _onSubmit(): void {
        // Pack the form input into the entry object
        this._entry.company = this._form.value.company;
        this._entry.title = this._form.value.title;
        this._entry.start_month = this._form.value.start_month;
        this._entry.start_year = this._form.value.start_year;
        this._entry.start_date = this._form.value.start_year + this._form.value.start_month / 100;
        if (!this._form.value.currently_works_here) {
            this._entry.end_month = this._form.value.end_month;
            this._entry.end_year = this._form.value.end_year;
            this._entry.end_date = this._form.value.end_year + this._form.value.end_month / 100;
        } else {
            this._entry.end_date = undefined;
        }
        this._entry.currently_works_here = this._form.value.currently_works_here;
        this._entry.description = this._form.value.description;

        // If this entry was being edited, remove the original from the work history array
        if (this._entryUnderEditIdx !== undefined) {
            this.developer.work_history.splice(this._entryUnderEditIdx, 1);
        }

        // If the developer currently works here
        let inserted = false;
        if (this._entry.currently_works_here) {
            // Insert the entry at the start of the work history
            this.developer.work_history.splice(0, 0, this._entry);
            inserted = true;

            // If there was a previous entry marked as "currently works here"
            if ((this.developer.work_history.length > 1) &&
                    (this.developer.work_history[1].end_date === undefined)) {
                // Set the end date to the start date of this new entry
                this.developer.work_history[1].end_date = this.developer.work_history[0].start_date;
            }
        }
        // If the developer does not currently work here
        else {
            // Find the correct chronological order and insert the entry
            for (let iter = 0; iter < this.developer.work_history.length; iter++) {
                if (this._entry.end_date > this.developer.work_history[iter].end_date) {
                    this.developer.work_history.splice(iter, 0, this._entry);
                    inserted = true;
                    break;
                }
            }
        }
        if (!inserted) {
            this.developer.work_history.push(this._entry);
        }

        // Save the updated profile to the database
        this._developerService.update(this.developer.id, this.developer)
            .then(
                // If the update is a success
                (developer) => {
                    // Report the edit to analytics
                    this._ga.sendEvent('work_history_edit', 'success');

                    // Close the modal
                    this.onClose.emit();

                    // Reset the component state
                    this._init();
                },
                // If the update fails
                (error) => {
                    // Report the error to analytics
                    this._ga.sendEvent('work_history_edit', 'error');

                    // Reveal the unexpected error message
                    this._unexpectedError = true;
                }
            );
    }
}
