/**
 * @author R. Matt McCann
 * @brief Unit tests for the work history details view
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, fakeAsync, TestBed } from '@angular/core/testing';

import { itFA } from '../../../misc/testing/form-test';
import { NewLinesToBrPipe } from '../../../misc/new-lines-to-br.pipe';
import { DeveloperService } from '../../developer.service';
import { WorkHistoryDetailsComponent } from './work-history-details.component';


describe('WorkHistoryDetailsComponent', () => {
    let component, fixture, mockDeveloperService, mockWorkHistory, mockDeveloper;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockWorkHistory = [
            {
                company: 'ScriptFlipers, Inc',
                start_date: 2012.3,
                end_date: undefined,
                description: 'Flippin the script',
                currently_works_here: true,
                title: 'Chief Somethin'
            },
            {
                company: 'NuttFudder, Co',
                start_date: 2012.3,
                end_date: 2014.9,
                description: 'Made pb & j',
                currently_works_here: false,
                title: 'Chief Somebody'
            },
        ];

        TestBed.configureTestingModule({
                declarations: [WorkHistoryDetailsComponent, NewLinesToBrPipe],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService }
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(WorkHistoryDetailsComponent);
                component = fixture.componentInstance;
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.work_history = mockWorkHistory;
                component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('constructor', () => {
        it('configures the property key correctly', () => {
            component._propertyKey = 'work_history';
        })
    });

    describe('template-input: "Add your work history details" button', () => {
        itFA('is hidden when there are work history details', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#addYourWorkDetailsButton')).toBe(null);
        });

        itFA('fires the edit entry request when clicked', () => {
            spyOn(component, '_handleEditEntryRequest');
            component.developer.work_history = [];
            fixture.detectChanges();

            fixture.nativeElement.querySelector('#addYourWorkDetailsButton').click();

            expect(component._handleEditEntryRequest).toHaveBeenCalledWith(undefined);
        });
    });

    describe('template-input: "Add another entry" button', () => {
        itFA('is hidden when there are not work history details', () => {
            component.developer.work_history = [];
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#addAnotherEntryButton')).toBe(null);
        });

        itFA('fires the edit entry request when clicked', () => {
            spyOn(component, '_handleEditEntryRequest');
            fixture.detectChanges();

            fixture.nativeElement.querySelector('#addAnotherEntryButton').click();

            expect(component._handleEditEntryRequest).toHaveBeenCalledWith(undefined);
        });
    });

    describe('template-view: work history entry', () => {
        let entryContainer;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();

            entryContainer = fixture.nativeElement.querySelector('#entryContainer');
        }));

        itFA('displays all entries', () => {
            expect(entryContainer.children.length).toBe(2);
        });

        itFA('fires the edit request when entry is clicked', () => {
            spyOn(component, '_handleEditEntryRequest');

            entryContainer.children[1].click();

            expect(component._handleEditEntryRequest).toHaveBeenCalledWith(component.developer.work_history[1]);
        });

        xit('delets the entry when delete is clicked', () => {

        });

        xit('styles the request as clickable on hover', () => {

        });

        itFA('maintains paragraph structure in the description', () => {
            component.developer.work_history[0].description = 'P1\nP2\nP3';
            fixture.detectChanges();

            expect(entryContainer.querySelector('.work-desc').children.length).toBe(3);
        });
    });
});
