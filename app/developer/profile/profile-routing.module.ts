/**
 * @author R. Matt McCann
 * @brief Routing module for the profile module
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileDetailsComponent } from './profile-details.component';

const routes: Routes = [
    { path: ':id', component: ProfileDetailsComponent }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class ProfileRoutingModule { }
