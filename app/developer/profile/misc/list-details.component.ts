/**
 * @author R. Matt McCann
 * @brief Base class for components displaying a list of editable details of a property
 * @copyright &copy; 2017 R. Matt McCann
 */

import { EventEmitter, Input, Output } from '@angular/core';

import { Developer } from '../../developer.model';
import { DeveloperService} from '../../developer.service';

/** Base class for components displayed a list of editable details of a property. */
export class ListDetailsComponent<T> {
    /** Developer object being editted. */
    @Input() developer: Developer;

    /** CRUD service used for updating the developer record. */
    _developerService: DeveloperService;

    /** Event fired when an entry edit is selected. Fires the index of the entry to be editted. */
    @Output() onOpen = new EventEmitter<number>();

    /** Property being listed. */
    _propertyKey: string;

    /** Marked true when an unexpected error occurs. */
    _unexpectedError = false;

    constructor(developerService: DeveloperService, propertyKey: string) {
        this._developerService = developerService;
        this._propertyKey = propertyKey;
    }

    /** Handles a request by the user to delete an entry. */
    _handleDeleteEntry(entry: T): void {
        // Clear the unexpected error message
        this._unexpectedError = false;

        // Delete the entry
        this.developer[this._propertyKey].splice(
            this.developer[this._propertyKey].indexOf(entry), 1);

        // Update the developer record
        this._developerService.update(this.developer.id, this.developer)
            .then(
                // If the update succeeds
                profile => {
                    // Nothing to do, yay!
                },
                // If the update fails
                error => {
                    // Reveal the unexpected error mesage
                    this._unexpectedError = true;
                }
            );
    }

    /** Handles a request by the user to edit an entry. */
    _handleEditEntryRequest(entry?: T): void {
        // If the entry event doesn't throw an entry index
        if (entry === undefined) {
            // Let's create a new entry
            this.onOpen.emit(undefined);
        // If the entry event contains an entry
        } else {
            // Fire the open event with the entry index
            this.onOpen.emit(this.developer[this._propertyKey].indexOf(entry));
        }
    }
}
