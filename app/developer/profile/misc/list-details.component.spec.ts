/**
 * @author R. Matt McCann
 * @brief Unit tests for listdetailscomponent base class
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';

import { DeveloperService } from '../../developer.service';
import { EducationDetails } from '../education/education-details.model';
import { ListDetailsComponent } from './list-details.component';


@Component({
    selector: 'list-details-component-impl',
    template: ``
})
class ListDetailsComponentImpl extends ListDetailsComponent<EducationDetails> {
    constructor(developerService: DeveloperService) {
        super(developerService, 'education');
    }
}

describe('BaseClass: ListDetailsComponent', () => {
    let component, fixture, mockDeveloperService, mockEducation, mockDeveloper;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockEducation = [
            {
                area_of_study: 'Computer Science & Engineering',
                degree: 'Bachelor of Science',
                description: 'Coding stuff real good',
                end_date: 2012,
                start_date: 2007,
                school: 'OSU'
            },
            {
                area_of_study: 'Computer Science & Engineering',
                degree: 'Masters of Science',
                description: 'Coding stuff real, real good',
                end_date: 2015,
                start_date: 2012,
                school: 'OSU'
            }
        ];

        TestBed.configureTestingModule({
                declarations: [ListDetailsComponentImpl],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService }
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(ListDetailsComponentImpl);
                component = fixture.componentInstance;
                component.propertyKey = 'education';
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.education = mockEducation;
                component.onOpen = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('handleDeleteEntry', () => {
        beforeEach(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(developer);
                    }
                };
            });
        });

        it('clears the unknown error', () => {
            component._unexpectedError = true;

            component._handleDeleteEntry(component.developer.education[0]);

            expect(component._unexpectedError).toBe(false);
        });

        it('deletes the entry', () => {
            component._handleDeleteEntry(component.developer.education[0]);

            expect(component.developer.education.length).toEqual(1);
            expect(component.developer.education[0].end_date).toEqual(2015);
        });

        it('reveals the unknown error on a failed update', () => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb(developer);
                    }
                };
            });

            component._handleDeleteEntry(component.developer.education[0]);

            expect(component.onOpen.emit).not.toHaveBeenCalled();
            expect(component._unexpectedError).toBe(true);
        });
    });

    describe('handleEditEntryRequest', () => {
        it('emits undefined when a new entry is requested', () => {
            component._handleEditEntryRequest();

            expect(component.onOpen.emit).toHaveBeenCalledWith(undefined);
        });

        it('emits the index of the entry to be edited', () => {
            component._handleEditEntryRequest(mockEducation[1]);

            expect(component.onOpen.emit).toHaveBeenCalledWith(1);
        });
    });
});
