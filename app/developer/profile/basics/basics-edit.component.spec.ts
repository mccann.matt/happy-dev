/**
 * @author R. Matt McCann
 * @brief Unit tests for the basics edit view
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AccountService } from '../../../account/account.service';
import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import {
    beforeEachFormUiTest, checkAcceptValid, checkEmptyRequirement, checkLongRequirement, itFA,
    checkNegativelyStyles, checkPositivelyStyles,
    checkNaturalEnglishRequirement
} from '../../../misc/testing/form-test';
import { DeveloperService} from '../../developer.service';
import { BasicsEditComponent } from './basics-edit.component';


describe('BasicsEditComponent', () => {
    let component, fixture, mockAccount, mockAccountService, mockDeveloper, mockDeveloperService, mockGa;

    beforeEach(async(() => {
        mockAccount = jasmine.createSpy('Account');
        mockAccountService = jasmine.createSpyObj('AccountService', ['update']);
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockDeveloper.profile = jasmine.createSpy('Profile');
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [BasicsEditComponent],
                schemas: [ NO_ERRORS_SCHEMA ],
                providers: [
                    { provide: AccountService, useValue: mockAccountService },
                    { provide: DeveloperService, useValue: mockDeveloperService },
                    { provide: GoogleAnalytics, useValue: mockGa },
                    FormBuilderService
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(BasicsEditComponent);
                component = fixture.componentInstance;
                component.account = mockAccount;
                component.account.id = 'profile_id';
                component.account.first_name = 'Matt';
                component.account.last_name = 'McCann';
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.title = 'King of Turd Mountain';
                component.developer.city = 'Columbus';
                component.developer.state = 'Ohio';
                component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('_init', () => {
        it('builds the form', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form).toBeDefined();
            expect(component._form.value.first_name).toBeDefined();
            expect(component._form.value.last_name).toBeDefined();
            expect(component._form.value.title).toBeDefined();
            expect(component._form.value.city).toBeDefined();
            expect(component._form.value.state).toBeDefined();
        }));

        it('attaches onFormChanged to form.valueChanges', fakeAsync(() => {
            spyOn(component, '_onFormChanged');

            fixture.detectChanges();

            component._form.controls.first_name.markAsDirty();
            component._form.controls.first_name.setValue('asdfasdfasdf');

            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        }));

        it('sets the form fields to the current state of the profile', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form.value.first_name).toEqual('Matt');
            expect(component._form.value.last_name).toEqual('McCann');
            expect(component._form.value.title).toEqual('King of Turd Mountain');
            expect(component._form.value.city).toEqual('Columbus');
            expect(component._form.value.state).toEqual('Ohio');
        }));
    });

    describe('_onFormChanged', () => {
        it('invalidates empty first name values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'first_name', 'First name');
        }));

        it('invalidates long first name values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'first_name', 25, 'First name');
        }));

        it('invalidates malformed first name values', fakeAsync(() => {
            checkNaturalEnglishRequirement(fixture, component, 'first_name', 'First name');
        }));

        it('accepts valid first name values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'first_name');
        }));

        it('invalidates empty last name values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'last_name', 'Last name');
        }));

        it('invalidates long last name values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'last_name', 25, 'Last name');
        }));

        it('invalidates malformed last name values', fakeAsync(() => {
            checkNaturalEnglishRequirement(fixture, component, 'last_name', 'Last name');
        }));

        it('accepts valid last name values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'last_name');
        }));

        it('invalidates empty title values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'title', 'Title');
        }));

        it('invalidates long title values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'title', 25, 'Title');
        }));

        it('invalidates malformed title values', fakeAsync(() => {
            checkNaturalEnglishRequirement(fixture, component, 'title', 'Title');
        }));

        it('accepts valid title values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'title');
        }));

        it('invalidates empty city values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'city', 'City');
        }));

        it('invalidates long city values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'city', 25, 'City');
        }));

        it('invalidates malformed city values', fakeAsync(() => {
            checkNaturalEnglishRequirement(fixture, component, 'city', 'City');
        }));

        it('accepts valid city values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'city');
        }));

        it('invalidates missing state values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'state', 'State');
        }));

        it('accepts valid state values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'state');
        }));
    });

    describe('_onSubmit', () => {
        let submitValue;

        beforeEach(fakeAsync(() => {
            submitValue = {
                first_name: 'New first_name',
                last_name: 'New last_name',
                title: 'New title',
                city: 'New city',
                state: 'New state'
            };

            mockAccountService.update.and.returnValue({
                then: (then_cb, catch_cb) => { }
            });

            fixture.detectChanges();
            component._form.setValue(submitValue);
            tick(1000);
        }));

        it('updates the profile fields to the form state', fakeAsync(() => {
            component._onSubmit();

            expect(component.account.first_name).toEqual('New first_name');
            expect(component.account.last_name).toEqual('New last_name');
            expect(component.developer.title).toEqual('New title');
            expect(component.developer.city).toEqual('New city');
            expect(component.developer.state).toEqual('New state');
        }));

        it('tries to update the account record', () => {
            component._onSubmit();

            expect(mockAccountService.update).toHaveBeenCalledWith('profile_id', component.account);
        });

        describe('after successful account update', () => {
            beforeEach(() => {
                mockAccountService.update.and.returnValue({
                    then: (then_cb, catch_cb) => { then_cb(component.account); }
                });

                mockDeveloperService.update.and.returnValue({
                    then: (then_cb, catch_cb) => { }
                });
            });

            it('tries to update the developer record', () => {
                component._onSubmit();

                expect(mockDeveloperService.update).toHaveBeenCalledWith('profile_id', component.developer);
            });

            describe('after successful developer update', () => {
                beforeEach(() => {
                    mockDeveloperService.update.and.returnValue({
                        then: (then_cb, catch_cb) => { then_cb(component.developer); }
                    });
                });

                it('fires the close modal event', () => {
                    component._onSubmit();

                    expect(component.onClose.emit).toHaveBeenCalled();
                });

                it('resets the component state', () => {
                    spyOn(component, '_init');

                    component._onSubmit();

                    expect(component._init).toHaveBeenCalled();
                });
            });

            describe('after failed developer update', () => {
                beforeEach(() => {
                    mockDeveloperService.update.and.returnValue({
                        then: (then_cb, catch_cb) => { catch_cb('fuck!') }
                    });
                });

                it('reveals the unexpected error message', () => {
                    component._onSubmit();

                    expect(component._unexpectedError).toBe(true);
                });
            });
        });

        describe('after failed account update', () => {
            beforeEach(() => {
                mockAccountService.update.and.returnValue({
                    then: (then_cb, catch_cb) => { catch_cb('fuck!') }
                })
            });

            it('reveals the unexpected error message', () => {
                component._onSubmit();

                expect(component._unexpectedError).toBe(true);
            });
        });
    });

    describe('template-input: first name field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'first_name');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.first_name.setValue('matt');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.first_name.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template-input: last name field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'last_name');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.last_name.setValue('mccann');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.last_name.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template-input: title field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'title');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.title.setValue('King of the Universe');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.title.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template-input: city field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'city');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.city.setValue('Cbus');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.city.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template-input: state field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'state');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.state.setValue('Ohio');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.state.setValue('');

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: unexpected error message', () => {
        itFA('revealed when unexpected error set', () => {
            component._unexpectedError = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).not.toBe(null);
        });

        itFA('hidden when unexpected error not set', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).toBe(null);
        });
    });

    describe('template-input: save button', () => {
        let button;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();

            button = fixture.nativeElement.querySelector('#saveButton');
        }));

        itFA('disabled when form is invalid', () => {
            component._form.controls.first_name.setValue('');
            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(true);
        });

        itFA('enabled when form is valid', () => {
            expect(button.disabled).toBe(false);
        });

        itFA('fires _onSubmit when clicked', () => {
            spyOn(component, '_onSubmit');

            button.click();

            expect(component._onSubmit).toHaveBeenCalled();
        });
    });

    describe('template-input: cancel button', () => {
        itFA('calls _onCancel when clicked', () => {
            fixture.detectChanges();
            spyOn(component, '_onCancel');

            fixture.nativeElement.querySelector('#cancelButton').click();

            expect(component._onCancel).toHaveBeenCalled();
        });
    });
});
