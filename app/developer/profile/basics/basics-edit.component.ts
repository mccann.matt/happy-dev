/**
 * @author R. Matt McCann
 * @brief Provides the developer with an interface to edit basic profile information
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validator, Validators } from '@angular/forms';

import { Account } from '../../../account/account.model';
import { AccountService } from '../../../account/account.service';
import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';


/** Provides the developer with an interface to edit basic profile information. */
@Component({
    selector: 'basics-edit',
    templateUrl: 'basics-edit.component.html'
})
export class BasicsEditComponent extends ModalFormComponent {
    /** User account which is partially editted by the basics. */
    @Input() account: Account;

    /** Developer object being editted. */
    @Input() developer: Developer;

    constructor(
        private _accountService: AccountService,
        private _developerService: DeveloperService,
        _formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder
    ) {
        super(_formBuilder);
    }

    /** Initialize the object state */
    _init(): void {
        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(['first_name', 'last_name', 'title', 'city', 'state']);

        // Configure the form
        this._validationMessages['first_name'] =
            this._formBuilder.buildValidatorMessages('first_name', ['required', ['maxlength', 25], 'pattern']);
        this._validationMessages['last_name'] =
            this._formBuilder.buildValidatorMessages('last_name', ['required', ['maxlength', 25], 'pattern']);
        this._validationMessages['title'] =
            this._formBuilder.buildValidatorMessages('title', ['required', ['maxlength', 25], 'pattern']);
        this._validationMessages['city'] =
            this._formBuilder.buildValidatorMessages('city', ['required', ['maxlength', 25], 'pattern']);
        this._validationMessages['state'] =
            this._formBuilder.buildValidatorMessages('state', ['required']);

        let validationRules = new Map<string, Array<Validator>>();
        validationRules['first_name'] = [
            Validators.required, Validators.maxLength(25), Validators.pattern(this._formBuilder.naturalEnglishRegex)];
        validationRules['last_name'] = [
            Validators.required, Validators.maxLength(25), Validators.pattern(this._formBuilder.naturalEnglishRegex)];
        validationRules['title'] = [
            Validators.required, Validators.maxLength(25), Validators.pattern(this._formBuilder.naturalEnglishRegex)];
        validationRules['city'] = [
            Validators.required, Validators.maxLength(25), Validators.pattern(this._formBuilder.naturalEnglishRegex)];
        validationRules['state'] = [Validators.required];

        // Build the form
        let formGroup = {
            'first_name': new FormControl(this.account.first_name, validationRules['first_name']),
            'last_name': new FormControl(this.account.last_name, validationRules['last_name']),
            'title': new FormControl(this.developer.title, validationRules['title']),
            'city': new FormControl(this.developer.city, validationRules['city']),
            'state': new FormControl(this.developer.state, validationRules['state'])
        };
        this._form = this._ngFormBuilder.group(formGroup);

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Process the form submission. */
    _onSubmit(): void {
        // Update the developer model with the editted values
        this.account.first_name = this._form.value.first_name;
        this.account.last_name = this._form.value.last_name;
        this.developer.title = this._form.value.title;
        this.developer.city = this._form.value.city;
        this.developer.state = this._form.value.state;

        // Update the account record
        this._accountService.update(this.account.id, this.account).then(
            // If the account update succeeds
            (account) => {
                // Update the developer record
                this._developerService.update(this.developer.id, this.developer).then(
                    // If the update operation succeeds
                    (developer) => {
                        // Report the basics edit to analytics
                        this._ga.sendEvent('basics_edit', 'success');

                        // Close the modal
                        this.onClose.emit();

                        // Reset the state of the component
                        this._init();
                    },
                    // If the update operation fails
                    (error) => {
                        // Report the error to analytics
                        this._ga.sendEvent('basics_edit', 'dev_update_error');

                        // Reveal the unexpected error
                        this._unexpectedError = true;
                    }
                );
            },
            // If the update operatins fails
            (error) => {
                // Report the error to analytics
                this._ga.sendEvent('basics_edit', 'account_update_error');

                // Reveal the unexpected error message
                this._unexpectedError = true;
            }
        );
    }
};
