/**
 * @author R. Matt McCann
 * @brief Provides the developer with an interface to edit their overview paragraph
 * @copyright &copy; 2017 R. Matt McCann
 */

import 'rxjs/add/operator/debounceTime';

import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validator, Validators } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';


/** Provides the developer with an interface to edit their profile overview. */
@Component({
    selector: 'overview-edit',
    templateUrl: 'overview-edit.component.html'
})
export class OverviewEditComponent extends ModalFormComponent {
    /** Developer object being editted. */
    @Input() developer: Developer;

    constructor(
        private _developerService: DeveloperService,
        _formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder
    ) {
        super(_formBuilder);
    }

    /** Initializes the component state. */
    _init(): void {
        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(['overview']);

        // Configure the form
        this._validationMessages['overview'] =
            this._formBuilder.buildValidatorMessages('overview', ['required', ['maxlength', 4096]]);

        let validationRules = new Map<string, Array<Validator>>();
            validationRules['overview'] = [Validators.required, Validators.maxLength(4096)];

        // Build the form
        let formGroup = {'overview': new FormControl(this.developer.overview, validationRules['overview'])};
        this._form = this._ngFormBuilder.group(formGroup);

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Process the form on submission. */
    _onSubmit(): void {
        // Update the developer model with the editted values
        this.developer.overview = this._form.value['overview'];

        // Update the database record
        this._developerService.update(this.developer.id, this.developer)
            .then(
                // If the update operation succeeds
                (developer) => {
                    // Report the edit to analytics
                    this._ga.sendEvent('overview_edit', 'success');

                    // Close the edit modal
                    this.onClose.emit();

                    // Reset the state of the component
                    this._init();
                },
                // If the update operation fails
                (error) => {
                    // Report the error to analytics
                    this._ga.sendEvent('overview_edit', 'error');

                    // Reveal the unexpected error
                    this._unexpectedError = true;
                }
            );
    }
};
