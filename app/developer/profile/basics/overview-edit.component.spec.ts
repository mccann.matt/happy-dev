/**
 * @author R. Matt McCann
 * @brief Unit tests for the OverviewEditComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import {
    beforeEachFormUiTest, checkAcceptValid, checkEmptyRequirement, checkLongRequirement, checkAcceptMissing, itFA,
    checkNegativelyStyles, checkPositivelyStyles, checkMinLengthRequirement, checkRevealsErrorMessage,
    checkNaturalEnglishRequirement
} from '../../../misc/testing/form-test';
import { Developer } from '../../developer.model';
import { DeveloperService} from '../../developer.service';
import { OverviewEditComponent } from './overview-edit.component';


describe('OverviewEditComponent', () => {
    let component, fixture, mockDeveloperService, mockDeveloper, mockGa;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockDeveloper.profile = jasmine.createSpy('Profile');
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [OverviewEditComponent],
                schemas: [ NO_ERRORS_SCHEMA ],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService},
                    { provide: GoogleAnalytics, useValue: mockGa },
                    FormBuilderService
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(OverviewEditComponent);
                component = fixture.componentInstance;
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.overview = 'sample overview';
                component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('_init', () => {
        it('builds the form', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form).toBeDefined();
            expect(component._form.value.overview).toBeDefined();
        }));

        it('attaches onFormChanged to form.valueChanges', fakeAsync(() => {
            spyOn(component, '_onFormChanged');

            fixture.detectChanges();

            component._form.controls.overview.markAsDirty();
            component._form.controls.overview.setValue('something else');

            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        }));

        it('sets the form fields to the current state of the profile', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form.value.overview).toEqual('sample overview');
        }));
    });

    describe('_onFormChanged', () => {
        it('invalidates empty overview values', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.overview.markAsDirty();
            component._form.controls.overview.setValue('');

            tick(1000);

            expect(component._form.controls.overview.valid).toBe(false);
            expect(component._formState.overview.accepted).toBe(false);
            expect(component._formState.overview.error).toEqual(['Overview is required']);
        }));

        it('invalidates overview values that are too long', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.overview.markAsDirty();
            component._form.controls.overview.setValue(new Array(4098).join('A'));

            tick(1000);

            expect(component._form.controls.overview.valid).toBe(false);
            expect(component._formState.overview.accepted).toBe(false);
            expect(component._formState.overview.error).toEqual(['Overview cannot be more than 4096 chacters long']);
        }));

        it('accepts correct overview values', fakeAsync(() => {
            fixture.detectChanges();

            component._form.controls.overview.markAsDirty();
            component._form.controls.overview.setValue('A valid overview!');

            tick(1000);

            expect(component._form.controls.overview.valid).toBe(true);
            expect(component._formState.overview.accepted).toBe(true);
            expect(component._formState.overview.error).toEqual([]);
        }));
    });

    describe('_onSubmit', () => {
        it('updates the developer fields to the form state', fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(developer);
                    }
                };
            });

            fixture.detectChanges();

            component._form.controls.overview.setValue('A new overview!');
            tick(1000);

            component._onSubmit();
            tick(1000); // Form.reset timer

            expect(component.developer.overview).toEqual('A new overview!');
        }));

        it('resets the form and emits if update call is successful', fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(developer);
                    }
                };
            });

            fixture.detectChanges();
            spyOn(component, '_init');

            component._onSubmit();
            tick(1000); // Form.reset timer

            expect(mockDeveloperService.update).toHaveBeenCalledWith(mockDeveloper.id, mockDeveloper);
            expect(component._init).toHaveBeenCalled();
            expect(component.onClose.emit).toHaveBeenCalled();
        }));

        it('reveals the expected error message if update call fails', fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb('error message');
                    }
                };
            });

            fixture.detectChanges();
            component._onSubmit();

            expect(mockDeveloperService.update).toHaveBeenCalledWith(mockDeveloper.id, mockDeveloper);
            expect(component._unexpectedError).toBe(true);
        }));
    });

    describe('template-input: overview field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'overview');
        }));

        itFA('positively styles field when good input is provided', () => {
            component._form.controls.overview.setValue('overview');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field and reveals error message when bad input is provided', () => {
            component._form.controls.overview.setValue(new Array(10000).join('A'));

            checkNegativelyStyles(fixture, elem);
        });
    });

    describe('template: unexpected error message', () => {
        itFA('revealed when unexpected error set', () => {
            component._unexpectedError = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).not.toBe(null);
        });

        itFA('hidden when unexpected error is not set', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).toBe(null);
        });
    });

    describe('template-input: submit button', () => {
        let button;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();

            button = fixture.nativeElement.querySelector('#saveButton');
        }));

        itFA('disabled when form is invalid', () => {
            component._form.controls.overview.setValue(new Array(10000).join('A'));
            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(true);
        });

        itFA('enabled when form is valid', () => {
            expect(button.disabled).toBe(false);
        });

        itFA('fires _onSubmit when clicked', () => {
            spyOn(component, '_onSubmit');

            button.click();

            expect(component._onSubmit).toHaveBeenCalled();
        });
    })

    describe('template-input: cancel button', () => {
        itFA('calls _onCancel when clicked', () => {
            fixture.detectChanges();
            spyOn(component, '_onCancel');

            fixture.nativeElement.querySelector('#cancelButton').click();

            expect(component._onCancel).toHaveBeenCalled();
        });
    });
});
