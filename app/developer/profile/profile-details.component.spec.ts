/**
 * @author R. Matt McCann
 * @brief Unit tests for the ProfileDetailsComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { NewLinesToBrPipe } from '../../misc/new-lines-to-br.pipe';
import { itFA } from '../../misc/testing/form-test';
import { SectionType } from '../developer.model';
import { DeveloperService} from '../developer.service';
import { ProfileDetailsComponent } from './profile-details.component';


describe('ProfileDetailsComponent', () => {
    let component, fixture, mockRouter;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
                declarations: [ ProfileDetailsComponent, NewLinesToBrPipe ],
                providers: [ ],
                schemas: [ NO_ERRORS_SCHEMA ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(ProfileDetailsComponent);
                component = fixture.componentInstance;
                component._editBasicsComp = jasmine.createSpyObj('ModalFormComponent', ['dismiss']);
                component._editEducationComp = jasmine.createSpyObj('ModalFormComponent', ['dismiss']);
                component._editOverviewComp = jasmine.createSpyObj('ModalFormComponent', ['dismiss']);
                component._editPortraitComp = jasmine.createSpyObj('ModalFormComponent', ['dismiss']);
                component._editSkillsComp = jasmine.createSpyObj('ModalFormComponent', ['dismiss']);
                component._editWorkHistoryComp = jasmine.createSpyObj('ModalFormComponent', ['dismiss']);
                component._editPortraitModal = jasmine.createSpyObj('ModalFormComponent', ['close', 'open']);
                component._editBasicsModal = jasmine.createSpyObj('Modal', ['open']);
                component._editSkillsModal = jasmine.createSpyObj('Modal', ['open']);
                component._editOverviewModal = jasmine.createSpyObj('Modal', ['open']);
                component._editEducationModal = jasmine.createSpyObj('Modal', ['open']);
                component._editWorkHistoryModal = jasmine.createSpyObj('Modal', ['open']);
                component.onDeveloperUpdate = jasmine.createSpyObj('Emitter', ['emit']);
            });
    }));

    describe('_onDismissEditBasicsModal', () => {
        it('dismisses the edit basics modal', () => {
            component._onDismissEditBasicsModal();

            expect(component._editBasicsComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onDismissEditEducationModal', () => {
        it('dismisses the edit education entry modal', () => {
            component._onDismissEditEducationModal();

            expect(component._editEducationComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onDismissEditOverviewModal', () => {
        it('dismisses the edit overview modal', () => {
            component._onDismissEditOverviewModal();

            expect(component._editOverviewComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onDismissEditPortraitModal', () => {
        it('dismisses the edit overview modal', () => {
            component._onDismissEditPortraitModal();

            expect(component._editPortraitComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onDismissEditSkillsModal', () => {
        it('dismisses the edit skills modal', () => {
            component._onDismissEditSkillsModal();

            expect(component._editSkillsComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onDismissEditWorkHistoryModal', () => {
        it('dismisses the edit work history modal', () => {
            component._onDismissEditWorkHistoryModal();

            expect(component._editWorkHistoryComp.dismiss).toHaveBeenCalled();
        });
    });

    describe('_onOpenEducation', () => {
        itFA('sets the index and reveals the modal', () => {
            fixture.detectChanges();
            component._editEducationModal = jasmine.createSpyObj('modal', ['open']);

            component._onOpenEducation(2);

            expect(component._editEducationIdx).toEqual(2);
            expect(component._editEducationModal.open).toHaveBeenCalled();
        });
    });

    describe('_onOpenWorkHistory', () => {
        itFA('sets the index and reveals the modal', () => {
            fixture.detectChanges();
            component._editWorkHistoryModal = jasmine.createSpyObj('modal', ['open']);

            component._onOpenWorkHistory(2);

            expect(component._editWorkHistoryIdx).toEqual(2);
            expect(component._editWorkHistoryModal.open).toHaveBeenCalled();
        });
    });

    describe('_onEditPortraitClose', () => {
        it('closes the edit modal', () => {
            let developer = jasmine.createSpy('Developer');

            component._onEditPortraitClose(developer);

            expect(component.onDeveloperUpdate.emit).toHaveBeenCalled();
            expect(component._editPortraitModal.close).toHaveBeenCalled();
        });
    });

    describe('openModal', () => {
        it('opens the portait modal', () => {
            component.openModal(SectionType.PORTRAIT);

            expect(component._editPortraitModal.open).toHaveBeenCalled();
        });

        it('opens the basics info modal', () => {
            component.openModal(SectionType.BASIC_INFO);

            expect(component._editBasicsModal.open).toHaveBeenCalled();
        });

        it('opens the skill tag modal', () => {
            component.openModal(SectionType.SKILL_TAGS);

            expect(component._editSkillsModal.open).toHaveBeenCalled();
        });

        it('opens the overview modal', () => {
            component.openModal(SectionType.OVERVIEW);

            expect(component._editOverviewModal.open).toHaveBeenCalled();
        });

        it('opens the education modal', () => {
            component.openModal(SectionType.EDUCATION);

            expect(component._editEducationModal.open).toHaveBeenCalled();
        });

        it('opens the work history modal', () => {
            component.openModal(SectionType.WORK_HISTORY);

            expect(component._editWorkHistoryModal.open).toHaveBeenCalled();
        });
    });

    describe('template-input: portrait-click', () => {
        xit('reveals the portrait edit view', () => {

        });
    });

    describe('template-input: basics-click', () => {
        xit('reveals the basics edit view', () => {

        });
    });

    describe('template-input: skills-click', () => {
        xit('reveals the skills edit view', () => {

        });
    });

    describe('template-input: overview-click', () => {
        xit('reveals the overview edit view', () => {

        });
    });
});
