/**
 * @author R. Matt McCann
 * @brief Defines the Angular module for the profile views
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageCropperModule } from 'ng2-img-cropper';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import { BasicsEditComponent } from './basics/basics-edit.component';
import { OverviewEditComponent } from './basics/overview-edit.component';
import { DegreeService } from './education/degree.service';
import { EducationEditEntryComponent } from './education/education-edit-entry.component';
import { EducationDetailsComponent } from './education/education-details.component';
import { SharedModule } from '../../misc/shared.module';
import { PortraitEditComponent } from './portrait/portrait-edit.component';
import { ProfileDetailsComponent } from './profile-details.component';
import { QuestionModule } from '../question/question.module';
import { SkillsEditComponent } from './skills/skills-edit.component';
import { SkillsService } from './skills/skills.service';
import { SkillTagComponent } from './skills/skill-tag.component';
import { WorkHistoryDetailsComponent } from './work/work-history-details.component';
import { WorkHistoryEditEntryComponent } from './work/work-history-edit-entry.component';

export let MODULE_DEF = {
    imports: [
        CommonModule,
        FormsModule,
        Ng2Bs3ModalModule,
        //QuestionModule,
        ReactiveFormsModule,
        FileUploadModule,
        ImageCropperModule,
        SharedModule
    ],
    exports: [
        BasicsEditComponent,
        EducationEditEntryComponent,
        EducationDetailsComponent,
        PortraitEditComponent,
        OverviewEditComponent,
        ProfileDetailsComponent,
        SkillsEditComponent,
        SkillTagComponent,
        WorkHistoryDetailsComponent,
        WorkHistoryEditEntryComponent
    ],
    declarations: [
        BasicsEditComponent,
        EducationEditEntryComponent,
        EducationDetailsComponent,
        PortraitEditComponent,
        OverviewEditComponent,
        ProfileDetailsComponent,
        SkillsEditComponent,
        SkillTagComponent,
        WorkHistoryDetailsComponent,
        WorkHistoryEditEntryComponent
    ],
    providers: [
        DegreeService,
        SkillsService
    ]
};

@NgModule(MODULE_DEF)
export class ProfileModule { }
