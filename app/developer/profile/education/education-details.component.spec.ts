/**
 * @author R. Matt McCann
 * @brief Unit tests for education details view
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, TestBed, fakeAsync } from '@angular/core/testing';

import { NewLinesToBrPipe } from '../../../misc/new-lines-to-br.pipe';
import { itFA } from '../../../misc/testing/form-test';
import { DeveloperService} from '../../developer.service';
import { EducationDetailsComponent } from './education-details.component';


describe('EducationDetailsComponent', () => {
    let component, fixture, mockDeveloperService, mockEducation, mockDeveloper;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockEducation = [
            {
                area_of_study: 'Computer Science & Engineering',
                degree: 'Bachelor of Science',
                description: 'Coding stuff real good',
                end_year: 2012,
                start_year: 2007,
                school: 'OSU'
            },
            {
                area_of_study: 'Computer Science & Engineering',
                degree: 'Masters of Science',
                description: 'Coding stuff real, real good',
                end_year: 2015,
                start_year: 2012,
                school: 'OSU'
            }
        ];

        TestBed.configureTestingModule({
                declarations: [EducationDetailsComponent, NewLinesToBrPipe],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService }
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(EducationDetailsComponent);
                component = fixture.componentInstance;
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.education = mockEducation;
                component.onOpen = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('constructor', () => {
        it('configures the property key', () => {
            expect(component._propertyKey).toEqual('education');
        });
    });
    
    describe('template-input: "Add your education details" button', () => {
        itFA('is hidden when there are education details', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#addYourEducationDetailsButton')).toBe(null);
        });

        itFA('fires the edit entry request when clicked', () => {
            spyOn(component, '_handleEditEntryRequest');
            component.developer.education = [];
            fixture.detectChanges();

            fixture.nativeElement.querySelector('#addYourEducationDetailsButton').click();

            expect(component._handleEditEntryRequest).toHaveBeenCalledWith();
        });
    });

    describe('template-input: "Add another entry" button', () => {
        itFA('is hidden when there are not education details', () => {
            component.developer.education = [];
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#addAnotherEntryButton')).toBe(null);
        });

        itFA('fires the edit entry request when clicked', () => {
            spyOn(component, '_handleEditEntryRequest');
            fixture.detectChanges();

            fixture.nativeElement.querySelector('#addAnotherEntryButton').click();

            expect(component._handleEditEntryRequest).toHaveBeenCalledWith();
        });
    });

    describe('template-view: education entry', () => {
        let entryContainer;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();

            entryContainer = fixture.nativeElement.querySelector('#entryContainer');
        }));

        itFA('displays all entries', () => {
            expect(entryContainer.children.length).toBe(2);
        });

        itFA('fires the edit request when entry is clicked', () => {
            spyOn(component, '_handleEditEntryRequest');

            entryContainer.children[1].click();

            expect(component._handleEditEntryRequest).toHaveBeenCalledWith(component.developer.education[1]);
        });

        xit('deletes the entry when delete is clicked', () => {

        });

        xit('styles the request as clickable on hover', () => {

        });

        itFA('maintains paragraph structure in the description', () => {
            component.developer.education[0].description = 'P1\nP2\nP3';
            fixture.detectChanges();

            expect(entryContainer.querySelector('.educ-description').children.length).toBe(3);
        });
    });
});
