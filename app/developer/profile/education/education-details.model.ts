/**
 * @author R. Matt McCann
 * @brief Model for the developer's education details
 * @copyright &copy; 2017 R. Matt McCann
 */

/** Details regarding a developer's education. */
export interface EducationDetails {
    /** Type of study e.g. Computer Science, Physics, etc. */
    area_of_study: string;

    /** Type of degree e.g. Bachelor of Science */
    degree: string;

    /** Description of the educational experience */
    description?: string;

    /** Year the degree was finished (or projected to be finished) */
    end_year: number;

    /** Name of the school attended. */
    school: string;

    /** Year the degree was started. */
    start_year: number;
}
