/**
 * @author R. Matt McCann
 * @brief Http service for hitting the Degrees API
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { buildGetSuggestions } from '../../../misc/suggestion.service';
import { Degree } from './degree.model';


@Injectable()
export class DegreeService {
    _degreeUrl = process.env.API_SERVER + '/degree/';

    getSuggestions: (fragment: any) => Observable<Degree[]>;

    constructor(private _http: Http) {
        this.getSuggestions = buildGetSuggestions<Degree>(this._degreeUrl + 'suggest/', _http);
    }
};
