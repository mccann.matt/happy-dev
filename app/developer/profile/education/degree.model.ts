/**
 * @author R. Matt McCann
 * @brief Model for the degree type
 * @copyright &copy; 2017 R. Matt McCann
 */

/** Model for types of degrees. */
export interface Degree {
    /** Display name of the degree. */
    name: string;

    /** The searchable version of the display name. */
    name_suggest: string;
};
