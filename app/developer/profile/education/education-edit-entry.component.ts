/**
 * @author R. Matt McCann
 * @brief Provides the user with an interface to create and edit eudcation entries
 * @copyright &copy; 2017 R. Matt McCann
 */

import 'rxjs/add/operator/debounceTime';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AfterViewInit, Component, ElementRef, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import {
    FormGroup, FormBuilder, FormControl, Validator, ValidatorFn, AbstractControl, Validators
} from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService, FieldState } from '../../../misc/form-builder.service';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { SuggestionField } from '../../../misc/suggestion.field';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';
import { EducationDetails } from './education-details.model';
import { DegreeService } from './degree.service';
import { Degree } from './degree.model';


@Component({
    selector: 'education-edit-entry',
    templateUrl: 'education-edit-entry.component.html'
})
export class EducationEditEntryComponent extends ModalFormComponent implements AfterViewInit {
    /** Error message displayed when the years attend range is invalid. */
    _chronoOrderViolatedMessage = {error: ['Start year must not come after end year']};

    /** Raw suggestion field for the degree type. */
    @ViewChild("degree") private _degreeField: ElementRef;

    /** Suggestion field for the degree type. */
    _degreeSuggestion = new SuggestionField<Degree>('degree', 'name');

    /** Developer account being edited. */
    @Input() developer: Developer;

    /** Options for the year the degree was/will be finished. */
    _endYearOptions = new Array<number>();

    /** Education entry being created/edited. */
    _entry: EducationDetails;

    /** Index of the education entry to be editted. */
    _entryUnderEditIdx: number;

    /** Options for the year the degree was started. */
    _startYearOptions = new Array<number>();

    constructor(
        private _degreeService: DegreeService,
        private _developerService: DeveloperService,
        formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder
    ) {
        super(formBuilder);

        let thisYear = new Date().getFullYear();

        // Build the list of start year options
        for (let iter = thisYear; iter > thisYear - 70; iter--) {
            this._startYearOptions.push(iter);
        }

        // Build the list of end year options
        for (let iter = thisYear+7; iter > thisYear - 70; iter--) {
            this._endYearOptions.push(iter);
        }
    }

    @Input()
    set entryUnderEditIdx(entryUnderEditIdx: number) {
        this._entryUnderEditIdx = entryUnderEditIdx;

        // Update the state of the component
        this._init();
    }

    /** Evaluates whether or not the chronological order of the dates attended is violated. */
    _isChronoOrderViolated(): boolean {
        // Safely handle pre-form build calls
        if (!this._form) {
            return false;
        }

        // Don't display the error if both dates have not been filled in
        if (!this._form.value.start_year || !this._form.value.end_year) {
            return false;
        }

        // The order is violated if the start date comes after the end date
        return this._form.value.start_year > this._form.value.end_year;
    }

    /** Initialize the component state. */
    _init(): void {
        // If this is a new education entry
        if (this._entryUnderEditIdx === undefined) {
            // Initialize the entry as an empty education object
            this._entry = {
                school: undefined,
                start_year: undefined,
                end_year: undefined,
                degree: undefined,
                area_of_study: undefined
            };
        }
        // If an education entry is being editted
        else {
            // Use the existing entry as the starting values
            this._entry = this.developer.education[this._entryUnderEditIdx];
        }

        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(
            ['school', 'start_year', 'end_year', 'degree', 'area_of_study', 'description']);

        // Configure the form
        this._validationMessages['school'] =
            this._formBuilder.buildValidatorMessages('school', ['required', ['maxlength', 64]]);
        this._validationMessages['start_year'] =
            this._formBuilder.buildValidatorMessages('start_year', ['required']);
        this._validationMessages['end_year'] =
            this._formBuilder.buildValidatorMessages('end_year', ['required']);
        this._validationMessages['degree'] =
            this._formBuilder.buildValidatorMessages('degree', ['required']);
        this._validationMessages['area_of_study'] =
            this._formBuilder.buildValidatorMessages('area_of_study', [['maxlength', 64]]);
        this._validationMessages['description'] =
            this._formBuilder.buildValidatorMessages('description', [['maxlength', 4096]]);

        let validationRules = new Map<string, Array<Validator>>();
        validationRules['school'] = [Validators.required, Validators.maxLength(64)];
        validationRules['start_year'] = [Validators.required, this._validateChronoOrder()];
        validationRules['end_year'] = [Validators.required, this._validateChronoOrder()];
        validationRules['degree'] = [this._degreeSuggestion.validateAccepted()];
        validationRules['area_of_study'] = [Validators.required, Validators.maxLength(64)];
        validationRules['description'] = [Validators.maxLength(4096)];

        // Build the form
        let formGroup = {
            'school': new FormControl(this._entry.school, validationRules['school']),
            'start_year': new FormControl(this._entry.start_year, validationRules['start_year']),
            'end_year': new FormControl(this._entry.end_year, validationRules['end_year']),
            'degree': new FormControl(this._entry.degree, validationRules['degree']),
            'area_of_study': new FormControl(this._entry.area_of_study, validationRules['area_of_study']),
            'description': new FormControl(this._entry.description, validationRules['description'])
        };
        this._form = this._ngFormBuilder.group(formGroup);

        // Initialize the suggestable field wrapper
        this._degreeSuggestion.ngOnInit(this._form.controls['degree'], this._degreeService.getSuggestions);
        if (this._entryUnderEditIdx) {
            this._degreeSuggestion.markAsAccepted();
        }

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** After the view has been initialized. */
    ngAfterViewInit(): void {
        // Pass the field reference to the suggestion field wrapper now that it is available
        this._degreeSuggestion.ngAfterViewInit(this._degreeField);
    }

    /** Builds a chronological order validator for the years attended range. */
    _validateChronoOrder(): ValidatorFn {
        let me = this;

        return (control: AbstractControl): {[key: string]: any} => {
            return me._isChronoOrderViolated() ? {chronoOrder: true} : null;
        }
    }

    /** Process changes in the input form. */
    _onFormChanged(): void {
        // If the date fields are filled and one of them changes, reevaluate both of them
        if (this._form.value.start_year && this._form.value.end_year) {
            if (this._form.controls['start_year'].dirty || this._form.controls['end_year'].dirty) {
                this._form.controls['start_year'].updateValueAndValidity();
                this._form.controls['start_year'].markAsDirty();
                this._form.controls['end_year'].updateValueAndValidity();
                this._form.controls['end_year'].markAsDirty();
            }
        }

        super._onFormChanged();
    }

    /** Process the form submission. */
    _onSubmit(): void {
        // Pack the form input into the entry object
        for (let field in this._form.value) {
            if (this._form.value.hasOwnProperty(field) && this._form.value[field]) {
                this._entry[field] = this._form.value[field];
            }
        }

        // If this entry was being edited, remove the original from the education array
        if (this._entryUnderEditIdx !== undefined) {
            this.developer.education.splice(this._entryUnderEditIdx, 1);
        }

        // Insert this edit in the correct chronological order
        let inserted = false;
        for (let iter = 0; iter < this.developer.education.length; iter++) {
            if (this._entry.end_year > this.developer.education[iter].end_year) {
                this.developer.education.splice(iter, 0, this._entry);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            this.developer.education.push(this._entry);
        }

        // Save the updated profile to the database
        this._developerService.update(this.developer.id, this.developer)
            .then(
                // If the update succeeds
                (developer) => {
                    // Report the edit to analytics
                    this._ga.sendEvent('education_edit', 'success');

                    // Close the modal
                    this.onClose.emit();

                    // Reset the component state
                    this._init();
                },
                // If the updated failed
                (error) => {
                    // Report the error to analytics
                    this._ga.sendEvent('education_edit', 'error');
                    
                    // Reveal the unexpected error message
                    this._unexpectedError = true;
                }
            );
    }
};
