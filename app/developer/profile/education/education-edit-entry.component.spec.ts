/**
 * @author R. Matt McCann
 * @brief Unit tests for the EducationEditEntry component
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import { FormFieldErrorsComponent } from '../../../misc/form-field-errors.component';
import {
    beforeEachFormUiTest, checkAcceptValid, checkEmptyRequirement, checkLongRequirement, checkAcceptMissing, itFA,
    checkNegativelyStyles, checkPositivelyStyles, checkRevealsErrorMessage
} from '../../../misc/testing/form-test';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';
import { DegreeService } from './degree.service';
import { EducationEditEntryComponent } from './education-edit-entry.component';


describe('EducationEditEntryComponent', () => {
    let component, fixture, mockDegreesService, mockDeveloperService, mockEducation, mockDeveloper, mockGa;

    beforeEach(async(() => {
        mockDegreesService = jasmine.createSpyObj('DegreeService', ['getSuggestions']);
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockEducation = [
            {
                area_of_study: 'Computer Science & Engineering',
                degree: 'Masters of Science',
                description: 'Coding stuff real, real good',
                end_year: 2015,
                start_year: 2012,
                school: 'OSU'
            },
            {
                area_of_study: 'Computer Science & Engineering',
                degree: 'Bachelor of Science',
                description: 'Coding stuff real good',
                end_year: 2012,
                start_year: 2007,
                school: 'OSU'
            }
        ];
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);

        TestBed.configureTestingModule({
                imports: [FormsModule, ReactiveFormsModule],
                declarations: [ EducationEditEntryComponent, FormFieldErrorsComponent],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService },
                    { provide: DegreeService, useValue: mockDegreesService },
                    { provide: GoogleAnalytics, useValue: mockGa },
                    FormBuilderService
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(EducationEditEntryComponent);
                component = fixture.componentInstance;
                component._entryUnderEditIdx = 0;
                component.developer = mockDeveloper;
                component.developer.id = 'profile_id';
                component.developer.education = mockEducation;
                component.onClose = jasmine.createSpyObj('EventEmitter', ['emit']);
            });
    }));

    describe('constructor', () => {
        const thisYear = new Date().getFullYear();

        it('builds a seventy year start date range', () => {
            for (let iter = 0; iter < 70; iter++) {
                expect(component._startYearOptions[iter]).toEqual(thisYear - iter);
            }
        });

        it('builds a seventy-seven year end date range', () => {
            for (let iter = 0; iter < 77; iter++) {
                expect(component._endYearOptions[iter]).toEqual(thisYear + 7 - iter);
            }
        });
    });

    describe('_isChronoOrderViolated', () => {
        it('does not freak out when called before form construction is complete', () => {
            expect(component._isChronoOrderViolated()).toBe(false);
        });

        itFA('is false when not both of dates have been filled', () => {
            fixture.detectChanges();

            expect(component._isChronoOrderViolated()).toBe(false);
        });

        itFA('is false when the start date is before the end date', () => {
            fixture.detectChanges();
            component._form.value.start_year = 1999;
            component._form.value.end_year = 2001;
            tick(1000);

            expect(component._isChronoOrderViolated()).toBe(false);
        });

        itFA('is true whenthe start date is after the end date', () => {
            fixture.detectChanges();
            component._form.value.start_year = 2009;
            component._form.value.end_year = 2001;
            tick(1000);

            expect(component._isChronoOrderViolated()).toBe(true);
        });
    });

    describe('_init', () => {
        it('initializes an empty object when this is a new entry edit', fakeAsync(() => {
            component._entryUnderEditIdx = undefined;

            fixture.detectChanges();

            expect(component._entry).toBeDefined();
        }));

        it('sets the entry to be worked when this is an existing entry edit', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._entry).toBe(mockEducation[0]);
        }));

        it('builds the form', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form).toBeDefined();
            expect(component._form.value.school).toBeDefined();
            expect(component._form.value.start_year).toBeDefined();
            expect(component._form.value.end_year).toBeDefined();
            expect(component._form.value.degree).toBeDefined();
            expect(component._form.value.area_of_study).toBeDefined();
            expect(component._form.value.description).toBeDefined();
        }));

        it('attaches _onFormChanged to form.valueChanges', fakeAsync(() => {
            spyOn(component, '_onFormChanged');

            fixture.detectChanges();

            component._form.controls.school.markAsDirty();
            component._form.controls.school.setValue('aasdf');

            tick(1000);

            expect(component._onFormChanged).toHaveBeenCalled();
        }));

        it('sets the form fields to the current state of the entry', fakeAsync(() => {
            fixture.detectChanges();

            expect(component._form.value.school).toEqual('OSU')
            expect(component._form.value.start_year).toEqual(2012);
            expect(component._form.value.end_year).toEqual(2015);
            expect(component._form.value.degree).toEqual('Masters of Science');
            expect(component._form.value.area_of_study).toEqual('Computer Science & Engineering')
            expect(component._form.value.description).toEqual('Coding stuff real, real good');
        }));

        itFA('initializes the degree suggestion field', () => {
            spyOn(component._degreeSuggestion, 'ngOnInit');

            fixture.detectChanges();

            expect(component._degreeSuggestion.ngOnInit).toHaveBeenCalled();
        });
    });

    describe('ngAfterViewInit', () => {
        it('post-view intializes the degree suggestion field', () => {
            spyOn(component._degreeSuggestion, 'ngAfterViewInit');

            component.ngAfterViewInit();

            expect(component._degreeSuggestion.ngAfterViewInit).toHaveBeenCalled();
        });
    });

    describe('_onFormChanged', () => {
        it('clears the unknown error message', fakeAsync(() => {
            component._unexpectedError = true;

            fixture.detectChanges();
            component._onFormChanged();

            expect(component._unexpectedError).toBe(false);
        }));

        it('accepts valid school values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'school');
        }));

        it('accepts valid start_year values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'start_year', 2012);
            tick(1000); // TODO: ?
        }));

        it('accepts valid end_year values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'end_year', 2015);
            tick(1000); // TODO: ?
        }));

        it('accepts valid area_of_study values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'area_of_study');
        }));

        it('accepts valid degree values', fakeAsync(() => {
            component._degreeSuggestion._suggestionAccepted = true;
            component._degreeSuggestion._suggestionInProgress = false;
            checkAcceptValid(fixture, component, 'degree');
        }));

        itFA('invalidates suggestion in progress degree values without error message', () => {
            component._degreeSuggestion._suggestionAccepted = false;
            component._degreeSuggestion._suggestionInProgress = true;

            fixture.detectChanges();

            component._form.controls['degree'].markAsDirty();
            component._form.controls['degree'].setValue('');

            tick(1000);

            expect(component._form.controls['degree'].valid).toBe(false);
            expect(component._formState['degree'].accepted).toBe(false);
            expect(component._formState['degree'].error).toEqual([undefined]);
        });


        it('accepts valid description values', fakeAsync(() => {
            checkAcceptValid(fixture, component, 'description');
        }));

        it('invalidates missing school values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'school', 'School');
        }));

        it('invalidates missing start_year values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'start_year', 'Start year');
        }));

        it('invalidates missing end_year values', fakeAsync(() => {
            checkEmptyRequirement(fixture, component, 'end_year', 'End year');
        }));

        it('accepts missing description values', fakeAsync(() => {
            checkAcceptMissing(fixture, component, 'description');
        }));

        it('invalidates too long school values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'school', 64, 'School');
        }));

        it('invalidates too long area_of_study values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'area_of_study', 64, 'Area of study');
        }));

        it('invalidates too long description values', fakeAsync(() => {
            checkLongRequirement(fixture, component, 'description', 4096, 'Description');
        }));
    });

    describe('_onSubmit', () => {
        let setInput = function() {
            component._form.controls.school.setValue('Michigan');
            component._form.controls.start_year.setValue(2013);
            component._form.controls.end_year.setValue(2014);
            component._form.controls.degree.setValue('Something');
            component._form.controls.area_of_study.setValue('else');
            component._form.controls.description.setValue('Woah!');
            tick(1000);
        }

        beforeEach(fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(developer);
                    }
                };
            });
        }));

        it('updates the entries fields to the form state', fakeAsync(() => {
            fixture.detectChanges();

            setInput();

            component._onSubmit();

            expect(component._entry.school).toEqual('Michigan');
            expect(component._entry.start_year).toEqual(2013);
            expect(component._entry.end_year).toEqual(2014);
            expect(component._entry.degree).toEqual('Something');
            expect(component._entry.area_of_study).toEqual('else');
            expect(component._entry.description).toEqual('Woah!');
        }));

        it('splices in existing entries', fakeAsync(() => {
            fixture.detectChanges();

            component._onSubmit();
            tick(1000); // Form.reset timer

            expect(component.developer.education[0]).toBe(component._entry);
        }));

        it('splices in new entries while maintaining chronological order re: end date', fakeAsync(() => {
            component.entryUnderEditIdx = undefined;
            fixture.detectChanges();

            setInput();

            component._onSubmit();

            expect(component.developer.education.length).toEqual(3);
            expect(component.developer.education[0].start_year).toEqual(2012);
            expect(component.developer.education[1].start_year).toEqual(2013);
            expect(component.developer.education[2].start_year).toEqual(2007);
        }));

        it('appends new entries that are the chronologically oldest re: end date', fakeAsync(() => {
            component.entryUnderEditIdx = undefined;
            fixture.detectChanges();

            setInput();
            component._form.controls.start_year.setValue(1990);
            component._form.controls.end_year.setValue(1995);
            tick(1000);

            component._onSubmit();

            expect(component.developer.education.length).toEqual(3);
            expect(component.developer.education[0].start_year).toEqual(2012);
            expect(component.developer.education[1].start_year).toEqual(2007);
            expect(component.developer.education[2].start_year).toEqual(1990);
        }));

        it('resets form state and fires on close event on success', fakeAsync(() => {
            let updatedProfile = jasmine.createSpy('UpdatedProfile');
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        then_cb(updatedProfile);
                    }
                };
            });

            fixture.detectChanges();
            spyOn(component, '_init');

            component._onSubmit();

            expect(mockDeveloperService.update).toHaveBeenCalledWith(mockDeveloper.id, mockDeveloper);
            expect(component._init).toHaveBeenCalled();
            expect(component.onClose.emit).toHaveBeenCalled();
        }));

        it('reveals the unknown error message on a failed api call', fakeAsync(() => {
            mockDeveloperService.update.and.callFake((id, developer) => {
                return {
                    then: function(then_cb, catch_cb) {
                        catch_cb('error message');
                    }
                };
            });

            fixture.detectChanges();
            component._onSubmit();

            expect(mockDeveloperService.update).toHaveBeenCalled();
            expect(component._unexpectedError).toBe(true);
        }));
    });

    describe('template-input: school field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'school');
        }));

        itFA('contents set to existing value', () => {
            expect(elem.field.value).toEqual('OSU');
        });

        itFA('positively styles field when good input is provided', () => {
            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field when bad input is provided', () => {
            component._form.controls.school.setValue('');

            checkNegativelyStyles(fixture, elem);
        });

        itFA('reveals the error message when a bad input is provided', () => {
            component._form.controls.school.setValue('');
            component._form.controls.school.markAsDirty();

            checkRevealsErrorMessage(fixture, elem);
        });
    });

    describe('template-input: start_year field', () => {
        let option;

        beforeEach(() => {
            option = fixture.nativeElement.querySelector('#start_year');
        })

        itFA('empty value field is disabled', () => {
            fixture.detectChanges();

            expect(option.children[0].disabled).toBe(true);
            expect(option.children[0].innerHTML.trim()).toEqual('');
        });

        itFA('selects the placeholder option when no start date is provided', () => {
            component.developer.education[0].start_year = undefined;

            fixture.detectChanges();

            expect(option.children[0].selected).toBe(true);
            expect(option.children[0].innerHTML.trim()).toEqual('');
        });

        itFA('selects the correct option to match the existing date date', () => {
            let date = new Date().getFullYear() - 2;
            component.developer.education[0].start_year = date;

            fixture.detectChanges();

            expect(option.children[3].selected).toBe(true);
            expect(option.children[3].innerHTML.trim()).toEqual(''+date);
        });
    });

    describe('template-input: end_year field', () => {
        let option;

        beforeEach(() => {
            option = fixture.nativeElement.querySelector('#end_year');
        })

        itFA('empty value field is disabled', () => {
            fixture.detectChanges();

            expect(option.children[0].disabled).toBe(true);
            expect(option.children[0].innerHTML.trim()).toEqual('');
        });

        itFA('selects the placeholder option when no start date is provided', () => {
            component.developer.education[0].end_year = undefined;

            fixture.detectChanges();

            expect(option.children[0].selected).toBe(true);
            expect(option.children[0].innerHTML.trim()).toEqual('');
        });

        itFA('selects the correct option to match the existing date date', () => {
            let date = new Date().getFullYear() - 2;
            component.developer.education[0].end_year = date;

            fixture.detectChanges();

            expect(option.children[10].selected).toBe(true);
            expect(option.children[10].innerHTML.trim()).toEqual(''+date);
        });
    });

    describe('template-input: degree field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'degree');
        }));

        itFA('contents set to existing value', () => {
            expect(elem.field.value).toEqual('Masters of Science');
        });

        itFA('positively styles field when good input is provided', () => {
            component._degreeSuggestion._suggestionAccepted = true;
            component._degreeSuggestion._suggestionInProgress = false;
            component._form.controls.degree.setValue('Something!');

            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field when no input is provided', () => {
            component._form.controls.degree.setValue('');

            checkNegativelyStyles(fixture, elem);
        });

        itFA('negatively styles field without error message when suggestion is in progres', () => {
            fixture.detectChanges();
            tick(1000);

            expect(elem.field.classList.contains('ng-valid')).toBe(false);
            expect(elem.field.classList.contains('ng-invalid')).toBe(true);
            expect(elem.errors.children.length).toBe(0);
        });

        itFA('reveals the error message when a bad input is provided', () => {
            component._form.controls.degree.setValue('');
            component._form.controls.degree.markAsDirty();

            checkRevealsErrorMessage(fixture, elem);
        });
    });

    describe('template-input: area_of_study field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'area_of_study');
        }));

        itFA('contents set to existing value', () => {
            expect(elem.field.value).toEqual('Computer Science & Engineering');
        });

        itFA('positively styles field when good input is provided', () => {
            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field when bad input is provided', () => {
            component._form.controls.area_of_study.setValue(new Array(68).join('A'));

            checkNegativelyStyles(fixture, elem);
        });

        itFA('reveals the error message when a bad input is provided', () => {
            component._form.controls.area_of_study.setValue(new Array(68).join('A'));
            component._form.controls.area_of_study.markAsDirty();

            checkRevealsErrorMessage(fixture, elem);
        });
    });

    describe('template-input: description field', () => {
        let elem;

        beforeEach(fakeAsync(() => {
            elem = beforeEachFormUiTest(fixture, 'description');
        }));

        itFA('contents set to existing value', () => {
            expect(elem.field.value).toEqual('Coding stuff real, real good');
        });

        itFA('positively styles field when good input is provided', () => {
            checkPositivelyStyles(fixture, elem);
        });

        itFA('negatively styles field when bad input is provided', () => {
            component._form.controls.description.setValue(new Array(4098).join('A'));

            checkNegativelyStyles(fixture, elem);
        });

        itFA('reveals the error message when a bad input is provided', () => {
            component._form.controls.description.setValue(new Array(4098).join('A'));
            component._form.controls.description.markAsDirty();

            checkRevealsErrorMessage(fixture, elem);
        });
    });

    describe('template: unexpected error message', () => {
        itFA('revealed when unexpected error set', () => {
            component._unexpectedError = true;
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).not.toBe(null);
        });

        itFA('hidden when unexpected error not set', () => {
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#unexpectedError')).toBe(null);
        });
    });

    describe('template-input: cancel button', () => {
        itFA('calls _onCancel when clicked', () => {
            fixture.detectChanges();
            spyOn(component, '_onCancel');

            fixture.nativeElement.querySelector('#cancelButton').click();

            expect(component._onCancel).toHaveBeenCalled();
        });
    });

    describe('template-input: submit button', () => {
        let button;

        beforeEach(fakeAsync(() => {
            fixture.detectChanges();
            tick(1000);

            button = fixture.nativeElement.querySelector('#saveButton');
        }));

        itFA('is enabled when the form is valid', () => {
            component._degreeSuggestion._suggestionAccepted = true;
            component._degreeSuggestion._suggestionInProgress = false;
            fixture.detectChanges();
            component._form.controls.degree.setValue('Something!');
            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(false);
        });

        itFA('is disabled when the form is invalid', () => {
            component._form.controls.school.setValue('');
            component._form.controls.school.markAsDirty();
            fixture.detectChanges();
            tick(1000);
            fixture.detectChanges();

            expect(button.disabled).toBe(true);
        });

        itFA('fires onSubmit() when clicked', () => {
            spyOn(component, '_onSubmit');
            component._degreeSuggestion._suggestionAccepted = true;
            component._degreeSuggestion._suggestionInProgress = false;
            fixture.detectChanges();
            component._form.controls.degree.setValue('Something!');
            tick(1000);
            fixture.detectChanges();

            button.click();

            expect(component._onSubmit).toHaveBeenCalled();
        });
    });

    describe('template-view: chrono order violated label', () => {
        it('hidden when order is not violated', fakeAsync(() => {
            spyOn(component, '_isChronoOrderViolated').and.returnValue(false);

            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#chronoOrderViolatedLabel')).toBe(null);
        }));

        it('revealed when order is violated', fakeAsync(() => {
            spyOn(component, '_isChronoOrderViolated').and.returnValue(true);

            fixture.detectChanges();

            expect(fixture.nativeElement.querySelector('#chronoOrderViolatedLabel').children.length).toBe(1);
        }))
    });
});
