/**
 * @author R. Matt McCann
 * @brief Unit tests for the degree service
 * @copyright &copy; 2017 R. Matt McCann
 */

import { TestBed, async, inject } from '@angular/core/testing';
import { HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { DegreeService } from './degree.service';


describe('DegreeService', () => {
    let degreeService, mockBackend;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                DegreeService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });
    });

    beforeEach(inject([DegreeService, XHRBackend], (_degreeService, _mockBackend) => {
        degreeService = _degreeService;
        mockBackend = _mockBackend;
    }));

    describe('constructor', () => {
        it('should populate getSuggestions', () => {
            expect(degreeService.getSuggestions).toBeDefined();
        });
    });
});
