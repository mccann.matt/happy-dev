/**
 * @author R. Matt McCann
 * @brief Summary details for the developer's education
 * @copyright &copy; 2016 R. Matt McCann
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';

import { ListDetailsComponent } from '../misc/list-details.component';
import { DeveloperService } from '../../developer.service';
import { EducationDetails } from './education-details.model';

@Component({
    selector: 'education-details',
    templateUrl: 'education-details.component.html',
    styleUrls: ['education-details.component.scss']
})
export class EducationDetailsComponent extends ListDetailsComponent<EducationDetails> {
    constructor(developerService: DeveloperService) {
        super(developerService, 'education');
    }
}
