/**
 * @author R. Matt McCann
 * @brief Provides the developer with an interface to upload and edit a profile picture
 * @copyright &copy; 2017 R. Matt McCann
 */

import {
    Component, DoCheck, Input, EventEmitter,  Output, ViewChild, trigger,
    state,  style,  animate,  transition
} from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { Developer } from '../../developer.model';
import { DeveloperService} from '../../developer.service';
import { DataManipService } from '../../../misc/data-manip.service';


/** Provides the developer with an interface to upload and edit a profile picture. */
@Component({
    selector: 'portrait-edit',
    templateUrl: 'portrait-edit.component.html',
    styleUrls: ['portrait-edit.component.scss'],
    animations: [
        trigger('enterExpand', [
            state('void', style({
                transform: 'scaleX(0)',
                width:0
            })),
            state('*',   style({
                transform: 'scaleX(1)',
                width:'*'
            })),
            transition('void<=>*', animate('300ms'))
        ]),
        trigger('enterFadeIn', [
            state('void', style({
                opacity:0
            })),
            state('*',   style({
                opacity:1
            })),
            transition('void<=>*', animate('200ms 300ms')),

        ])
    ]
})
export class PortraitEditComponent extends ModalFormComponent implements DoCheck {
    /** Component that let's the user crop the image. */
    @ViewChild('cropper') _cropper: ImageCropperComponent;

    /** Cropped image data output by the cropper. */
    _cropperData = { image: '' };

    /** Configuration settings for the cropper. */
    _cropperSettings: CropperSettings;

    /** Developer object being editted. */
    @Input() developer: Developer;

    /** Marked as true when the onClose event is fired. This is used to avoid duplicate fires. */
    _hasEmittedComplete = false;

    /** Marked true when the file drop zone is activated. */
    _hasDropZoneOver = false;

    /** Marked true when the portrait is being displayed. */
    _isPortraitDisplayed = false;

    /** Marked true to reveal the upload progress indicator. */
    _showUploadProgress = false;

    /** Service that uploads the cropped portrait image to the backend. */
    _uploader: FileUploader;

    constructor(
        private _dataManipService: DataManipService,
        private _developerService: DeveloperService,
        private _ga: GoogleAnalytics
    ) {
        super();

        // Configure the image cropper
        this._cropperSettings = new CropperSettings();

        // Hide the cropper's built-in file selector
        this._cropperSettings.noFileInput = true;
    }

    /** Initialize the component state. */
    _init(): void {
        // Configure the upload url for the portrait file
        this._uploader = new FileUploader({
            url: process.env.API_SERVER + '/developer/' + this.developer.id + '/portrait/'
        });

        // Hide the image cropper
        this._isPortraitDisplayed = false;

        // Reset the upload progress indicator
        this._showUploadProgress = false;
        this._hasEmittedComplete = false;
    }

    /** Check ran whenever the component state changes. */
    ngDoCheck(): void {
        // If the upload has completed and the onClose event hasn't fired yet
        if (this._showUploadProgress && this._uploader.progress === 100 && !this._hasEmittedComplete) {
            // Mark the emit as fired
            this._hasEmittedComplete = true;

            // The actual emit needs to take place outside of the flow of change detection. The developer object
            // needs to be fetched because the portrait url is dynamically calculated by the backend
            this._developerService.read(this.developer.id)
                .then(
                    // If the read succeeds
                    (developer) => {
                        // Report the edit to analytics
                        this._ga.sendEvent('portrait_edit', 'success');

                        // Fire the onClose event with the updated developer object
                        this.onClose.emit(developer);

                        // Reset the state of the component
                        this._init();
                    },
                    // If the read fails
                    (error) => {
                        // Report the error to analytics
                        this._ga.sendEvent('portrait_edit', 'error');

                        // Reveal the unexpected error message
                        this._unexpectedError = true;
                    }
                );
        }
    }

    /** Loads the selected portrait photo and provides the image data to the cropper. */
    _renderSelectedPhoto(): void {
        // Grab the file object from the uploader component
        let file = this._uploader.queue[0]._file;
        let fileReader = new FileReader();
        let me = this;

        // Load the image data
        fileReader.onloadend = function(result) {
            let image = new Image();

            // Set the image contents of the cropper
            image.src = fileReader.result;
            me._cropper.setImage(image);

            // Reveal the cropper
            me._isPortraitDisplayed = true;
        };
        fileReader.readAsDataURL(file);
    }

    /** Resets the portrait photo input so the user can provide a new image file. */
    _resetPhotoInput(): void {
        // Clear the uploader queue
        this._uploader.queue = [];

        // Hide the image cropper
        this._isPortraitDisplayed = false;
    }

    /** Uploads the cropped portrait image to the backend. */
    _uploadPhoto(): void {
        // Rename the file to a default thumb value
        let fileName = this._uploader.queue[0].file.name;
        let fileExt = fileName.substr(fileName.lastIndexOf('.'));

        // Drop the original file from the upload queue
        this._uploader.queue = [];

        // Pack up the selected thumbnail and add it to the upload queue
        let blob = this._dataManipService.dataUriToBlob(this._cropperData.image);
        let thumb = new File([blob], 'thumb' + fileExt);
        this._uploader.addToQueue([thumb]);

        // Upload the files
        this._showUploadProgress = true;
        this._uploader.uploadAll();
    }
}
