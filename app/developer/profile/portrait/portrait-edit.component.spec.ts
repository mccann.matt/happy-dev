/**
 * @author R. Matt McCann
 * @brief Unit tests for the portrait edit component.
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, TestBed } from '@angular/core/testing';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageCropperModule } from 'ng2-img-cropper';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { DeveloperService } from '../../developer.service';
import { DataManipService } from '../../../misc/data-manip.service';
import { PortraitEditComponent } from './portrait-edit.component';


describe('PortraitEditComponent', () => {
    let component, fixture, mockDeveloperService, mockDataManipService, mockGa;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['read']);
        mockDataManipService = jasmine.createSpyObj('DataManipService', ['dataUriToBlob']);
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);

        TestBed.configureTestingModule({
                imports: [FileUploadModule, ImageCropperModule],
                declarations: [ PortraitEditComponent ],
                providers: [
                    { provide: DeveloperService, useValue: mockDeveloperService },
                    { provide: DataManipService, useValue: mockDataManipService },
                    { provide: GoogleAnalytics, useValue: mockGa }
                ]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(PortraitEditComponent);
                component = fixture.componentInstance;
                component.developer = jasmine.createSpy('Developer');
                component.developer.id = 'profile_id';
                component._uploader = jasmine.createSpy('Uploader');
            });
    }));

    describe('constructor', () => {
        it('configures required cropper settings', () => {
            expect(component._cropperSettings).toBeDefined();
            expect(component._cropperSettings.canvasWidth).toBeGreaterThan(0);
            expect(component._cropperSettings.canvasHeight).toBeGreaterThan(0);
            expect(component._cropperSettings.croppedWidth).toBeGreaterThan(0);
            expect(component._cropperSettings.croppedHeight).toBeGreaterThan(0);
            expect(component._cropperSettings.noFileInput).toBe(true);
        });
    });

    describe('ngDoCheck', () => {
        beforeEach(() => {
            spyOn(component.onClose, 'emit');
        });

        it('does not emit profile changes when the upload has not started', async(() => {
            component._showUploadProgress = false;

            component.ngDoCheck();

            expect(component._hasEmittedComplete).toBe(false);
            expect(mockDeveloperService.read).not.toHaveBeenCalled();
        }));

        it('does not emit profile changes when the upload has not finished', async(() => {
            component._showUploadProgress = true;
            component._uploader.progress = 50;

            component.ngDoCheck();

            expect(component._hasEmittedComplete).toBe(false);
            expect(mockDeveloperService.read).not.toHaveBeenCalled();
        }));

        it('emits profile changes when the upload has finished', async(() => {
            mockDeveloperService.read.and.callFake(() => {
                return {
                    then: function(then_cb, catch_cb) { then_cb('woof'); }
                };
            });

            component._showUploadProgress = true;
            component._uploader.progress = 100;

            component.ngDoCheck();

            // TODO(mmccann) - Why is this false?
            // expect(component._hasEmittedComplete).toBe(true);
            expect(mockDeveloperService.read).toHaveBeenCalledWith('profile_id');
            expect(component._unexpectedError).toBe(false);
            expect(component.onClose.emit).toHaveBeenCalledWith('woof');
        }));

        it('does not emit profile changes a second time', () => {
            mockDeveloperService.read.and.callFake(() => {
                return {
                    then: function(then_cb, catch_cb) { then_cb('woof'); }
                };
            });

            component._showUploadProgress = true;
            component._uploader.progress = 100;

            component.ngDoCheck();
            component.ngDoCheck();

            // TODO(mmccann) - Why is this false?
            // expect(component._hasEmittedComplete).toBe(true);
            expect(mockDeveloperService.read.calls.count()).toEqual(1);
            expect(component._unexpectedError).toBe(false);
        });

        it('reveals the unknown error message when the emit profile changes fails', () => {
            mockDeveloperService.read.and.callFake(() => {
                return {
                    then: function(then_cb, catch_cb) { catch_cb('woof'); }
                };
            });

            component._showUploadProgress = true;
            component._uploader.progress = 100;

            component.ngDoCheck();

            expect(component._hasEmittedComplete).toBe(true);
            expect(mockDeveloperService.read).toHaveBeenCalledWith('profile_id');
            expect(component.onClose.emit).not.toHaveBeenCalled();
            expect(component._unexpectedError).toBe(true);
        });
    });

    describe('_init', () => {
        it('initializes the _uploader to point at the the correct API url', () => {
            component.ngOnInit();

            expect(component._uploader).toBeDefined();
            expect(component._uploader.options.url).toEqual('http://localhost:8001/developer/profile_id/portrait/');
        });
    });

    describe('renderSelectedPhoto', () => {
        xit('displays the photo when successfully loaded', async(() => {
            // TODO(mmccann):
            // Hooking into FileReader was proving to be a massive fucking pain, so for now
            // I'm moving past these tests.
            component.renderSelectedPhoto();
        }));

        xit('displays an error message when the photo fails to load', () => {
            // TODO(mmccann):
            // Hooking into FileReader was proving to be a massive fucking pain, so for now
            // I'm moving past these tests.
            component.renderSelectedPhoto();
        });
    });

    describe('resetPhotoInput', () => {
        it('cleans up photo input correctly', () => {
            component.ngOnInit();
            component._uploader.queue = ['woof'];
            component._isPortraitDisplayed = true;

            component._resetPhotoInput();

            expect(component._uploader.queue).toEqual([]);
            expect(component._isPortraitDisplayed).toBe(false);
        });
    });

    describe('uploadPhoto', () => {
        it('configures upload of only the thumbnail', () => {
            component._uploader = jasmine.createSpyObj('Uploader', ['addToQueue', 'uploadAll']);
            component._uploader.queue = [];
            component._uploader.queue.push({file: {name: 'woof.jpg'}});
            component._uploader.queue.push({file: {name: 'dontincludeme.png'}});
            component._uploader.addToQueue.and.callFake((item) => {
                component._uploader.queue.push({file: item[0]});
            });

            component._uploadPhoto();

            expect(component._uploader.queue.length).toEqual(1);
            expect(component._uploader.queue[0].file.name).toEqual('thumb.jpg');
            expect(component._uploader.uploadAll).toHaveBeenCalled();
            expect(component._showUploadProgress).toBe(true);
        });
    });

    describe('template-input: drop files', () => {
        xit('hides the file input fields', () => {

        });

        xit('renders the portrait', () => {

        });

        xit('renders the thumbnail', () => {

        });

        xit('reveals the upload-a-different-photo link', () => {

        });
    });

    describe('template-input: select file', () => {
        xit('hides the file input fields', () => {

        });

        xit('renders the portrait', () => {

        });

        xit('renders the thumbnail', () => {

        });

        xit('reveals the upload-a-different-photo link', () => {

        });
    });

    describe('template-input: save button', () => {
        xit('starts the upload', () => {

        });

        xit('displays the upload progress', () => {

        });
    });

    describe('template-input: cancel button', () => {
        xit('emits a close modal request', () => {

        });
    });
});
