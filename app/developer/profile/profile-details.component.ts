/**
 * @author R. Matt McCann
 * @brief Profile for the developer
 * @copyright &copy; 2016 R. Matt McCann
 */

import 'rxjs/add/operator/switchMap';

import { Component, EventEmitter, ElementRef, Input, Output, ViewChild, HostListener, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Observable } from 'rxjs/Observable';

import { Account } from '../../account/account.model';
import { ModalFormComponent } from '../../misc/modal-form.component';
import { Developer, SectionType } from '../developer.model';
import { DeveloperService} from '../developer.service';
import { OverviewEditComponent } from './basics/overview-edit.component';


/** Provides the developer with an interface to view/edit their profile */
@Component({
    selector: 'profile-details',
    templateUrl: 'profile-details.component.html',
    styleUrls: ['profile-details.component.scss']
})
export class ProfileDetailsComponent {
    /** Account object being editted. */
    @Input() account: Account;

    /** Developer object being editted. */
    @Input() developer: Developer;

    /** Component that edits the basic profile info. */
    @ViewChild('editBasics') _editBasicsComp: ModalFormComponent;

    /** Modal holding the edit basics view. */
    @ViewChild('editBasicsModal') _editBasicsModal: ModalComponent;

    /** Component that edits the education entry info. */
    @ViewChild('editEducation') _editEducationComp: ModalFormComponent;

    /** Education entry currently being editted. */
    _editEducationIdx: number;

    /** Modal holding the edit education view. */
    @ViewChild('editEducationModal') _editEducationModal: ModalComponent;

    /** Component that edits the overview field. */
    @ViewChild('editOverview') _editOverviewComp: ModalFormComponent;

    /** Modal holding the edit overview view. */
    @ViewChild('editOverviewModal') _editOverviewModal: ModalComponent;

    /** Component that edits the profile portrait. */
    @ViewChild('editPortrait') _editPortraitComp: ModalFormComponent;

    /** Modal holding the edit portrait view. */
    @ViewChild('editPortraitModal') _editPortraitModal: ModalComponent;

    /** Component that edits the skill tags. */
    @ViewChild('editSkills') _editSkillsComp: ModalFormComponent;

    /** Modal holding the edit skills view. */
    @ViewChild('editSkillsModal') _editSkillsModal: ModalComponent;

    /** Component that edits the work history info. */
    @ViewChild('editWorkHistory') _editWorkHistoryComp: ModalFormComponent;

    /** Work history entry currently being editted. */
    _editWorkHistoryIdx: number;

    /** Modal holding the edit work history view. */
    @ViewChild('editWorkHistoryModal') _editWorkHistoryModal: ModalComponent;

    @Output() onDeveloperUpdate = new EventEmitter<Developer>();

    _onDismissEditBasicsModal(): void {
        this._editBasicsComp.dismiss();
    }

    _onDismissEditEducationModal(): void {
        this._editEducationComp.dismiss();
    }

    _onDismissEditOverviewModal(): void {
        this._editOverviewComp.dismiss();
    }

    _onDismissEditPortraitModal(): void {
        this._editPortraitComp.dismiss();
    }

    _onDismissEditSkillsModal(): void {
        this._editSkillsComp.dismiss();
    }

    _onDismissEditWorkHistoryModal(): void {
        this._editWorkHistoryComp.dismiss();
    }

    _onEditPortraitClose(developer?: Developer) {
        if (developer) {
            this.onDeveloperUpdate.emit(developer);
        }

        this._editPortraitModal.close();
    }

    _onOpenEducation(editEducationIdx: number): void {
        this._editEducationIdx = editEducationIdx;
        this._editEducationModal.open();
    }

    _onOpenWorkHistory(editWorkHistoryIdx: number): void {
        this._editWorkHistoryIdx = editWorkHistoryIdx;
        this._editWorkHistoryModal.open();
    }

    openModal(section: SectionType): void {
        if (section === SectionType.PORTRAIT) {
            this._editPortraitModal.open();
        } else if (section === SectionType.BASIC_INFO) {
            this._editBasicsModal.open();
        } else if (section === SectionType.SKILL_TAGS) {
            this._editSkillsModal.open();
        } else if (section === SectionType.OVERVIEW) {
            this._editOverviewModal.open();
        } else if (section === SectionType.EDUCATION) {
            this._editEducationModal.open();
        } else if (section === SectionType.WORK_HISTORY) {
            this._editWorkHistoryModal.open();
        }
    }

    public leftNavIsFixed: boolean = false;

    constructor() { }

    //ngOnInit() { }

    /*@HostListener("window:scroll", [])
        onWindowScroll() {
            let number = this.document.body.scrollTop;
                if (number > 100) {
            this.navIsFixed = true;
            } else if (this.navIsFixed && number < 10) {
                this.navIsFixed = false;
        }
    }*/
};
