/**
 * @author R. Matt McCann
 * @brief Dashboard for developer accounts that allows them to edit profile, happiness questions, etc
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { Account } from '../../account/account.model';
import { AccountService } from '../../account/account.service';
import { GoogleAnalytics } from '../../analytics/google/google-analytics';
import { Developer, SectionType } from '../developer.model';
import { DeveloperService} from '../developer.service';
import { ProfileDetailsComponent } from '../profile/profile-details.component';


/** Dashboard for developer accounts that allows them to edit profile, happiness questions, etc. */
@Component({
    selector: 'developer-dashboard',
    templateUrl: 'developer-dashboard.component.html',
    styleUrls: ['developer-dashboard.component.scss']
})
export class DeveloperDashboardComponent implements OnInit {
    /** Account object being editted. */
    _account: Account;

    /** Developer object being editted. */
    _developer: Developer;

    /** The profile details component that owns the edit modals. */
    @ViewChild('profileDetails') _profileDetails: ProfileDetailsComponent;

    constructor(
        private _accountService: AccountService,
        private _developerService: DeveloperService,
        private _ga: GoogleAnalytics,
        private _route: ActivatedRoute,
        private _router: Router
    ) { }

    /** Initialize the component state. */
    ngOnInit(): void {
        console.error("Initialized...");

        this._route.params
            .switchMap((params: Params) => {
                return new Promise<[Developer, Account]>((resolve, reject) => {
                    console.error("Fetching the developer record...");

                    // Fetch the developer record
                    this._developerService.read(params['id'])
                        .then(
                            // If the read succeeds
                            (developer: Developer) => {
                                console.error("Fetching the account record...");

                                // Fetch the account record
                                this._accountService.read(params['id'])
                                    .then(
                                        // If the read succeeds
                                        (account: Account) => { resolve([developer, account]); },
                                        // If the read fails
                                        (error) => { reject(error); }
                                    );
                            },
                            // If the read fails
                            (error) => { reject(error); }
                        );
                });
            })
            .subscribe(
                // If the read succeeds
                ([developer, account]) => {
                    console.error("Read succeeded!");
                    this._account = account;
                    this._developer = developer;
                },
                // If the read fails
                (error) => {
                    console.error("404");

                    // Display the 404 error page
                    this._router.navigate(['/404'], {skipLocationChange: true});
                }
            );
    }

    _onClickUpNext(clickedSection: SectionType) {
        // Report the up next click to analytics
        this._ga.sendEvent('developer_dashboard', 'up_next_click');

        this._profileDetails.openModal(clickedSection);
    }

    _onDeveloperUpdate(developer: Developer) {
        this._developer = developer;
    }
};
