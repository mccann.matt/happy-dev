/**
 * @author R. Matt McCann
 * @brief Unit tests for the DeveloperDashboard component
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, tick, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountService } from '../../account/account.service';
import { GoogleAnalytics } from '../../analytics/google/google-analytics';
import { itFA } from '../../misc/testing/form-test';
import { SectionType } from '../developer.model';
import { DeveloperService } from '../developer.service';
import { DeveloperDashboardComponent } from './developer-dashboard.component';


describe('DeveloperDashboardComponent', () => {
    let component, fixture, mockAccountService, mockDeveloperService, mockGa, mockRouter;

    beforeEach(async(() => {
        mockAccountService = jasmine.createSpyObj('AccountService', ['read']);
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['read']);
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
            declarations: [ DeveloperDashboardComponent ],
            providers: [
                { provide: AccountService, useValue: mockAccountService },
                { provide: DeveloperService, useValue: mockDeveloperService },
                { provide: GoogleAnalytics, useValue: mockGa },
                { provide: Router, useValue: mockRouter },
                { provide: ActivatedRoute, useValue: { params: Observable.of({id: 'profile_id'})} }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(DeveloperDashboardComponent);
            component = fixture.componentInstance;
            component._profileDetails = jasmine.createSpyObj('ProfileDetails', ['openModal']);
        });
    }));

    describe('ngOnInit', () => {
        let mockDeveloper = jasmine.createSpy('Developer');

        beforeEach(() => {
            mockDeveloperService.read.and.returnValue({
                then: (then_cb, catch_cb) => { }
            });
        });

        itFA('attempts to read the developer record', () => {
            fixture.detectChanges();
            tick();

            expect(mockDeveloperService.read).toHaveBeenCalledWith('profile_id');
        });

        describe('after successful developer read', () => {
            beforeEach(() => {
                mockDeveloperService.read.and.returnValue({
                    then: (then_cb, catch_cb) => { then_cb(mockDeveloper); }
                });

                mockAccountService.read.and.returnValue({
                    then: (then_cb, catch_cb) => { }
                });
            });

            itFA('attempts to read the account record', () => {
                fixture.detectChanges();
                tick();

                expect(mockAccountService.read).toHaveBeenCalledWith('profile_id');
            });

            describe('after successful account read', () => {
                let mockAccount = jasmine.createSpy('Account');

                beforeEach(() => {
                    mockAccountService.read.and.returnValue({
                        then: (then_cb, catch_cb) => { then_cb(mockAccount); }
                    });
                });

                itFA('sets the local account and developer variables', () => {
                    fixture.detectChanges();
                    tick();

                    expect(component._account).toBe(mockAccount);
                    expect(component._developer).toBe(mockDeveloper);
                });
            });

            describe('after failed account read', () => {
                beforeEach(() => {
                    mockAccountService.read.and.returnValue({
                        then: (then_cb, catch_cb) => { catch_cb('error') }
                    });
                });

                itFA('displays the 404 error', () => {
                    fixture.detectChanges();
                    tick();

                    expect(mockRouter.navigate).toHaveBeenCalledWith(['/404'], {skipLocationChange: true});
                });
            });
        });

        describe('after failed developer read', () => {
            beforeEach(() => {
                mockDeveloperService.read.and.returnValue({
                    then: (then_cb, catch_cb) => { catch_cb('error'); }
                });
            });

            itFA('displays the 404 error', () => {
                fixture.detectChanges();
                tick();

                expect(mockRouter.navigate).toHaveBeenCalledWith(['/404'], {skipLocationChange: true});
            });
        });
    });

    describe('_onClickUpNext', () => {
        it('opens the clicked section modal', () => {
            component._onClickUpNext(SectionType.BASIC_INFO);

            expect(component._profileDetails.openModal).toHaveBeenCalledWith(SectionType.BASIC_INFO);
        });
    });

    describe('_onDeveloperUpdate', () => {
        it('updates the local developer instance', () => {
            let newDeveloper = jasmine.createSpy('Developer');

            component._onDeveloperUpdate(newDeveloper);

            expect(component._developer).toBe(newDeveloper);
        });
    });
});
