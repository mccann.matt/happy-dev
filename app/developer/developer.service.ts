/**
 * @author R. Matt McCann
 * @brief CRUD service for the developer model
 * @copyright &copy; 2016 R. Matt McCann
 */

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { CrudService } from '../misc/crud.service';
import { Developer } from './developer.model';

@Injectable()
export class DeveloperService extends CrudService<Developer> {
    constructor(http: Http) {
        super(http, '/developer/');
    }
}
