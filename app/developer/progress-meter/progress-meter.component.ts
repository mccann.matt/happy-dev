/**
 * @author R. Matt McCann
 * @brief Provides a visual indicator of developer profile completion progress
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { Developer, SectionType } from '../developer.model';

/** State of an individual section of the developer profile. */
interface Section {
    isComplete: () => boolean;
    text: string;
    type: SectionType;
    value: number;
}

/** Provides a visual indicator of developer profile completion progress. */
@Component({
    selector: 'progress-meter',
    templateUrl: 'progress-meter.component.html',
    styleUrls: ['progress-meter.component.scss']
})
export class ProgressMeterComponent {
    /** Developer object whose profile progress is being evaluated. */
    @Input() developer: Developer;

    gaugeStyles(): any {
        return {
            'stroke-dasharray': (Math.PI * (this.radius * 2)) + 'px',
            'stroke-dashoffset': this._getCompletion() + 'px'
        };
    };

    /** Event emitter triggered when the user clicks on the up next text. */
    @Output() onClickUpNext = new EventEmitter<SectionType>();

    /** Code for svg circle interpolation */
    radius:number = 80;
    circumference:number = Math.PI * (this.radius * 2);

    _getCompletion():number {
        return ((100 - this._getProgressPercentage()) / 100 * this.circumference);
    }

    constructor(private _router: Router) { }

    /** Builds the list of profile sections. */
    _buildSections(): Array<Section> {
        let me = this;

        return [
            {
                text: 'BASIC INFO',
                type: SectionType.BASIC_INFO,
                value: 10,
                isComplete: (): boolean => { return (me.developer.title !== undefined); }
            },
            {
                text: 'PORTRAIT',
                type: SectionType.PORTRAIT,
                value: 10,
                isComplete: (): boolean => { return (this.developer.portrait_thumbnail !== undefined); }
            },
            {
                text: 'SKILLS',
                type: SectionType.SKILL_TAGS,
                value: 10,
                isComplete: (): boolean => { return (this.developer.skills.length > 0); }
            },
            {
                text: 'OVERVIEW',
                type: SectionType.OVERVIEW,
                value: 10,
                isComplete: (): boolean => { return (this.developer.overview !== undefined); }
            },
            {
                text: 'EDUCATION',
                type: SectionType.EDUCATION,
                value: 20,
                isComplete: (): boolean => { return (this.developer.education.length > 0); }
            },
            {
                text: 'WORK',
                type: SectionType.WORK_HISTORY,
                value: 20,
                isComplete: (): boolean => { return (this.developer.work_history.length > 0); }
            },
            {
                text: 'HAPPYMATCH',
                type: SectionType.HAPPINESS_QUESTIONS,
                value: 20,
                isComplete: (): boolean => {
                    for (let question of this.developer.questions) {
                        if (question.importance === undefined) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        ];
    }

    /** Returns the percent completed for the developer profile. */
    _getProgressPercentage(): number {
        let progressPercentage = 0;
        let sections = this._buildSections();

        for (let section of sections) {
            if (section.isComplete()) {
                progressPercentage += section.value;
            }
        }

        return progressPercentage;
    }

    /** Returns the "what to do next" message. */
    _getUpNextMessage(): string {
        let sections = this._buildSections();

        for (let section of sections) {
            if (!section.isComplete()) {
                return section.text;
            }
        }
    }

    /** Handles the user clicking on the up next text. */
    _onClickUpNext(): void {
        let sections = this._buildSections();

        for (let section of sections) {
            if (!section.isComplete()) {
                if (section.type === SectionType.HAPPINESS_QUESTIONS) {
                    this._router.navigate(['developer', this.developer.id, 'questions']);
                } else {
                    this.onClickUpNext.emit(section.type);
                    break;
                }
            }
        }
    }




};
