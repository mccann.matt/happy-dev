/**
 * @author R. Matt McCann
 * @brief Defines the Angular module for the developer account functionality
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import { DeveloperDashboardComponent } from './dashboard/developer-dashboard.component';
import { DeveloperService } from './developer.service';
import { DeveloperRoutingModule } from './developer-routing.module';
import { IRNotifierComponent } from './interview-requests/IR-notifier.component';
import { ProfileModule } from './profile/profile.module';
import { ProgressMeterComponent } from './progress-meter/progress-meter.component';
import { QuestionModule } from './question/question.module';

export let MODULE_DEF = {
    imports: [
        CommonModule,
        DeveloperRoutingModule,
        FormsModule,
        Ng2Bs3ModalModule,
        ProfileModule,
        QuestionModule,
        ReactiveFormsModule
    ],
    exports: [ ],
    declarations: [ DeveloperDashboardComponent, IRNotifierComponent, ProgressMeterComponent ],
    providers: [ DeveloperService ]
};

@NgModule(MODULE_DEF)
export class DeveloperModule { }
