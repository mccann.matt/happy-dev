/**
 * @author R. Matt McCann
 * @brief Provides a visual indicator of interview requests waiting for the developer
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component } from '@angular/core';


/** Provides a visual indicator of interview requests waiting for the developer. */
@Component({
    selector: 'ir-notifier',
    templateUrl: './IR-notifier.component.html',
    styleUrls: ['./IR-notifier.component.scss']
})
export class IRNotifierComponent {
    _getNewInterviewRequestCount(): number {
        return 0;
    }
};
