/**
 * @author R. Matt McCann
 * @brief Routing module for the developer module
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeveloperDashboardComponent } from './dashboard/developer-dashboard.component';
import { QuestionDetailsComponent } from './question/question-details/question-details.component';

const routes: Routes = [
    { path: ':id/dashboard', component: DeveloperDashboardComponent },
    { path: ':id/questions', component: QuestionDetailsComponent }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class DeveloperRoutingModule { }
