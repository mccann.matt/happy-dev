/**
 * @authors R. Matt McCann
 * @brief Unit tests for the QuestionEditComponent
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, tick, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import { FormFieldErrorsComponent } from '../../../misc/form-field-errors.component';
import { NewLinesToBrPipe } from '../../../misc/new-lines-to-br.pipe';
import { itFA } from '../../../misc/testing/form-test';
import { DeveloperService } from '../../developer.service';
import { QuestionViewEditComponent } from './question-view-edit.component';


describe('QuestionEditComponent', () => {
    let component, fixture, mockDeveloper, mockDeveloperService, mockGa;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['update']);
        mockDeveloper = jasmine.createSpy('Developer');
        mockDeveloper.questions = [];
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);

        TestBed.configureTestingModule({
            imports: [ FormsModule, ReactiveFormsModule ],
            declarations: [ NewLinesToBrPipe, QuestionViewEditComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: DeveloperService, useValue: mockDeveloperService },
                { provide: GoogleAnalytics, useValue: mockGa },
                FormBuilderService
            ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(QuestionViewEditComponent);
            component = fixture.componentInstance;
            component.developer = mockDeveloper;
        });
    }));

    describe('_formatSalary', () => {
        it('formats empty string properly', () => {
            expect(component._formatSalary('')).toEqual('');
        });

        it('formats short number', () => {
            expect(component._formatSalary('23')).toEqual('$23');
        });

        it('formats long number without $ prefix', () => {
            expect(component._formatSalary('1231231231')).toEqual('$1,231,231,231');
        });

        it('formats number with $ prefix', () => {
            expect(component._formatSalary('$1235')).toEqual('$1,235');
        });

        it('formats number with commas already', () => {
            expect(component._formatSalary('$123,456,678')).toEqual('$123,456,678');
        });

        it('adds missing comma', () => {
            expect(component._formatSalary('$123,123456,678')).toEqual('$123,123,456,678');
        });
    });
});
