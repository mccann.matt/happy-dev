/**
 * @author R. Matt McCann
 * @brief Provides the developer with an interface to answer questions about what makes them happy
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validator, Validators } from '@angular/forms';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { FormBuilderService } from '../../../misc/form-builder.service';
import { ModalFormComponent } from '../../../misc/modal-form.component';
import { Developer } from '../../developer.model';
import { DeveloperService } from '../../developer.service';
import { Importance, Question } from '../question.model';


/** Provides the developer with an interface to answer questions about what makes them happy. */
@Component({
    selector: 'question-view-edit',
    templateUrl: './question-view-edit.component.html',
    styleUrls: ['question-view-edit.component.scss']
})
export class QuestionViewEditComponent extends ModalFormComponent {
    /** Developer object being editted. */
    @Input() developer: Developer;

    _importanceHover: Importance;

    /** Whether or not this component is editting the question answer. */
    @Input() isEditMode = false;

    /** Short-hand for the question to make writing the template less verbose. */
    _question: Question;

    /** Index of the question being editted. */
    @Input() questionIdx = -1;

    _revealEditMode = false;

    _viewModeHover = false;

    constructor(
        private _developerService: DeveloperService,
        formBuilder: FormBuilderService,
        private _ga: GoogleAnalytics,
        private _ngFormBuilder: FormBuilder
    ) {
        super(formBuilder);
    }

    _calcNextQuestionIdx(): void {
        for (let iter = this.questionIdx + 1; iter < this.developer.questions.length; iter++) {
            if (this.developer.questions[iter].importance === undefined) {
                this.questionIdx = iter;
                return;
            }
        }

        for (let iter = 0; iter < this.questionIdx; iter++) {
            if (this.developer.questions[iter].importance === undefined) {
                this.questionIdx = iter;
                return;
            }
        }
    }

    /** Initializes the object state. */
    _init(): void {
        if (this.questionIdx === -1) {
            this._calcNextQuestionIdx();
        }

        this._question = this.developer.questions[this.questionIdx];

        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(['answer', 'explanation', 'iDontCare', 'importance']);

        // Configure the form
        if (this._question.answerChoices) {
            this._validationMessages['answer'] =
                this._formBuilder.buildValidatorMessages('answer', ['required']);
        } else {
            this._validationMessages['answer'] =
                this._formBuilder.buildValidatorMessages('answer', ['required', ['maxlength', 12], 'pattern']);
        }
        this._validationMessages['explanation'] =
            this._formBuilder.buildValidatorMessages('explanation', [['maxlength', 1024]]);
        this._validationMessages['importance'] =
            this._formBuilder.buildValidatorMessages('importance', ['required']);

        let validationRules = new Map<string, Array<Validator>>();
        if (this._question.answerChoices) {
            validationRules['answer'] = [Validators.required];
        } else {
            validationRules['answer'] = [Validators.required, Validators.maxLength(12),
                Validators.pattern(this._formBuilder.moneyRegex)];
        }
        validationRules['explanation'] = [Validators.maxLength(1024)];
        validationRules['importance'] = [Validators.required];

        // Build the form
        let formGroup = {
            'answer': new FormControl(this._question.answer, validationRules['answer']),
            'explanation': new FormControl(this._question.explanation, validationRules['explanation']),
            'importance': new FormControl(this._question.importance, validationRules['importance'])
        };
        this._form = this._ngFormBuilder.group(formGroup);

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Process the user clicking the Answer button. */
    _onAnswer(): void {
        // Update the question model with the editted values
        if (this._form.value.answer) {
            this._question.answer = this._form.value.answer;
        }
        this._question.importance = this._form.value.importance;
        if (this._form.value.explanation) {
            this._question.explanation = this._form.value.explanation;
        }
        this.developer.questions[this.questionIdx] = this._question;

        // Update the database record
        this._developerService.update(this.developer.id, this.developer)
            .then(
                // If the update operation succeeds
                (developer) => {
                    if (this.isEditMode) {
                        // Report the initial answer to analytics
                        this._ga.sendEvent('question_view_edit', 'initial_answer');

                        // Move to the next question
                        this._calcNextQuestionIdx();
                    } else {
                        // Report the editted answer to analytics
                        this._ga.sendEvent('question_view_edit', 'reanswer');

                        // Hide the edit view
                        this._revealEditMode = false;
                        this._viewModeHover = false;
                    }

                    // Update the component state
                    this._init();
                },
                // If the update operation fails
                (error) => {
                    // Report the error to analytics
                    this._ga.sendEvent('question_view_edit', 'error');
                    
                    // Reveal the unexpected error
                    this._unexpectedError = true;
                }
            );
    }

    _onClick(importance: Importance): void {
        this._form.controls['importance'].setValue(importance);
        this._form.controls['importance'].markAsDirty();

        if (importance === Importance.iDontCare) {
            this._form.controls['answer'].disable();
            this._form.controls['explanation'].disable();
        } else {
            this._form.controls['answer'].enable();
            this._form.controls['explanation'].enable();
        }
    }

    _onClickCancel(event): void {
        this._revealEditMode = false;

        event.stopPropagation();
    }

    _onClickPanel(): void {
        if (!this.isEditMode && !this._revealEditMode) {
            this._revealEditMode = true;
            this._form.controls['importance'].setValue(undefined);
        }
    }

    _formatSalary(answerIn: string): string {
        let answer = answerIn;

        if (!answer) {
            return '';
        }

        if (!answer.startsWith('$')) {
            answer = '$' + answer;
        }

        let digitCount = 0;
        for (let iter = answer.length - 1; iter >= 0; iter--) {
            if (digitCount === 3) {
                if (answer[iter] !== ',' && answer[iter] !== '$') {
                    answer = [answer.slice(0, iter + 1), ',', answer.slice(iter + 1)].join('');
                } else {
                    iter--;
                }

                digitCount = 0;
            }

            digitCount++;
        }

        return answer;
    }

    _onFormChanged(): void {
        if (!this._question.answerChoices) {
            this._form.controls['answer'].setValue(this._formatSalary(this._form.value.answer));
            this._form.controls['answer'].markAsDirty();
        }

        super._onFormChanged();
    }

    _onMouseEnter(importance: Importance): void {
        this._importanceHover = importance;
    }

    _onMouseLeave(): void {
        this._importanceHover = undefined;
    }

    _onMouseEnterPanel(): void {
        this._viewModeHover = true;
    }

    _onMouseLeavePanel(): void {
        this._viewModeHover = false;
    }

    /** Process the user clicking the Skip Question button. */
    _onSkipQuestion(): void {
        // Clear the error message if it exists
        this._unexpectedError = false;

        // Move to the next question
        this._calcNextQuestionIdx();

        // Update the component state
        this._init();
    }
}
