/**
 * @author R. Matt McCann
 * @brief Unit tests for the NextQuestion component
 * @copyright &copy; 2017 R. Matt McCann
 */

import { async, TestBed, fakeAsync } from '@angular/core/testing';
import { Router } from '@angular/router';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { itFA } from '../../../misc/testing/form-test';
import { NextQuestionComponent } from './next-question.component';


describe('NextQuestionComponent', () => {
    let component, fixture, mockGa, mockRouter;

    beforeEach(async(() => {
        mockGa = jasmine.createSpyObj('Ga', ['sendEvent']);
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
            declarations: [ NextQuestionComponent ],
            providers: [
                { provide: GoogleAnalytics, useValue: mockGa },
                { provide: Router, useValue: mockRouter }
            ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(NextQuestionComponent);
            component = fixture.componentInstance;
        });
    }));

    describe('_getNextQuestionText', () => {
        it('returns the next question text', () => {
            component.developer = jasmine.createSpy('Developer');
            component.developer.questions = [
                {
                    importance: 1,
                    text: 'something'
                },
                {
                    importance: undefined,
                    text: 'something else'
                }
            ];

            expect(component._getNextQuestionText()).toEqual('something else');
        });
    });

    describe('_onClickAnswer', () => {
        it('navigates to the questions page', () => {
            component.developer = jasmine.createSpy('Developer');
            component.developer.id = 'id';

            component._onClickAnswer();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['developer', 'id', 'questions']);
        });
    });

    describe('template', () => {
        describe('section: Your next happiness question:', () => {
            beforeEach(fakeAsync(() => {
                component.developer = jasmine.createSpy('Developer');
                component.developer.questions = [{ importance: undefined, text: 'something' }];

                fixture.detectChanges();
            }));

            it('reveals the section when a question is not answered', () => {
                expect(fixture.nativeElement.querySelector('#needsQuestionsAnswered')).not.toBe(null);
            });

            itFA('hides the section when all questions are answered', () => {
                component.developer.questions = [{ importance: 1, text: 'something' }];

                fixture.detectChanges();

                expect(fixture.nativeElement.querySelector('#needsQuestionsAnswered')).toBe(null);
            });

            describe('label: answer text', () => {
                it('contains the question text', () => {
                    let elem = fixture.nativeElement.querySelector('#answerText');

                    expect(elem.innerHTML).toEqual('something');
                });
            });

            describe('button: answer button', () => {
                it('fires _onClickAnswer when clicked', () => {
                    spyOn(component, '_onClickAnswer');

                    fixture.nativeElement.querySelector('#answerButton').click();

                    expect(component._onClickAnswer).toHaveBeenCalled();
                });
            });
        });

        describe('section: You answered your happiness questions', () => {
            beforeEach(fakeAsync(() => {
                component.developer = jasmine.createSpy('Developer');
                component.developer.questions = [{ importance: 1, text: 'something' }];

                fixture.detectChanges();
            }));

            it('reveals the section when all questions are answered', () => {
                expect(fixture.nativeElement.querySelector('#allQuestionsAnswered')).not.toBe(null);
            });

            itFA('hides the section when not all questions are answered', () => {
                component.developer.questions[0].importance = undefined;

                fixture.detectChanges();

                expect(fixture.nativeElement.querySelector('#allQuestionsAnswered')).toBe(null);
            });
        });
    });
});
