/**
 * @author R. Matt McCann
 * @brief Shows the developer the next happiness question and prompts them to answer it
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { GoogleAnalytics } from '../../../analytics/google/google-analytics';
import { Developer } from '../../developer.model';


/** Shows the developer the next happiness question and prompts them to answer it. */
@Component({
    selector: 'next-question',
    templateUrl: './next-question.component.html',
    styleUrls: ['./next-question.component.scss']
})
export class NextQuestionComponent {
    @Input() developer: Developer;

    constructor(
        private _ga: GoogleAnalytics,
        private _router: Router
    ) { }

    /** Returns the text of the next question for the developer to answer. */
    _getNextQuestionText(): string {
        for (let question of this.developer.questions) {
            if (question.importance === undefined) {
                return question.text;
            }
        }
    }

    _onClickAnswer(): void {
        // Report the click to analytics
        this._ga.sendEvent('next_question', 'click_answer');

        this._router.navigate(['developer', this.developer.id, 'questions']);
    }
};
