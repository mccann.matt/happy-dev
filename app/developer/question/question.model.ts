/**
 * @author R. Matt McCann
 * @brief Model for happiness questions
 * @copyright &copy; 2017 R. Matt McCann
 */


export enum Importance {
    iDontCare = 0,
    thatdBeNice,
    mustHave
};

export interface Question {
    /** Answer to the question. */
    answer?: number;

    /** Possible answers to the question when the question is multiple choice. */
    answerChoices?: Array<string>;

    /** Explanation of why the developer answered how they did. */
    explanation?: string;

    /** Level of important that the company meet the answer. */
    importance?: Importance;

    /** Text of the question. */
    text: string;
};
