/**
 * @author R, Matt McCann
 * @brief Defines the Angular module for the question functionality
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../misc/shared.module';
import { NextQuestionComponent } from './next-question/next-question.component';
import { QuestionDetailsComponent } from './question-details/question-details.component';
import { QuestionViewEditComponent } from './question-view-edit/question-view-edit.component';
import { QuestionProgressMeterComponent } from './question-progress-meter/question-progress-meter.component';

export let MODULE_DEF = {
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    exports: [
        NextQuestionComponent,
        QuestionDetailsComponent,
        QuestionProgressMeterComponent,
        QuestionViewEditComponent
    ],
    declarations: [
        NextQuestionComponent,
        QuestionDetailsComponent,
        QuestionProgressMeterComponent,
        QuestionViewEditComponent
    ]
};

@NgModule(MODULE_DEF)
export class QuestionModule { }
