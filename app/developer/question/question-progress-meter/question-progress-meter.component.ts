/**
 * @author R. Matt McCann
 * @brief Reports the question answering completion progress.
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, Input } from '@angular/core';

import { Developer } from '../../developer.model';


/** Reports the question answering completion progress. */
@Component({
    selector: 'question-progress-meter',
    templateUrl: './question-progress-meter.component.html',
    styleUrls: ['./question-progress-meter.component.scss']
})
export class QuestionProgressMeterComponent {
    @Input() developer: Developer;

    _getCompletionPercentage(): number {
        let numAnsweredQuestions = 0;

        for (let question of this.developer.questions) {
            if (question.importance !== undefined) {
                numAnsweredQuestions += 1;
            }
        }

        return (numAnsweredQuestions / this.developer.questions.length) * 100;
    }
};