/**
 * @author R. Matt McCann
 * @brief Unit tests for the QuestionsDetails component
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Observable } from 'rxjs/Observable';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { itFA } from '../../../misc/testing/form-test';
import { DeveloperService } from '../../developer.service';
import { QuestionDetailsComponent } from './question-details.component';


describe('QuestionDetailsComponent', () => {
    let component, fixture, mockDeveloperService, mockRouter;

    beforeEach(async(() => {
        mockDeveloperService = jasmine.createSpyObj('DeveloperService', ['read']);
        mockRouter = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
            declarations: [ QuestionDetailsComponent ],
            providers: [
                { provide: DeveloperService, useValue: mockDeveloperService },
                { provide: Router, useValue: mockRouter },
                { provide: ActivatedRoute, useValue: { params: Observable.of({id: 'id'}) } }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(QuestionDetailsComponent);
            component = fixture.componentInstance;
        });
    }));

    describe('_hasUnansweredQuestions', () => {
        beforeEach(() => {
            component._developer = jasmine.createSpy('Developer');
            component._developer.questions = [{ importance: undefined }];
        });

        it('returns true when there are unanswered questions', () => {
            expect(component._hasUnansweredQuestions()).toBe(true);
        });

        it('returns false when there are no unanswered questions', () => {
            component._developer.questions[0].importance = 1;

            expect(component._hasUnansweredQuestions()).toBe(false);
        });
    });

    describe('ngOnInit', () => {
        let mockDeveloper = jasmine.createSpy('Developer');

        beforeEach(() => {
            mockDeveloperService.read.and.returnValue({
                then: (then_cb, catch_cb) => { }
            });
        });

        itFA('attempts to read the developer record', () => {
            fixture.detectChanges();

            expect(mockDeveloperService.read).toHaveBeenCalledWith('id');
        });

        describe('after successful developer read', () => {
            beforeEach(() => {
                mockDeveloperService.read.and.returnValue({
                    then: (then_cb, catch_cb) => { then_cb(mockDeveloper); }
                });
            });

            itFA('sets the local developer variable', () => {
                fixture.detectChanges();
                tick();

                expect(component._developer).toBe(mockDeveloper);
            });
        });

        describe('after failed developer read', () => {
            beforeEach(() => {
                mockDeveloperService.read.and.returnValue({
                    then: (then_cb, catch_cb) => { catch_cb('error'); }
                });
            });

            itFA('navigates to the 404 page', () => {
                fixture.detectChanges();
                tick();

                expect(mockRouter.navigate).toHaveBeenCalledWith(['/404'], {skipLocationChange: true});
            });
        });
    });

    describe('panel: Next Question Edit', () => {
        xit('revealed when there are unanswered questions', () => {

        });

        xit('hidden when there are no unanswered questions', () => {

        });
    });

    describe('panel list: Answered questions', () => {
        xit('hides questions that are not answered', () => {

        });

        xit('reveals questions that are answered', () => {

        });
    });
});
