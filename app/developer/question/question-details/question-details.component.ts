/**
 * @author R. Matt McCann
 * @brief Interface for the developer to answer happiness questions and see/edit previously answered questions
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { Developer } from '../../developer.model';
import { DeveloperService} from '../../developer.service';

/** Interface for the developer to answer happiness questions and see/edit previously answered questions. */
@Component({
    selector: 'questions-details',
    templateUrl: './question-details.component.html',
    styleUrls: ['./question-details.component.scss']
})
export class QuestionDetailsComponent implements OnInit {
    /** Index of the currently displayed question under edit. */
    _currentQuestionIdx = -1;

    /** Developer object being editted. */
    _developer: Developer;

    constructor(
        private _developerService: DeveloperService,
        private _route: ActivatedRoute,
        private _router: Router
    ) { }

    /** Returns true if there are unanswered questions. */
    _hasUnansweredQuestions(): boolean {
        for (let iter = 0; iter < this._developer.questions.length; iter++) {
            if (this._developer.questions[iter].importance === undefined) {
                return true;
            }
        }

        return false;
    }

    /** Initialize the component state. */
    ngOnInit(): void {
        this._route.params
            .switchMap((params: Params) => {
                return new Promise<Developer>((resolve, reject) => {
                    // Fetch the developer record
                    this._developerService.read(params['id'])
                        .then(
                            // If the read succeeds
                            profile => { resolve(profile); },
                            // If the read fails
                            error => { reject(error); }
                        );
                });
            })
            .subscribe(
                // If the read succeeds
                (developer) => {
                    this._developer = developer;
                },
                // If the read fails
                (error) => {
                    // Display the 404 error page
                    this._router.navigate(['/404'], {skipLocationChange: true});
                }
            );
    }
};
