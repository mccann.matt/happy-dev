/**
 * @author R. Matt McCann
 * @brief Model for the developer accounts
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Question } from './question/question.model';

export interface Developer {
    city?: string;

    education?: any;

    id: string;

    overview?: string;

    portrait_thumbnail?: string;

    questions?: Array<Question>;

    skills?: Array<string>;

    state?: string;

    title?: string;

    work_history?: any;
};

/** Enum used to indicate which section is of interest. */
export enum SectionType {
    PORTRAIT,
    BASIC_INFO,
    SKILL_TAGS,
    OVERVIEW,
    EDUCATION,
    WORK_HISTORY,
    HAPPINESS_QUESTIONS
};
