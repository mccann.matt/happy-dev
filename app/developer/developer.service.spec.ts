/**
 * @author R. Matt McCann
 * @brief Unit tests for the DeveloperService
 * @copyright &copy; 2017 R. Matt McCann
 */

import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { DeveloperService } from './developer.service';


describe('DeveloperService', () => {
    let developerService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                DeveloperService,
            ]
        });
    });

    beforeEach(inject([DeveloperService], (_developerService) => {
        developerService = _developerService;
    }));

    describe('constructor', () => {
        it('correctly initializes the url', () => {
            expect(developerService._urlBase).toEqual('http://localhost:8001/developer/');
        });
    });
});
