'''
@author R. Matt McCann
@brief Unit tests for the auth helper decorators AND general auth testing helper functions
@copyright &copy; 2017 R. MattMcCann
'''

import json


def create_account_but_dont_login(api):
        '''
        Creates a test account but does not log into it.

        @param api Test client for the api
        @returns Id of the test account
        '''
        # Create the test profile
        dev_profile_data = {
            'email': 'mccann.matt@gmail.com',
            'first_name': 'Matt',
            'last_name': 'McCann',
            'password': 'password'
        }
        response = api.post('/account/', data=json.dumps(dev_profile_data), content_type='application/json')
        decoded = json.loads(response.get_data(as_text=True))

        return decoded['id']


def create_account_and_login(api):
    '''
    Creates a test account and logs into it.

    @param api Test client for the api
    @returns Id of the test account
    '''
    # Create the test profile
    dev_profile_data = {
        'email': 'mccann.matt@gmail.com',
        'first_name': 'Matt',
        'last_name': 'McCann',
        'password': 'password'
    }
    response = api.post('/account/', data=json.dumps(dev_profile_data), content_type='application/json')
    decoded = json.loads(response.get_data(as_text=True))
    print(decoded)

    # Log in to the profile
    login_data = {
        'email': 'mccann.matt@gmail.com',
        'password': 'password',
        'remember_me': False
    }
    api.post('/account/login/', data=json.dumps(login_data), content_type='application/json')

    return decoded['id']


def create_account_but_login_to_another(api):
    '''
    Creates a test account but logs into a different test account.

    @param api Test client for the api
    @returns Id of the test account NOT logged into
    '''
    create_account_and_login(api)

    dev_profile_data = {
        'email': 'mccann.matt2@gmail.com',
        'first_name': 'Matt',
        'last_name': 'McCann',
        'password': 'password'
    }
    response = api.post('/account/', data=json.dumps(dev_profile_data), content_type='application/json')
    decoded = json.loads(response.get_data(as_text=True))

    return decoded['id']


def create_staff_account_and_login(api):
    '''
    Creates a staff account and logs into it

    @param api Test client for the api
    @returns Id of the staff account logged into
    '''
    from happydev.api.account import Account

    id = create_account_and_login(api)

    account = Account.db_get(id=id)
    account.is_staff = True
    account.db_update()

    return id
