'''
@author R. Matt McCann
@brief Unit tests for the degree types
@copyright &copy; 2017 R. Matt McCann
'''

import json

import happydev.api.test as test_helper

from happydev.api.api_root import api
from happydev.api.degree import Degree


class TestDegree(object):
    ''' Unit tests for the degrees model. '''

    def test_name__too_long(self):
        ''' Test a name value that is too long. '''
        degree = Degree()

        test_helper.check_empty_value(degree, 'name')
        test_helper.check_too_long_value(degree, 'name', 65)


class TestDegreeApi(object):
    ''' Unit tests for the Degree API. '''

    def setup(self):
        ''' Setup before each test. '''
        Degree.db_debootstrap()
        Degree.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

    def test_degrees_suggestions(self):
        ''' Simple good path test of degrees suggestions endpoint. '''
        for iter in range(20):
            degree = Degree()
            degree.name = 'A%d' % iter
            degree.db_create()

        response = self.api.post('/degree/suggest/', data=json.dumps('a'), content_type='application/json')
        decoded = json.loads(response.get_data(as_text=True))

        assert len(decoded) == 10, decoded
