import logging

logging.getLogger('boto').setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.INFO)


def check_empty_value(object, field):
    '''
    Tests assigning an empty value to a field.

    @param object Object whose field is being set
    @param field Field to be set
    '''
    try:
        setattr(object, field, '')
        assert False, "Should have thrown ValueError"
    except ValueError:
        pass


def check_too_long_value(object, field, length):
    '''
    Tests assigning a too long value to a field.

    @param object Object whose field is being set
    @param field Field to be set
    @param length Length of the value to be set
    '''
    try:
        setattr(object, field, 'A'*length)
        assert False, "Should have thrown ValueError"
    except ValueError:
        pass
