'''
@author R. Matt McCann
@brief Unit tests for the Company model and API
@copyright &copy; 2017 R. Matt McCann
'''

import json

import happydev.api.test as test_helper
import happydev.api.test.test_auth_helper as test_auth_helper

from happydev.api.account import Account
from happydev.api.api_root import api
from happydev.api.company import Company


class TestCompany(object):
    ''' Unit tests for the Company model. '''

    def setup(self):
        ''' Before each test. '''
        self.record = Company.construct()

    def test_about__bad_input(self):
        ''' Tests setting about to bad values. '''
        test_helper.check_empty_value(self.record, 'about')
        test_helper.check_too_long_value(self.record, 'about', 4097)

    def test_city__bad_input(self):
        ''' Tests setting city to bad values. '''
        test_helper.check_empty_value(self.record, 'city')
        test_helper.check_too_long_value(self.record, 'city', 65)

    def test_name__bad_input(self):
        ''' Tests setting name to bad values. '''
        test_helper.check_empty_value(self.record, 'name')
        test_helper.check_too_long_value(self.record, 'name', 65)

    def test_state__bad_input(self):
        ''' Tests setting state to bad values. '''
        test_helper.check_empty_value(self.record, 'state')

    def test_website_url__bad_input(self):
        ''' Tests setting website_url to bad values. '''
        test_helper.check_empty_value(self.record, 'website_url')
        test_helper.check_too_long_value(self.record, 'website_url', 65)


class TestCompanyApi(object):
    ''' Unit tests for the Company API. '''

    def setup(self):
        ''' Setup before each test. '''
        Account.db_debootstrap()
        Account.db_bootstrap()
        Company.db_debootstrap()
        Company.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

        self.company_data = {
            'about': 'about',
            'city': 'city',
            'name': 'name',
            'state': 'state',
            'number_of_employees': 2,
            'year_founded': 2017,
            'website_url': 'google.com'
        }

    def test_create__not_logged_in(self):
        ''' Tests trying to create a company when not logged in. '''
        response = self.api.post('/company/')
        assert response.status_code == 401, response.status_code

    def test_create__not_staff(self):
        ''' Tests trying to create a company when logged into a non-staff account. '''
        test_auth_helper.create_account_and_login(self.api)

        response = self.api.post('/company/')
        assert response.status_code == 401, response.status_code

    def test_create__good_path(self):
        ''' Tests creating a company record with proper data. '''
        test_auth_helper.create_staff_account_and_login(self.api)

        response = self.api.post('/company/', data=json.dumps(self.company_data), content_type='application/json')
        assert response.status_code == 200, response.status_code

        # Check the response includes the details of the created company
        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['about'] == self.company_data['about'], decoded
        assert decoded['city'] == self.company_data['city'], decoded
        assert decoded['name'] == self.company_data['name'], decoded
        assert decoded['state'] == self.company_data['state'], decoded
        assert decoded['number_of_employees'] == self.company_data['number_of_employees'], decoded
        assert decoded['year_founded'] == self.company_data['year_founded'], decoded
        assert decoded['website_url'] == self.company_data['website_url'], decoded

        # Check the company record was created properly
        company = Company.db_get(id=decoded['id'])
        assert company.about == self.company_data['about'], decoded
        assert company.city == self.company_data['city'], decoded
        assert company.name == self.company_data['name'], decoded
        assert company.state == self.company_data['state'], decoded
        assert company.number_of_employees == self.company_data['number_of_employees'], decoded
        assert company.year_founded == self.company_data['year_founded'], decoded
        assert company.website_url == self.company_data['website_url'], decoded

    def test_create__missing_required(self):
        ''' Tests creating a company record with improper data. '''
        test_auth_helper.create_staff_account_and_login(self.api)

        response = self.api.post('/company/', data=json.dumps({}), content_type='application/json')
        assert response.status_code == 403, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['status'] == 'failure', decoded
        assert decoded['message'] == 'missing_keys', decoded
        assert decoded['missing'] == \
            ['about', 'city', 'name', 'state', 'number_of_employees', 'year_founded', 'website_url'], decoded

    def test_read__good_path(self):
        ''' Tests reading a company record that exists. '''
        company = Company.construct()
        company.id = 'id'
        company.about = 'about'
        company.city = 'city'
        company.state = 'state'
        company.number_of_employees = 2
        company.year_founded = 2001
        company.website_url = 'website_url'
        company.db_create()

        response = self.api.get('/company/%s/' % company.id)
        assert response.status_code == 200, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['about'] == 'about', decoded
        assert decoded['city'] == 'city', decoded
        assert decoded['state'] == 'state', decoded
        assert decoded['number_of_employees'] == 2, decoded
        assert decoded['year_founded'] == 2001, decoded
        assert decoded['website_url'] == 'website_url', decoded

    def test_read__does_not_exist(self):
        ''' Tests reading a company record that does not exist. '''
        response = self.api.get('/company/id/')
        assert response.status_code == 404, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['status'] == 'failure', decoded

    def test_suggest__not_logged_in(self):
        ''' Tests trying to search companies when not logged in. '''
        response = self.api.post('/company/suggest/')
        assert response.status_code == 401, response.status_code

    def test_suggest__not_staff(self):
        ''' Tests trying to search companies when not logged into a staff account. '''
        test_auth_helper.create_account_and_login(self.api)

        response = self.api.post('/company/suggest/')
        assert response.status_code == 401, response.status_code

    # def test_suggest__good_path(self):
    #    ''' Tests searching comapny suggestions. '''
    #    test_auth_helper.create_staff_account_and_login(self.api)

    #    # Create something to search for
    #    self.api.post('/company/', data=json.dumps(self.company_data), content_type='application/json')

    #    # Search the companies
    #    response = self.api.post('/company/suggest/', data=json.dumps('na'), content_type='application/json')

    #    # Check the results
    #    decoded = json.loads(response.get_data(as_text=True))
    #    assert len(decoded) == 1, decoded
    #    assert decoded[0]['name'] == 'name', decoded

    def test_update__not_logged_in(self):
        ''' Tests trying to update a company when not loggin in. '''
        response = self.api.put('/company/id/')
        assert response.status_code == 401, response.status_code

    def test_update__not_staff(self):
        ''' Tests trying to update a company when logged into a non-staff account. '''
        test_auth_helper.create_account_and_login(self.api)

        response = self.api.put('/company/id/')
        assert response.status_code == 401, response.status_code

    def test_update__does_not_exist(self):
        ''' Tests trying to updatea company record that does not exist. '''
        test_auth_helper.create_staff_account_and_login(self.api)

        response = self.api.put('/company/id/')
        assert response.status_code == 404, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['status'] == 'failure', decoded

    # def test_update__good_path(self):
    #    ''' Tests updating a valid company record. '''
    #    test_auth_helper.create_staff_account_and_login(self.api)

    #    response = self.api.post('/company/', data=json.dumps(self.company_data), content_type='application/json')
    #    decoded = json.loads(response.get_data(as_text=True))
    #    id = decoded['id']

    #    response = self.api.put(
    #        '/company/%s/' % id, data=json.dumps({'about': 'something'}), content_type='application/json')
    #    assert response.status_code == 200, response.status_code

    #    # Check the response includes the details of the updated company
    #    decoded = json.loads(response.get_data(as_text=True))
    #    assert decoded['status'] == 'success', decoded

    #    # Check the company record was updated properly
    #    company = Company.db_get(id=id)
    #    assert company.about == 'something', decoded
    #    assert company.city == self.company_data['city'], decoded
    #    assert company.state == self.company_data['state'], decoded
    #    assert company.number_of_employees == self.company_data['number_of_employees'], decoded
    #    assert company.year_founded == self.company_data['year_founded'], decoded
    #    assert company.website_url == self.company_data['website_url'], decoded
