'''
@author R. Matt McCann
@brief Unit tests for the Developer model and API.
@copyright &copy; 2017 R. Matt McCann
'''

import json
import mock

from io import BytesIO

import happydev.api.test.test_auth_helper as test_auth_helper
import pydi

from happydev.api.account import Account
from happydev.api.api_root import api
from happydev.api.developer import Developer


class TestDeveloperApi(object):
    ''' Unit tests for the Developer API. '''

    def setup(self):
        ''' Setup before each test. '''
        Account.db_debootstrap()
        Account.db_bootstrap()
        Developer.db_debootstrap()
        Developer.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

    # def test_create__good_path_dev(self):
    #    ''' Simple good path test for creating the dev profile. '''
    #    # Create the profile
    #    response = self.api.post(
    #        '/developer/', data=json.dumps(self.dev_profile_data), content_type='application/json')
    #
    #    # Check the response includes the details of the created profile
    #    decoded = json.loads(response.get_data(as_text=True))
    #    assert decoded['email'] == 'mccann.matt@gmail.com', decoded
    #    assert decoded['first_name'] == 'Matt', decoded
    #    assert decoded['last_name'] == 'McCann', decoded
    #    assert 'id' in decoded.keys(), decoded
    #    assert 'password' not in decoded.keys(), decoded
    #    assert 'salt' not in decoded.keys(), decoded
    #
    #    # Check the profile was created properly
    #    profile = DeveloperProfile.db_get(id=decoded['id'])
    #    assert profile.id == decoded['id'], profile
    #    assert profile.email == 'mccann.matt@gmail.com', profile
    #    assert profile.first_name == 'Matt', profile
    #    assert profile.last_name == 'McCann', profile
    #    assert profile.password == hashlib.sha512(
    #        'password'.encode('utf-8') + profile.salt).digest(), profile

    # def test_create__duplicate_email(self):
    #    ''' Tests trying to create a profile with a duplicate email. '''
    #    # Create the first profile
    #    self.api.post(
    #        '/developer/', data=json.dumps(self.dev_profile_data), content_type='application/json')
    #    response = self.api.post(
    #        '/developer/', data=json.dumps(self.dev_profile_data), content_type='application/json')
    #
    #    # Check the request was rejected
    #    assert response.status_code == 403, response.status_code
    #    decoded = json.loads(response.get_data(as_text=True))
    #    assert decoded['status'] == 'failure'
    #    assert decoded['message'] == 'duplicate email'

    # def test_create__missing_required_fields(self):
    #    ''' Tests trying to create a profile with incomplete data. '''
    #    data = {
    #        'first_name': 'bob'
    #    }
    #    response = self.api.post('/developer/', data=json.dumps(data), content_type='application/json')
    #
    #    # Check that the request was rejected
    #    assert response.status_code == 403, response.status_code

    # def test_get__developers_view__no_login(self):
    #    id = test_auth_helper.create_account_but_dont_login(self.api)
    #
    #    # Try to fetch the profile
    #    response = self.api.get('/developer/%s/' % id)
    #    assert response.status_code == 401, response.status_code

    # def test_get__developers_view__different_user(self):
    #    id = test_auth_helper.create_account_but_login_to_another(self.api)
    #
    #    # Try to fetch the profile
    #    response = self.api.get('/developer/%s/' % id)
    #    assert response.status_code == 401, response.status_code

    # def test_get__developers_view(self):
    #    ''' Simple good path test for fetching a developer profile. '''
    #    id = test_auth_helper.create_account_and_login(self.api)
    #
    #    # Fetch the profile
    #    response = self.api.get('/developer/%s/' % id)
    #    decoded = json.loads(response.get_data(as_text=True))
    #
    #    # Check the profile was returned as expected
    #    assert decoded['email'] == 'mccann.matt@gmail.com', decoded
    #    assert decoded['first_name'] == 'Matt', decoded
    #    assert decoded['last_name'] == 'McCann', decoded
    #    assert 'id' in decoded.keys(), decoded
    #    assert 'password' not in decoded.keys(), decoded
    #    assert 'salt' not in decoded.keys(), decoded

    # def test_get__companies_view(self):
    #    ''' Simple good path test for fetch a preview of a developer profile. '''
    #    id = test_auth_helper.create_account_but_dont_login(self.api)
    #
    #    # Fetch the profile
    #    response = self.api.get('/developer/%s/preview/' % id)
    #    decoded = json.loads(response.get_data(as_text=True))
    #
    #    # Check the profile was returned as expected
    #    assert 'email' not in decoded.keys(), decoded
    #    assert 'first_name' not in decoded.keys(), decoded
    #    assert 'last_name' not in decoded.keys(), decoded
    #    assert 'id' in decoded.keys(), decoded
    #    assert 'password' not in decoded.keys(), decoded
    #    assert 'salt' not in decoded.keys(), decoded

    def test_portrait_upload__no_login(self):
        ''' Tests trying to call the portrait upload endpoint without logging in. '''
        id = test_auth_helper.create_account_but_dont_login(self.api)
        self.api.post('/developer/', content_type='application/json', data=json.dumps({'id': id}))

        # Upload the portrait
        data = dict(file=(BytesIO(b'my file contents'), "portrait.123"),)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)

        assert response.status_code == 401, response.status_code

    def test_portrait_upload__different_user(self):
        ''' Tests trying to call the portrait upload endpoint, but for a different user than who is authed. '''
        id = test_auth_helper.create_account_but_login_to_another(self.api)
        self.api.post('/developer/', content_type='application/json', data=json.dumps({'id': id}))

        # Upload the portrait
        data = dict(file=(BytesIO(b'my file contents'), "portrait.123"),)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)

        assert response.status_code == 401, response.status_code

    def test_portrait_upload__invalid_file_extensions(self):
        ''' Tests trying to upload a portrait thumbnail that has an invalid file extension. '''
        id = test_auth_helper.create_account_and_login(self.api)
        self.api.post('/developer/', content_type='application/json', data=json.dumps({'id': id}))

        # Upload the portrait
        data = dict(file=(BytesIO(b'my file contents'), "portrait.123"),)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)
        decoded = json.loads(response.get_data(as_text=True))

        # Check that the portrait upload was rejected
        assert response.status_code == 403, response.status_code
        assert decoded['status'] == 'failure', decoded
        assert decoded['message'] == 'unsupported file extension', decoded

    def test_portrait_upload__first_upload(self):
        ''' Tests uploading the first portrait thumbnail associated with the profile. '''
        mock_conn = mock.MagicMock()
        mock_conn.upload_fileobj = mock.MagicMock()
        mock_conn.put_object_acl = mock.MagicMock()
        di = pydi.di()
        di.bindings['s3_client'] = mock_conn

        id = test_auth_helper.create_account_and_login(self.api)
        self.api.post('/developer/', content_type='application/json', data=json.dumps({'id': id}))

        # Upload the portrait
        file = (BytesIO(b'my file contents'), "portrait.jpg")
        data = dict(file=file,)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)
        decoded = json.loads(response.get_data(as_text=True))

        # Check that the file was uploaded
        print(decoded)
        print(mock_conn.upload_fileobj.call_args)
        mock_conn.upload_fileobj.call_args[0][1] == 'happydev.io'
        mock_conn.upload_fileobj.call_args[0][2] == '%s/portrait.0.jpg' % id
        mock_conn.put_object_acl.assert_called_with(
            ACL='public-read', Bucket='happydev.io', Key='%s/portrait.0.jpg' % id)

        # Check the response
        assert response.status_code == 200, response.status_code
        assert decoded['status'] == 'success', decoded

        # Check that the profile was updated properly
        profile = Developer.db_get(id=id)
        assert profile.portrait_thumbnail == \
            'https://s3-us-west-2.amazonaws.com/happydev.io/%s/portrait.0.jpg' % id

    def test_portrait_upload__multiple_uploads(self):
        ''' Tests uploading multiple portrait thumbnails. '''
        mock_conn = mock.MagicMock()
        mock_conn.upload_fileobj = mock.MagicMock()
        mock_conn.put_object_acl = mock.MagicMock()
        di = pydi.di()
        di.bindings['s3_client'] = mock_conn

        id = test_auth_helper.create_account_and_login(self.api)
        self.api.post('/developer/', content_type='application/json', data=json.dumps({'id': id}))

        # Upload the portrait
        file = (BytesIO(b'my file contents'), "portrait.jpg")
        data = dict(file=file,)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)
        decoded = json.loads(response.get_data(as_text=True))
        file = (BytesIO(b'my file contents'), "portrait.jpg")
        data = dict(file=file,)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)
        decoded = json.loads(response.get_data(as_text=True))
        file = (BytesIO(b'my file contents'), "portrait.jpg")
        data = dict(file=file,)
        response = self.api.post(
            '/developer/%s/portrait/' % id, content_type='multipart/form-data', data=data)
        decoded = json.loads(response.get_data(as_text=True))

        # Check that the file was uploaded
        mock_conn.upload_fileobj.call_args[0][1] == 'happydev.io'
        mock_conn.upload_fileobj.call_args[0][2] == '%s/portrait.2.jpg' % id
        mock_conn.put_object_acl.assert_called_with(
            ACL='public-read', Bucket='happydev.io', Key='%s/portrait.2.jpg' % id)

        # Check the response
        assert response.status_code == 200, response.status_code
        assert decoded['status'] == 'success', decoded

        # Check that the profile was updated properly
        profile = Developer.db_get(id=id)
        assert profile.portrait_thumbnail == \
            'https://s3-us-west-2.amazonaws.com/happydev.io/%s/portrait.2.jpg' % id

    def test_put__no_login(self):
        ''' Tests trying to update a profile when not properly logged in. '''
        id = test_auth_helper.create_account_but_dont_login(self.api)
        self.api.post('/developer/', content_type='application/json', data={'id': id})

        # Try to fetch the profile
        data = json.dumps({})
        response = self.api.put('/developer/%s/' % id, content_type='application/json', data=data)
        assert response.status_code == 401, response.status_code

    def test_put__different_user(self):
        ''' Tests trying to update a profile when not properly logged in. '''
        id = test_auth_helper.create_account_but_login_to_another(self.api)
        self.api.post('/developer/', content_type='application/json', data={'id': id})

        # Try to fetch the profile
        data = json.dumps({})
        response = self.api.put('/developer/%s/' % id, content_type='application/json', data=data)
        assert response.status_code == 401, response.status_code

    # def test_put(self):
    #    ''' Tests updating the profile when properly logged in. '''
    #    id = test_auth_helper.create_account_and_login(self.api)
    #
    #    # Try to fetch the profile
    #    data = json.dumps({'first_name': 'Bob'})
    #    response = self.api.put('/developer/%s/' % id, content_type='application/json', data=data)
    #    decoded = json.loads(response.get_data(as_text=True))
    #
    #    assert decoded['first_name'] == 'Bob', decoded
    #
    #    # Check the profile was created properly
    #    profile = DeveloperProfile.db_get(id=id)
    #    assert profile.id == decoded['id'], profile
    #    assert profile.first_name == 'Bob', profile
