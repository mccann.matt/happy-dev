'''
@author R. Matt McCann
@brief Unit tests for the account model & API
@copyright &copy; 2017 R. Matt McCann
'''

import json

import happydev.api.account

from happydev.api.account import Account
from happydev.api.api_root import api


class TestAccount(object):
    ''' Unit tests for the Account model. '''

    @classmethod
    def setup_class(cls):
        ''' Setup the test fixture. '''
        Account.db_debootstrap()
        Account.db_bootstrap()

    def test_create__bad_email(self):
        ''' Tests creating a UserProfile with bad email values '''
        account = Account.construct()

        try:
            account.email = ' ' * 129
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

        try:
            account.email = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

        try:
            account.email = 'baddemail@badbadbad'
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

        try:
            account.email = 'badbasdfasgmail.com'
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

    def test_create__bad_first_name(self):
        ''' Tests creating a UserProfile with bad first name values. '''
        account = Account.construct()

        try:
            account.first_name = ' ' * 65
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

        try:
            account.first_name = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

    def test_create__bad_last_name(self):
        ''' Tests creating a UserProfile with bad last name values. '''
        account = Account.construct()

        try:
            account.last_name = ' ' * 65
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

        try:
            account.last_name = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

    def test_create__bad_password(self):
        ''' Tests creating a UserProfile with bad password values. '''
        account = Account.construct()

        try:
            account.password = None
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass

        try:
            account.password = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass


class TestAccountApi(object):
    ''' Unit tests for Account API. '''

    def setup(self):
        ''' Setup before each test. '''
        Account.db_debootstrap()
        Account.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

        self.account_data = {
            'email': 'mccann.matt@gmail.com',
            'first_name': 'Matt',
            'last_name': 'McCann',
            'password': 'password'
        }

    def test_load__missing(self):
        ''' Tests the login manager load user hook when the user account does not exist. '''
        assert happydev.api.account.account_load('id') is None

    def test_load__present(self):
        ''' Tests the login manager load user hook when the account exists. '''
        # Create the test profile
        response = self.api.post(
            '/account/', data=json.dumps(self.account_data), content_type='application/json')
        decoded = json.loads(response.get_data(as_text=True))
        id = decoded['id']

        assert happydev.api.account.account_load(id) is not None

    def test_login__good_path(self):
        ''' Tests logging in with a simple good path test. '''
        # Create the test profile
        response = self.api.post(
            '/account/', data=json.dumps(self.account_data), content_type='application/json')
        decoded = json.loads(response.get_data(as_text=True))
        id = decoded['id']

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 401, response.status_code

        # Log in to the profile
        login_data = {
            'email': 'mccann.matt@gmail.com',
            'password': 'password',
            'remember_me': False
        }
        response = self.api.post('/account/login/', data=json.dumps(login_data), content_type='application/json')
        assert response.status_code == 200, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['status'] == 'success'

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 200, response.status_code

    def test_login__no_such_account(self):
        ''' Tests logging in with an account that doesn't exist '''
        # Log in to the profile
        login_data = {
            'email': 'mccann.matt@gmail.com',
            'password': 'password',
            'remember_me': False
        }
        response = self.api.post('/account/login/', data=json.dumps(login_data), content_type='application/json')
        assert response.status_code == 403, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['status'] == 'failure'
        assert decoded['message'] == 'unrecognized email/password combo', decoded

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 401, response.status_code

    def test_login__bad_password(self):
        ''' Tests logging in with a bad password. '''
        # Create the test profile
        response = self.api.post(
            '/account/', data=json.dumps(self.account_data), content_type='application/json')

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 401, response.status_code

        # Log in to the profile
        login_data = {
            'email': 'mccann.matt@gmail.com',
            'password': 'not the password',
            'remember_me': False
        }
        response = self.api.post('/account/login/', data=json.dumps(login_data), content_type='application/json')
        assert response.status_code == 403, response.status_code

        decoded = json.loads(response.get_data(as_text=True))
        assert decoded['status'] == 'failure'
        assert decoded['message'] == 'unrecognized email/password combo'

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 401, response.status_code

    def test_logout(self):
        ''' Test logging out of a user account. '''
        # Create the test profile
        response = self.api.post(
            '/account/', data=json.dumps(self.account_data), content_type='application/json')
        decoded = json.loads(response.get_data(as_text=True))
        id = decoded['id']

        # Log in to the profile
        login_data = {
            'email': 'mccann.matt@gmail.com',
            'password': 'password',
            'remember_me': False
        }
        response = self.api.post('/account/login/', data=json.dumps(login_data), content_type='application/json')

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 200, response.status_code

        # Log out of the profile
        response = self.api.post('/account/logout/')
        assert response.status_code == 200, response.status_code

        # Try to access a login-required endpoint
        response = self.api.put('/account/%s/' % id, data=json.dumps({}), content_type='application/json')
        assert response.status_code == 401, response.status_code
