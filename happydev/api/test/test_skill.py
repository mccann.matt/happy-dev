'''
@author R. Matt McCann
@brief Unit tests for the skill tags
@copyright &copy; 2017 R. Matt McCann
'''

import json

import happydev.api.test as test_helper

from happydev.api.api_root import api
from happydev.api.skill import Skill


class TestSkill(object):
    ''' Unit tests for the skills model. '''

    def test_name__bad_input(self):
        ''' Tests setting name to bad values. '''
        skill = Skill()

        test_helper.check_empty_value(skill, 'name')
        test_helper.check_too_long_value(skill, 'name', 65)


class TestSkillApi(object):
    ''' Unit tests for the Skill API. '''

    def setup(self):
        ''' Setup before each test. '''
        Skill.db_debootstrap()
        Skill.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

    def test_skills_suggestions(self):
        ''' Simple good path test of skills suggestions endpoint. '''
        for iter in range(20):
            skill = Skill()
            skill.name = 'A%d' % iter
            skill.db_create()

        response = self.api.post('/skills/suggest/', data=json.dumps('a'), content_type='application/json')
        decoded = json.loads(response.get_data(as_text=True))

        assert len(decoded) == 6, decoded
