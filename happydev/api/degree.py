'''
@author R. Matt McCann
@brief Model & API for degree types
@copyright &copy; 2017 R. Matt McCann
'''

import os

import dynoflrud

from happydev.api.api_root import api
from happydev.api.suggestable import suggestable, list_suggestions_api_endpoint


class Degree(dynoflrud.Record):
    ''' A type of degree, used in autosuggest service / searchable '''

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HappyDev.Degree' % os.environ['TARGET']

    ''' Attribute definitions for the table. '''
    DB_TABLE_ATTRIBUTE_DEFINITIONS = [
        {'AttributeName': 'name_suggest_fl', 'AttributeType': 'S'},
        {'AttributeName': 'name_suggest', 'AttributeType': 'S'}
    ]

    ''' Primary key schema for the table. '''
    DB_TABLE_KEY_SCHEMA = [
        {'AttributeName': 'name_suggest_fl', 'KeyType': 'HASH'},
        {'AttributeName': 'name_suggest', 'KeyType': 'RANGE'}
    ]

    def __init__(self):
        super().__init__()

    @classmethod
    def construct(cls):
        ''' Override the standard record constructor as our primary is not 'id' '''
        return cls()

    @property
    def name(self):
        ''' Displayed name of the skill. '''
        return self._name

    @name.setter
    @suggestable
    def name(self, name):
        dynoflrud.require_max_length('name', name, 64)
        dynoflrud.require_not_empty('name', name)

        self._name = name


@api.route('/degree/suggest/', methods=['POST'])
def degree_suggestions():
    return list_suggestions_api_endpoint(Degree, 'name')
