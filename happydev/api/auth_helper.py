'''
@author R. Matt McCann
@brief Provides authentication decorators
@copyright &copy; 2017 R. Matt McCann
'''

import flask
import flask_login

from functools import wraps


def authenticated_owning_user_only(func):
    @flask_login.login_required
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if flask_login.current_user.id != kwargs['id']:
            response = flask.jsonify({'status': 'failure', 'message': 'access denied'})
            response.status_code = 401
            return response
        return func(*args, **kwargs)
    return decorated_view


def staff_only(func):
    @flask_login.login_required
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not flask_login.current_user.is_staff:
            response = flask.jsonify({'status': 'failure', 'message': 'access denied'})
            response.status_code = 401
            return response
        return func(*args, **kwargs)
    return decorated_view
