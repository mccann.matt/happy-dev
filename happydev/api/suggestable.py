'''
@author R. Matt McCann
@brief Mixin that enables a model/api to provide value suggestions
@copyright &copy; 2017 R. Matt McCann
'''

import flask

from functools import wraps

import dynoflrud
import pydi


def list_suggestions_api_endpoint(cls, field_name, results_limit=10):
    '''
    Provides a standard implementation for delivering suggestion results.

    @param cls Class which is being suggested
    @param field_name Suggestable field name
    @param results_limit Maximum number of results returned
    '''
    db = pydi.di()[pydi.DynamoDB]

    # Build the list all filter parameters
    fragment = flask.request.json
    attribute_values = {
        ':%s_suggest_fl' % field_name: {'S': fragment[0]},
        ':%s_suggest' % field_name: {'S': fragment}
    }
    filter_query = '%s_suggest_fl = :%s_suggest_fl AND begins_with(%s_suggest, :%s_suggest)' % \
        (field_name, field_name, field_name, field_name)

    # Fetch the raw results
    raw_results = db.query(
        TableName=cls.DB_TABLE_NAME,
        Limit=results_limit,
        ExpressionAttributeValues=attribute_values,
        KeyConditionExpression=filter_query
    )

    # Encode the results into json
    json_results = []
    for item in raw_results['Items']:
        packed_response = {'Item': item}
        instance = cls.construct()
        instance.db_from_db_repr(packed_response)
        json_results.append(dynoflrud.jsonify(instance, flaskify=False))

    return flask.jsonify(json_results)


def suggestable(func):
    '''
    Decorator that, when applied to a setter function, will also set a searchable field that can be used
    for suggestion fetching.
    '''
    @wraps(func)
    def decorated_view(*args, **kwargs):
        # Set the suggestion search field to the all lowercase value of the original suggestable field value
        search_field_name = '%s_suggest' % func.__name__
        setattr(args[0], search_field_name, args[1].lower())

        if len(args[1]) > 0:
            setattr(args[0], '%s_fl' % search_field_name, args[1].lower()[0])

        return func(*args, **kwargs)
    return decorated_view
