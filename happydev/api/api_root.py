import os

from flask import Flask
from flask_login import LoginManager
from flask_session import Session

if 'TARGET' not in os.environ:
    os.environ['TARGET'] = 'test'

# Initialize the Flask module
api = Flask(__name__)
api.secret_key = '\xac\xf7q{,\x82D\xb5\xf8~ci\x11=b\xe8Nb\x15\x94)Kh\xfb'

# Configure the session behavior
if os.environ['TARGET'] == 'production':
    SESSION_TYPE = 'redis'

    from redis import Redis
    SESSION_REDIS = Redis(host='webserver-session.qjzaqu.0001.usw2.cache.amazonaws.com', port=6379)
else:
    SESSION_TYPE = 'filesystem'
api.config.from_object(__name__)
Session(api)

# Prevent big file uploads
api.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

# Enable and initialize the Flask login module
lm = LoginManager()
lm.init_app(api)

# Import our APIs into
import happydev.api.account # NOQA
import happydev.api.company # NOQA
import happydev.api.degree # NOQA
import happydev.api.developer # NOQA
import happydev.api.interview_request # NOQA
import happydev.api.skill # NOQA