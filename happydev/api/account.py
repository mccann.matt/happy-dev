'''
@author R. Matt McCann
@brief User account model & API
@copyright &copy; 2017 R. Matt McCann
'''

import flask
import flask_login
import hashlib
import re
import os

from flask_login.mixins import UserMixin
from uuid import uuid4

import dynoflrud
import pydi

from happydev.api.api_root import api, lm
from happydev.api.auth_helper import authenticated_owning_user_only


class Account(dynoflrud.Record, UserMixin):
    ''' User account model. '''

    ''' Secondary index used to validate email uniqueness '''
    DB_EMAIL_INDEX_NAME = '%s.HappyDev.Account.EmailIdx' % os.environ['TARGET']

    ''' Fields that cannot be read via the API. '''
    DB_FIELDS_HIDDEN = ['password', 'salt']

    ''' Fields required to commit the user profile to the db. '''
    DB_FIELDS_REQUIRED = ['first_name', 'last_name', 'email', 'password']

    ''' Attribute definitions for the table. '''
    DB_TABLE_ATTRIBUTE_DEFINITIONS = [
        {'AttributeName': 'id', 'AttributeType': 'S'},
        {'AttributeName': 'email', 'AttributeType': 'S'}
    ]

    ''' Global secondary indicies for the table. '''
    DB_TABLE_GLOBAL_SECONDARY_INDICES = [{
        'IndexName': DB_EMAIL_INDEX_NAME,
        'KeySchema': [{'AttributeName': 'email', 'KeyType': 'HASH'}],
        'Projection': {'ProjectionType': 'KEYS_ONLY'},
        'ProvisionedThroughput': {'ReadCapacityUnits': 1, 'WriteCapacityUnits': 1}
    }]

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HappyDev.Account' % os.environ['TARGET']

    ''' Whether or not this is a staff account '''
    is_staff = False

    def __init__(self, id=None, salt=None):
        super().__init__()

        self.id = id
        self.salt = salt

    @classmethod
    def construct(cls):
        ''' Factory method for building new User profile objects. '''
        return Account(id=str(uuid4()), salt=uuid4().bytes)

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        if len(email) > 128:
            raise ValueError('email must 128 characters or less')
        if len(email) == 0:
            raise ValueError('email must not be empty')
        if not re.match("[^@]+@[^@]+\.[^@]+", email):
            raise ValueError('email format is invalid')

        self._email = email

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        if len(first_name) > 64:
            raise ValueError('first_name must be 64 characters or less')
        if len(first_name) == 0:
            raise ValueError('first_name must not be empty')

        self._first_name = first_name

    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        if len(last_name) > 64:
            raise ValueError('last_name must be 64 characters or less')
        if len(last_name) == 0:
            raise ValueError('last_name must not be empty')

        self._last_name = last_name

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        if password is None:
            raise ValueError("Password must not be None")

        if len(password) == 0:
            raise ValueError("Password must not be empty")

        self._password = password


@api.route('/account/', methods=['POST'])
def account_create():
    db = pydi.di()[pydi.DynamoDB]

    # Construct the account record
    account = Account.construct()

    # Populate the account properties
    response = dynoflrud.set_properties(Account, account)
    if response is not None:
        return response

    # Salt+hash the password
    pre_hash_password = flask.request.json['password']
    account._password = hashlib.sha512(pre_hash_password.encode('utf-8') + account.salt).digest()

    # Ensure the email is unique
    response = db.query(
            TableName=Account.DB_TABLE_NAME,
            IndexName=Account.DB_EMAIL_INDEX_NAME,
            KeyConditionExpression='email = :email',
            ExpressionAttributeValues={':email': {'S': account.email}},
            Select='COUNT'
        )
    if response['Count'] > 0:
        response = flask.jsonify({'status': 'failure', 'message': 'duplicate email'})
        response.status_code = 403
        return response

    # Commit the account record
    account.db_create()

    return dynoflrud.jsonify(account)


@lm.user_loader
def account_load(id):
    ''' Helper function for the Flask login manager to fetch the account. '''
    try:
        return Account.db_get(id=id)
    except ValueError:
        return None


@api.route('/account/login/', methods=['POST'])
def account_login():
    '''
    Endpoint that allows the user to login. Expects 'password' and 'email' in the request data

    @returns 200 on successful login, 403 on bad email/password combo
    '''
    failure_message = 'unrecognized email/password combo'

    # Fetch the account
    try:
        email = flask.request.json['email']

        db = pydi.di()[pydi.DynamoDB]
        response = db.query(
                TableName=Account.DB_TABLE_NAME,
                IndexName=Account.DB_EMAIL_INDEX_NAME,
                KeyConditionExpression='email = :email',
                ExpressionAttributeValues={':email': {'S': email}}
            )
        if response['Count'] == 0:
            raise ValueError('No such account')

        id = response['Items'][0]['id']['S']
        account = Account.db_get(id=id)
    except ValueError:
        response = flask.jsonify({'status': 'failure', 'message': failure_message})
        response.status_code = 403
        return response

    # Check that the password matches
    # TODO(mmccann) - Modify this to support browser password hashing for transmission
    print("Flask JSON")
    print(flask.request.json)
    pre_hash_password = flask.request.json['password']
    hashed_password = hashlib.sha512(pre_hash_password.encode('utf-8') + account.salt).digest()
    if hashed_password != account.password:
        response = flask.jsonify({'status': 'failure', 'message': failure_message})
        response.status_code = 403
        return response

    # Login the user!
    # result = flask_login.login_user(profile, remember=flask.request.json['remember_me'])
    # TODO(mmccann) - Presently the remember me function is barfing!!
    flask_login.login_user(account)

    return flask.jsonify({'status': 'success', 'id': id, 'is_staff': account.is_staff})


@api.route("/account/logout/", methods=['POST'])
def account_logout():
    ''' Endpoint to log out the account. '''
    flask_login.logout_user()

    return flask.jsonify({'status': 'success'})


@api.route('/account/<id>/', methods=['GET'])
@authenticated_owning_user_only
def account_read(id):
    ''' Fetches the account record. '''
    return dynoflrud.jsonify(Account.db_get(id=id))


@api.route('/account/<id>/', methods=['PUT'])
@authenticated_owning_user_only
def account_update(id):
    ''' Updates the account record. '''
    return dynoflrud.update(Account, id)
