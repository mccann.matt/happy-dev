'''
@author R. Matt McCann
@brief Model & API for developer profiles
@copyright &copy; 2017 R. Matt McCann
'''

import flask
import os

import dynoflrud
import pydi

from happydev.api.api_root import api
from happydev.api.auth_helper import authenticated_owning_user_only


class Developer(dynoflrud.Record):
    ''' Profiles for developer account. '''

    ''' Fields required to commit the developer to the db. '''
    DB_FIELDS_REQUIRED = ['id']

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HappyDev.Developer' % os.environ['TARGET']

    @classmethod
    def construct(cls):
        record = Developer()
        record.education = []
        record.questions = [
            {
                'text': 'What is your desired base salary?'
            },
            {
                'text': 'How often would you like to work from home?',
                'answerChoices': [
                    "I'd like to work from home full-time",
                    "I'd like to work from home 1-3 days a week",
                    "I'd like to work from home on occassion",
                    "I'd like to work in office full-time"
                ]
            },
            {
                'text': 'Do you require medical insurance benefits?',
                'answerChoices': ['Yes', 'No']
            },
            {
                'text': 'Do you require dental insurance benefits?',
                'answerChoices': ['Yes', 'No']
            },
            {
                'text': 'Do you require vision insurance benefits?',
                'answerChoices': ['Yes', 'No']
            },
            {
                'text': 'What are your expectations with retirement benefits?',
                'answerChoices': [
                    '401k with company full match',
                    '401k with company partial match',
                    '401k available without match',
                    'Pension'
                ]
            },
            {
                'text': 'What are your interests in continuing education benefits?',
                'answerChoices': [
                    'Full college tuition reimbursement',
                    'Partial college tuition reimbursement',
                    'Certification / training reimbursement'
                ]
            },
            {
                'text': 'What are your paid time off expectations?',
                'answerChoices': [
                    '4+ weeks',
                    '3 weeks',
                    '2 weeks',
                    '1 week'
                ]
            },
            {
                'text': 'What is the maximum commute time you will tolerate?',
                'answerChoices': [
                    '60+ minutes',
                    '45 minutes',
                    '30 minutes',
                    '15 minutes'
                ]
            },
            {
                'text': 'What is your preferred work dress code?',
                'answerChoices': [
                    'Old-school business formal',
                    'Business Casual',
                    'Casual',
                ]
            },
            {
                'text': 'What size of company would you prefer to work with?',
                'answerChoices': [
                    'Large corporation (1000+ employees)',
                    'Medium firm (100-1000 employees)',
                    'Mom & pop shop (0-100 employees)',
                    'Startup (0-100 employees)'
                ]
            },
            {
                'text':
                    'Do you prefer to report to a technical manager (i.e. someone with a similar background to you)?',
                'answerChoices': [
                    'Yes', 'No'
                ]
            },
            {
                'text': 'What is your preference for team composition?',
                'answerChoices': [
                    'Team with similar skillsets',
                    'Cross-disciplinary team'
                ]
            },
            {
                'text': 'Do you want to be provided with computing hardware?',
                'answerChoices': [
                    'Yes', 'No'
                ]
            }
        ]
        record.skills = []
        record.work_history = []

        return record


@api.route('/developer/', methods=['POST'])
def developer_create():
    return dynoflrud.create(Developer)


@api.route('/developer/<id>/', methods=['GET'])
@authenticated_owning_user_only
def developer_read(id):
    ''' Fetches the full developer profile as seen by the developer herself. '''
    return dynoflrud.jsonify(Developer.db_get(id=id))


@api.route('/developer/<id>/portrait/', methods=['POST'])
@authenticated_owning_user_only
def developer_portrait_upload(id):
    '''
    Endpoint that manages pushing the portrait thumbnail to S3 and updating the profile model to point at that
    thumbnail location.

    NOTE: The monotomic increment in the portrait thumbnail name is used to ensure the url of the thumbnail on S3
    changes every time a user uploads a new thumbnail. Without this, the browser does not reload the thumbnail after
    the thumbnail edit is completed, instead requiring a refresh due to the url remaining the same.
    '''
    s3 = pydi.di()[pydi.S3]
    file = flask.request.files['file']

    # Fetch the profile whose portrait is being uploaded
    profile = Developer.db_get(id=id)

    # Check that the filename extension is valid
    file_extension = file.filename[file.filename.rfind('.'):].lower()
    if file_extension not in ['.jpg', '.jpeg', '.png']:
        response = flask.jsonify({'status': 'failure', 'message': 'unsupported file extension'})
        response.status_code = 403
        return response

    # Determine the thumbnail iteration
    iteration = 0
    if hasattr(profile, 'portrait_thumbnail'):
        iteration = int(profile.portrait_thumbnail.split('.')[-2]) + 1
    s3_filename = '%s/portrait.%d.%s' % (id, iteration, file_extension[1:])

    # Upload the file
    s3.upload_fileobj(file, 'happydev.io', s3_filename)
    s3.put_object_acl(ACL='public-read', Bucket='happydev.io', Key=s3_filename)

    # Update the profile portrait raw & thumb properties
    profile.portrait_thumbnail = 'https://s3-us-west-2.amazonaws.com/happydev.io/%s' % s3_filename

    # Commit the profile changes to the db
    profile.db_update()

    return flask.jsonify({'status': 'success'})


@api.route('/developer/<id>/preview/', methods=['GET'])
def developer_read_company_view(id):
    ''' Fetches the anonymized developer profile as seen by the companies. '''
    profile = Developer.db_get(id=id)

    response = dynoflrud.jsonify(profile, flaskify=False)
    del response['first_name']
    del response['last_name']
    del response['email']

    return flask.jsonify(response)


@api.route('/developer/<id>/', methods=['PUT'])
@authenticated_owning_user_only
def developer_update(id):
    return dynoflrud.update(Developer, id)
