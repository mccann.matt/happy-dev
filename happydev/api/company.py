'''
@author R. Matt McCann
@brief Model & API for companies
@copyright &copy; 2017 R. Matt McCann
'''

import flask
import os

import dynoflrud

from happydev.api.api_root import api
from happydev.api.auth_helper import staff_only
from happydev.api.suggestable import suggestable, list_suggestions_api_endpoint


class Company(dynoflrud.Record):
    ''' Profile for a company. '''

    ''' Fields required to commit the company to the db. '''
    DB_FIELDS_REQUIRED = ['about', 'city', 'name', 'state', 'number_of_employees', 'year_founded', 'website_url']

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HappyDev.Company' % os.environ['TARGET']

    @property
    def about(self):
        return self._about

    @about.setter
    def about(self, about):
        dynoflrud.require_max_length('about', about, 4096)
        dynoflrud.require_not_empty('about', about)

        self._about = about

    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, city):
        dynoflrud.require_max_length('city', city, 64)
        dynoflrud.require_not_empty('city', city)

        self._city = city

    @property
    def name(self):
        return self._name

    @name.setter
    @suggestable
    def name(self, name):
        dynoflrud.require_max_length('name', name, 64)
        dynoflrud.require_not_empty('name', name)

        self._name = name
        self._name_suggest_fl = name.lower()[0]

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        dynoflrud.require_not_empty('state', state)

        self._state = state

    @property
    def website_url(self):
        return self._website_url

    @website_url.setter
    def website_url(self, website_url):
        dynoflrud.require_max_length('website_url', website_url, 64)
        dynoflrud.require_not_empty('website_url', website_url)

        self._website_url = website_url


@api.route('/company/', methods=['POST'])
@staff_only
def company_create():
    return dynoflrud.create(Company)


@api.route('/company/<id>/', methods=['GET'])
def company_read(id):
    try:
        return dynoflrud.jsonify(Company.db_get(id=id))
    except ValueError:
        response = flask.jsonify({'status': 'failure', 'message': 'unknown company id'})
        response.status_code = 404
        return response


@api.route('/company/suggest/', methods=['POST'])
@staff_only
def company_suggestion():
    return list_suggestions_api_endpoint(Company, 'name')


@api.route('/company/<id>/', methods=['PUT'])
@staff_only
def company_update(id):
    return dynoflrud.update(Company, id)
