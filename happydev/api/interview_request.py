'''
@author R. Matt McCann
@brief Model & API for interview requests
@copyright &copy; 2017 R. Matt McCann
'''

import flask
import os

import dynoflrud

from happydev.api.api_root import api
from happydev.api.auth_helper import staff_only


class InterviewRequest(dynoflrud.Record):
    ''' Request for an interview from a company. '''

    ''' Fields required to commit the interview request to the db. '''
    DB_FIELDS_REQUIRED = ['company', 'description']

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HappyDev.InterviewRequest' % os.environ['TARGET']

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        dynoflrud.require_max_length('description', description, 4096)
        dynoflrud.require_not_empty('description', description)

        self._description = description


@api.route('/interview-request/', methods=['POST'])
@staff_only
def interview_request_create():
    return dynoflrud.create(InterviewRequest)


@api.route('/interview-request/<id>/', methods=['GET'])
def interview_request_read(id):
    try:
        return dynoflrud.jsonify(InterviewRequest.db_get(id=id))
    except ValueError:
        response = flask.jsonify({'status': 'failure', 'message': 'unknown interview-request id'})
        response.status_code = 404
        return response


@api.route('/interview-request/<id>/', methods=['PUT'])
@staff_only
def interview_request_update(id):
    return dynoflrud.update(InterviewRequest, id)
