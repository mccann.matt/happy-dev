/**
 * @author R. Matt McCann
 * @brief System-wide configuration for the app
 * @copyright &copy; 2016 R. Matt McCann
 */
(function (global) {
    System.config({
        // Configure path aliases
        paths: {
            // Alias for the npm module folder
            'npm:': 'node_modules/'
        },

        // Tell the System loader where to look for things
        map: {
            // Source folder for the app
            app: 'app',

            // Angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

            // Other libraries
            'rxjs': 'npm:rxjs',
            'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
            'angular2-recaptcha': 'npm:angular2-recaptcha'
        },

        // Tell the System loader how to load when no filename or extension is provided
        packages: {
            app: {
                main: './main.js',
                defaultExtension: 'js'
            },
            'angular2-recaptcha': {
                defaultExtension: 'js',
                main: 'index'
            },
            rxjs: {
                defaultExtension: 'js'
            }
        }
    });
})(this);