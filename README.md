[![build status](https://gitlab.com/mccann.matt/happy-dev/badges/master/build.svg)](https://gitlab.com/mccann.matt/happy-dev/commits/master)

[![coverage report](https://gitlab.com/mccann.matt/happy-dev/badges/master/coverage.svg)](https://gitlab.com/mccann.matt/happy-dev/commits/master)

# How to stand up the full-stack locally (Windows):

Define the following environment variables (the values should be your GitLab creds):
```
DEPLOY_USERNAME
DEPLOY_PASSWORD
```

In a Windows Command Prompt, update the code module dependencies:
```
python .\bin\download_deps
```

In a Windows Command Prompt, launch the local AWS clone:
```
cd api
.\bin\launch_local_dynamodb.sh
```

In the Powershell, launch the api server:
```
cd api
.\bin\launch_api.py
```

In the Powershell, launch the dev frontend server:
```
npm run start_dev
```
