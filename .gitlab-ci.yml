image: registry.gitlab.com/mccann.matt/happy-dev:dev

before_script:
    # Copy the existing sources installed in the build folder
    - cp -R /build/node_modules ./node_modules

    # Configure Docker
    - export DOCKER_HOST=tcp://172.17.0.1:4243

    # Install the AWS credentials
    - mkdir ~/.aws
    - touch ~/.aws/credentials
    - echo "[Credentials]" >> ~/.aws/credentials
    - echo "aws_access_key_id=$AWS_ACCESS_KEY_ID" >> ~/.aws/credentials
    - echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials

    # Install the AWS configuration
    - touch ~/.aws/config
    - echo "[default]" >> ~/.aws/config
    - echo "output = json" >> ~/.aws/config
    - echo "region = us-west-2" >> ~/.aws/config

    # Install the external deps
    - mkdir ~/.ssh
    - touch ~/.ssh/id_rsa
    - echo "$DEPLOY_KEY_SECRET" > ~/.ssh/id_rsa
    - chmod 400 ~/.ssh/id_rsa
    - ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
    - eval "$(ssh-agent -s)"
    - |
        if [ "$DEPLOY_KEY_SECRET" ]; then
            ssh-add ~/.ssh/id_rsa
        fi
    - ./bin/download_deps

# Build the docker image
build_docker_image:
    stage: build
    script:
        - ./docker/build.py base --just-assemble-dockerfile
        - docker build --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -t registry.gitlab.com/mccann.matt/happy-dev .

# Build the dev docker image
build_docker_image_dev:
    stage: build
    script:
        - ./docker/build.py dev --just-assemble-dockerfile
        - docker build --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -t registry.gitlab.com/mccann.matt/happy-dev:dev .

# Runs static analysis on the python code base and barfs is lint is found
python_linter:
    stage: test
    script:
        - flake8 happydev --config ./config/flake8.conf

# Runs static analysis on the typescript code base and barfs if list is found
typescript_linter:
    stage: test
    script:
        - npm run tslint

test_python:
    stage: test
    script:
        - ln -s /build/external/DynamoDBLocal.jar external
        - ln -s /build/external/DynamoDBLocal_lib external

        # Launch the local test DynamoDB
        - ./bin/launch_local_dynamodb &
        - sleep 3

        # Run the python unit tests
        - nosetests3 --with-coverage --cover-package=happydev --cover-erase --cover-html --cover-branches --cover-min-percentage=90
    artifacts:
        paths:
            - cover/

test_typescript:
    stage: test
    script:
        # Run the unit tests
        - npm run test-once

        # Check the test coverage is sufficient
        - ./bin/check_typescript_coverage_level.py
    artifacts:
        paths:
            - coverage/

deploy_docker_image:
    stage: deploy
    only:
        - master
    script:
        # It's necessary to rebuild the docker image because the image is autopulled at because of
        # image definition at the beginning of the configuration file
        - ./docker/build.py base --just-assemble-dockerfile
        - docker build --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -t registry.gitlab.com/mccann.matt/happy-dev .

        # Push the image to the registry
        - docker login -p $GITLAB_DOCKER_PASSWORD -u $GITLAB_DOCKER_USERNAME registry.gitlab.com
        - docker push registry.gitlab.com/mccann.matt/happy-dev

        # Bootstrap the database
        - TARGET=production PYTHONPATH=.:./external:./external/mvt-microservice ./bin/bootstrap_db

        # Reboot the stack
        - ./bin/kill_cluster_tasks.py production

deploy_docker_image_dev:
    stage: deploy
    only:
        - master
    script:
        # It's necessary to rebuild the docker image because the image is autopulled at because of
        # image definition at the beginning of the configuration file
        - ./docker/build.py dev --just-assemble-dockerfile
        - docker build --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -t registry.gitlab.com/mccann.matt/happy-dev:dev .

        # Push the image to the registry
        - docker login -p $GITLAB_DOCKER_PASSWORD -u $GITLAB_DOCKER_USERNAME registry.gitlab.com
        - docker push registry.gitlab.com/mccann.matt/happy-dev:dev
