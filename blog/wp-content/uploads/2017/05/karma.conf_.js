// NOTE(mmccann) - This was in the seed, but doesn't appear to be used. Disabling it doesn't
//     cause anything to blow up
var path = require('path');

// Load the webpack configuration
var webpackConfig = require('./config/webpack.unittest.js');

// Check if the npm command was 'test' (run-once) or 'test-watch' (continuous test)
var ENV = process.env.npm_lifecycle_event;
var isTestWatch = ENV === 'test-watch';

// Configure Karma
module.exports = function (config) {
    var _config = {

        // Base path that will be used to resolve all patterns (eg. files, exclude)
        // NOTE(mmccann) - Because we keep the build configuration files outside of the app folder,
        //     we can't cleanly take advantage of the basePath shortcut
        basePath: '',

        // Unit tests are implemented with Jasmine, load it here
        frameworks: ['jasmine'],

        // List of files / patterns to load in the browser
        // NOTE(mmccann) - Everything else is loaded via the webpack configuration
        files: [
            { pattern: './karma-test-shim.js', watched: false }
        ],

        // List of files to exclude
        exclude: [],

        // Preprocess matching files before serving them to the browser
        // NOTE(mmccann) - Hand the shim off to webpack & generate a source map
        preprocessors: {
            './karma-test-shim.js': ['webpack', 'sourcemap'],
            './main.ts': ['coverage']
        },

        // Pass the webpack configuration to the karma webpack plugin
        webpack: webpackConfig,

        // Make webpack less chatty
        webpackMiddleware: {
           stats: 'errors-only'
        },

        // Make webpack less chatty
        webpackServer: {
            noInfo: true // please don't spam the console when running in karma!
        },

        // Test results reporter to use
        reporters: ["mocha", "coverage", "remap-coverage"],

        // save interim raw coverage report in memory
        coverageReporter: {
            type: 'in-memory'
        },

        // define where to save final remaped coverage reports
        remapCoverageReporter: {
            'text-summary': null,
            html: './coverage/html',
            cobertura: './coverage/cobertura.xml'
        },

        // Web server port
        port: 9876,

        // Enable / disable colors in the output (reporters and logs)
        colors: true,

        // Level of logging
        logLevel: config.LOG_INFO,

        browserConsoleLogOptions: {
            level: "error"
        },

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        browsers: ['ChromeNoSandboxHeadless'],

        customLaunchers: {
          ChromeNoSandboxHeadless: {
            base: 'Chrome',
            flags: [
              // See https://chromium.googlesource.com/chromium/src/+/lkgr/headless/README.md
              '--no-sandbox',
              '--headless',
              '--disable-gpu',
              // Without a remote debugging port, Google Chrome exits immediately.
              ' --remote-debugging-port=9222',
            ],
          },
        },

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
  };

    // The original seed had some funny business with conditional edits to the config; this
    // officially passes it off to Karma
    config.set(_config);
};
