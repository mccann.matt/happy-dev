# @author R. Matt McCann
# @brief Helper script that pulls and installs the local Dynamodb jars
# @copyright &copy; 2017 R. Matt McCann

wget https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz > /dev/null
cd external && tar -xvf ../dynamodb_local_latest.tar.gz > /dev/null && cd ..
rm dynamodb_local_latest.tar.gz
