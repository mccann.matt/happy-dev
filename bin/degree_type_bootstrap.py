#!/usr/bin/env python3
import sys
sys.path.insert(0, '.')
sys.path.insert(0, './external')

from happydev.api.degree import Degree
degree = Degree.construct()
degree.name = "Associate of Arts (A.A.)"
degree.name_suggest = "associate of arts (a.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Associate of Arts and Sciences (A.A.S.)"
degree.name_suggest = "associate of arts and sciences (a.a.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Associate of Science (A.S.)"
degree.name_suggest = "associate of science (a.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Associate's degree"
degree.name_suggest = "associate's degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Applied Science (B.A.Sc.)"
degree.name_suggest = "bachelor of applied science (b.a.sc.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Architecture (B.Arch.)"
degree.name_suggest = "bachelor of architecture (b.arch.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Arts (B.A.)"
degree.name_suggest = "bachelor of arts (b.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Business Administration (B.B.A.)"
degree.name_suggest = "bachelor of business administration (b.b.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Fine Arts (B.F.A.)"
degree.name_suggest = "bachelor of fine arts (b.f.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Business Administration (D.B.A)"
degree.name_suggest = "doctor of business administration (d.b.a)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Applied Science (B.A.Sc.)"
degree.name_suggest = "bachelor of applied science (b.a.sc.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Architecture (B.Arch.)"
degree.name_suggest = "bachelor of architecture (b.arch.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Arts (B.A.)"
degree.name_suggest = "bachelor of arts (b.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Business Administration (B.B.A.)"
degree.name_suggest = "bachelor of business administration (b.b.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Commerce (B.Com.)"
degree.name_suggest = "bachelor of commerce (b.com.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Education (B.Ed.)"
degree.name_suggest = "bachelor of education (b.ed.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Engineering (B.Eng.)"
degree.name_suggest = "bachelor of engineering (b.eng.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Fine Arts (B.F.A.)"
degree.name_suggest = "bachelor of fine arts (b.f.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Laws (LL.B.)"
degree.name_suggest = "bachelor of laws (ll.b.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Medicine, Bachelor of Surgery (M.B.B.S.)"
degree.name_suggest = "bachelor of medicine, bachelor of surgery (m.b.b.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Commerce (B.Com.)"
degree.name_suggest = "bachelor of commerce (b.com.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Computer Applications (M.C.A.)"
degree.name_suggest = "master of computer applications (m.c.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Associate's degree"
degree.name_suggest = "associate's degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor's degree"
degree.name_suggest = "bachelor's degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Business Administration (D.B.A)"
degree.name_suggest = "doctor of business administration (d.b.a)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Education (Ed.D.)"
degree.name_suggest = "doctor of education (ed.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Law (J.D.)"
degree.name_suggest = "doctor of law (j.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Medicine (M.D.)"
degree.name_suggest = "doctor of medicine (m.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Pharmacy (Pharm.D.)"
degree.name_suggest = "doctor of pharmacy (pharm.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Philosophy (Ph.D.)"
degree.name_suggest = "doctor of philosophy (ph.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Engineer's degree"
degree.name_suggest = "engineer's degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Foundation degree"
degree.name_suggest = "foundation degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Education (B.Ed.)"
degree.name_suggest = "bachelor of education (b.ed.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Engineering (B.Eng.)"
degree.name_suggest = "bachelor of engineering (b.eng.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Education (Ed.D.)"
degree.name_suggest = "doctor of education (ed.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Engineer's degree"
degree.name_suggest = "engineer's degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Education (M.Ed.)"
degree.name_suggest = "master of education (m.ed.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Fine Arts (B.F.A.)"
degree.name_suggest = "bachelor of fine arts (b.f.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Foundation degree"
degree.name_suggest = "foundation degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Fine Arts (M.F.A.)"
degree.name_suggest = "master of fine arts (m.f.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "High school degree"
degree.name_suggest = "high school degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Public Health (M.P.H.)"
degree.name_suggest = "master of public health (m.p.h.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Library & Information Science (M.L.I.S.)"
degree.name_suggest = "master of library & information science (m.l.i.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Laws (LL.B.)"
degree.name_suggest = "bachelor of laws (ll.b.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Law (J.D.)"
degree.name_suggest = "doctor of law (j.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Licentiate degree"
degree.name_suggest = "licentiate degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Laws (LL.M.)"
degree.name_suggest = "master of laws (ll.m.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Library & Information Science (M.L.I.S.)"
degree.name_suggest = "master of library & information science (m.l.i.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Medicine, Bachelor of Surgery (M.B.B.S.)"
degree.name_suggest = "bachelor of medicine, bachelor of surgery (m.b.b.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Medicine (M.D.)"
degree.name_suggest = "doctor of medicine (m.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Architecture (M.Arch.)"
degree.name_suggest = "master of architecture (m.arch.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Arts (M.A.)"
degree.name_suggest = "master of arts (m.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Business Administration (M.B.A.)"
degree.name_suggest = "master of business administration (m.b.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Computer Applications (M.C.A.)"
degree.name_suggest = "master of computer applications (m.c.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Divinity (M.Div.)"
degree.name_suggest = "master of divinity (m.div.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Education (M.Ed.)"
degree.name_suggest = "master of education (m.ed.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Fine Arts (M.F.A.)"
degree.name_suggest = "master of fine arts (m.f.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Laws (LL.M.)"
degree.name_suggest = "master of laws (ll.m.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Other"
degree.name_suggest = "other"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Pharmacy (B.Pharm.)"
degree.name_suggest = "bachelor of pharmacy (b.pharm.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Pharmacy (Pharm.D.)"
degree.name_suggest = "doctor of pharmacy (pharm.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Doctor of Philosophy (Ph.D.)"
degree.name_suggest = "doctor of philosophy (ph.d.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Philosophy (M.Phil.)"
degree.name_suggest = "master of philosophy (m.phil.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Public Adminstration (M.P.A.)"
degree.name_suggest = "master of public adminstration (m.p.a.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Public Health (M.P.H.)"
degree.name_suggest = "master of public health (m.p.h.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Associate of Arts and Sciences (A.A.S.)"
degree.name_suggest = "associate of arts and sciences (a.a.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Associate of Science (A.S.)"
degree.name_suggest = "associate of science (a.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Applied Science (B.A.Sc.)"
degree.name_suggest = "bachelor of applied science (b.a.sc.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Medicine, Bachelor of Surgery (M.B.B.S.)"
degree.name_suggest = "bachelor of medicine, bachelor of surgery (m.b.b.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Science (B.S.)"
degree.name_suggest = "bachelor of science (b.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "High school degree"
degree.name_suggest = "high school degree"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Library & Information Science (M.L.I.S.)"
degree.name_suggest = "master of library & information science (m.l.i.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Science (M.S.)"
degree.name_suggest = "master of science (m.s.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Social Work (M.S.W.)"
degree.name_suggest = "master of social work (m.s.w.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Bachelor of Technology (B.Tech.)"
degree.name_suggest = "bachelor of technology (b.tech.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Technology (M.Tech.)"
degree.name_suggest = "master of technology (m.tech.)"
degree.db_create()

degree = Degree.construct()
degree.name = "Master of Social Work (M.S.W.)"
degree.name_suggest = "master of social work (m.s.w.)"
degree.db_create()

