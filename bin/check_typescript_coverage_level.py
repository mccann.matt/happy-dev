#!/usr/bin/env python3

import re

coverage_report = open('coverage/html/index.html').read()

global_statement_covr_regex = re.compile('\d+\.\d+\% <\/span>\n.*Statements')
search_result = global_statement_covr_regex.search(coverage_report)
string_match = search_result.group(0)

coverage_level = float(search_result.group(0)[0:5])
if coverage_level < 90:
    raise ValueError("Typescript coverage level must be greater than 90%%, got %0.2f%%!" % coverage_level)
