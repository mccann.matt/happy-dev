#!/usr/bin/env python3

import sys
sys.path.insert(0, '.')
sys.path.insert(0, './external')

import os

from flask_cors import CORS

os.environ['TARGET'] = 'test'

from happydev.api.api_root import api


cors = CORS(api, supports_credentials=True)

api.run(host='0.0.0.0', port=8001)
