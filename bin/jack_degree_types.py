#!/usr/bin/env python3

import json
import os

from string import ascii_lowercase


curl_pre = "curl 'https://www.upwork.com/freelancers/api/v1/data/degrees?prefix="
curl_post = " -H 'cookie: device_view=full; _vhipo=0; __troRUID=234d75fb-dfd6-4729-b882-1a38acf4e9d7; ki_u=ba640bc0-514f-2ada-7f48-bc82; __ssid=1f504133-0804-4a1d-9b0e-0e6893db31f5; mp_b36aa4f2a42867e23d8f9907ea741d91_mixpanel=%7B%22distinct_id%22%3A%20%220a6771a7-ae7d-05b9-5bf7-7a57d4275ec3%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.upwork.com%2Ffreelancers%2F~01875d4bf292a25cc3%22%2C%22%24initial_referring_domain%22%3A%20%22www.upwork.com%22%7D; __zlcmid=frgLOxiyRJckUf; last_accessed_app=dash; sc.ASP.NET_SESSIONID=pizn5dlow5quy3psxa230vy3; _ga=GA1.1.1713655955.1491005305; _br_uid_2=uid%3D6744689336295%3Av%3D11.8%3Ats%3D1491005304999%3Ahc%3D4; ki_t=1491005306249%3B1491953339381%3B1491953339381%3B2%3B2; ki_r=; console_user=bob_villa; recognized=1; oauth2_global_js_token=a4047323c4df7356317ffa94ff3494ab; __cfduid=da9e1a99e1fb8c4f2b2440a2eb4e4ca8f1492118107; optimizelySegments=%7B%222772971468%22%3A%22false%22%2C%222800491501%22%3A%22gc%22%2C%222801081125%22%3A%22direct%22%2C%222806681009%22%3A%22none%22%2C%222853391793%22%3A%22true%22%2C%222877800604%22%3A%22true%22%7D; optimizelyEndUserId=oeu1491005303653r0.08817738665530706; mp_fdf88b8da1749bafc5f24aee259f5aa4_mixpanel=%7B%22distinct_id%22%3A%20%2215b26d80962a5-0cc90db9fa4923-1b23150f-1fa400-15b26d80963a97%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.upwork.com%2Ffreelancers%2F~01875d4bf292a25cc3%22%2C%22%24initial_referring_domain%22%3A%20%22www.upwork.com%22%7D; session_id=0c51e7083a0a7481c3d3d2aee4eeeeec; _ga=GA1.2.1713655955.1491005305; optimizelyBuckets=%7B%7D; _px3=4f9a14d562eb7a5fe2f4351ae936f3bb4e1525d11ec2f397971a11245545b763:xISNebb7KiNHyv+Py/NN7DIbSfzGOU7/l9PCw1yJ+gG7UEcDa/PmwjJa5dN/BdfIp7EDiPxXsQ+w+p/Q+BAZJA==:1000:TmyJvhs6IqlFLkgThJtDT/n+Ve1sXD0T9w6DEq0PkHvgwAXRrxTYYhpykipmTRJGI3c7DTv/d6485fvCyU7eYzTlhJxylMH2ktSX4k67EVGUt4oeFL4OJKBH+LHataY7O8FYQAxj+pW6B0nqPQPrqscgRQYfAkz+6HRNRvfZ0TE=; mp_mixpanel__c=32; visitor_id=67.149.187.21.1491005321587758; current_organization_uid=809579112397828097; company_last_accessed=d13862689; qt_visitor_id=67.149.187.21.1481852231392013; XSRF-TOKEN=ac66d532bcf890db8a84ef058c01062a' -H 'accept-encoding: gzip, deflate, sdch, br' -H 'accept-language: en-US,en;q=0.8' -H 'x-requested-with: XMLHttpRequest' -H 'pragma: no-cache' -H 'x-newrelic-id: VQIBUF5RGwICV1ZTAQAP' -H 'x-odesk-user-agent: oDesk LM' -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36' -H 'accept: application/json, text/plain, */*' -H 'cache-control: no-cache' -H 'authority: www.upwork.com' -H 'referer: https://www.upwork.com/freelancers/~01875d4bf292a25cc3' -H 'x-odesk-csrf-token: ac66d532bcf890db8a84ef058c01062a' --compressed"

output = open('degree_type_bootstrap.py', 'w')
output.write('#!/usr/bin/env python3\n\n')
output.write('from happydev.api.degree import Degree\n')

for letter in ascii_lowercase:
    curl_cmd = curl_pre + letter + "'" + curl_post + ' > /tmp/' + letter + '.dump'

    os.system(curl_cmd)

    degrees = json.loads(open('/tmp/' + letter + '.dump').readline())['result']

    for degree in degrees:
        output.write('degree = Degree.construct()\n')
        output.write('degree.display_name = "%s"\n' % degree['degree'].replace('"', ''))
        output.write('degree.search_name = "%s"\n' % degree['degree'].replace('"', '').lower())
        output.write('degree.db_create()\n\n')


output.close()
