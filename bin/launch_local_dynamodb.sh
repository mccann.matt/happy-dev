echo "woof"

java -Djava.library.path=.\\external\\DynamoDBLocal.jar -jar .\\external\\DynamoDBLocal.jar -sharedDb -inMemory -port 3307 &

sleep 1
echo "Waiting for DynamoDB spin-up..."
sleep 1

echo "Bootstrapping db..."
TARGET=test python ./bin/bootstrap_db

PID=$!
trap 'echo "Shutting down DynamoDB..."; kill $PID; wait $PID' 2
wait $PID
