#!/usr/bin/env python3

# @author R. Matt McCann
# @brief Backups the database containing the blog contents
# @copyright &copy; 2017 R. Matt McCann

import os
import re
import sys

# Check if we have enough arguments
if len(sys.argv) != 3:
    print("Usage: [target] [backup/restore]")
    sys.exit(-1)
target = sys.argv[1]
backup_restore = sys.argv[2]

# Check we are in the correct directory
if not os.path.isfile('backup.sql'):
    print("Usage: Run this script from inside the wordpress repository!")
    sys.exit(-1)

# Parse out the targeted config file
config_file = 'wp-config.%s.php' % target
if target not in ['production', 'pre_production', 'local', 'test']:
    print("Usage: [target] must be (production, pre_production, local, test)")
    sys.exit(-1)

# Parse the config file
db_host = None
db_user = None
db_name = None
db_password = None

lines = open(config_file).readlines()
for line in lines:
    tokens = line.split(' ')

    if len(tokens) < 2:
        continue

    if 'DB_NAME' in tokens[-2]:
        db_name = re.sub(r'\W+', '', tokens[-1].strip())
    if 'DB_USER' in tokens[-2]:
        db_user = re.sub(r'\W+', '', tokens[-1].strip())
    if 'DB_HOST' in tokens[-2]:
        db_host = re.sub(r'[;)\']', '', tokens[-1].strip())
    if 'DB_PASSWORD' in tokens[-2]:
        db_password = re.sub(r'\W+', '', tokens[-1].strip())

print("Database Details: %s@%s:%s" % (db_user, db_host, db_name))

is_backup = False
if backup_restore == 'backup':
    is_backup = True
elif backup_restore == 'restore':
    is_backup = False
else:
    print("Usage: [backup/restore] must be (backup, restore)")
    sys.exit(-1)

command = ''
if is_backup:
    command = "mysqldump -P 3306 -h %s -u %s -p%s %s > backup.sql" % (db_host, db_user, db_password, db_name)
else:
    command = "mysql -P 3306 -h %s -u %s -p%s %s < backup.sql" % (db_host, db_user, db_password, db_name)

os.system(command)
